EESchema Schematic File Version 4
LIBS:mppt-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "MPPT 5V to 12V Boost Converter"
Date "2019-07-14"
Rev "3.0"
Comp "BLUESat UNSW"
Comment1 "Yuchen Wang"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:INDUCTOR L2
U 1 1 5D298860
P 5600 2500
F 0 "L2" H 5600 2715 50  0000 C CNN
F 1 "10uH" H 5600 2624 50  0000 C CNN
F 2 "CustomFootprints:BigL" H 5600 2500 50  0001 C CNN
F 3 "" H 5600 2500 50  0001 C CNN
	1    5600 2500
	1    0    0    -1  
$EndComp
$Comp
L pspice:DIODE D1
U 1 1 5D29894C
P 6600 3350
F 0 "D1" H 6600 3615 50  0000 C CNN
F 1 "MBR0520" H 6600 3524 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6600 3350 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBR0520LT1-D.PDF" H 6600 3350 50  0001 C CNN
	1    6600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3350 6150 3350
Wire Wire Line
	6150 2500 6150 3350
Connection ~ 6150 3350
Wire Wire Line
	6150 3350 6400 3350
Wire Wire Line
	6800 3350 7150 3350
$Comp
L Device:R R18
U 1 1 5D298B2C
P 6600 3700
F 0 "R18" V 6393 3700 50  0000 C CNN
F 1 "117K" V 6484 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6530 3700 50  0001 C CNN
F 3 "~" H 6600 3700 50  0001 C CNN
	1    6600 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 3700 6150 3700
Wire Wire Line
	6750 3700 7150 3700
Wire Wire Line
	7150 3700 7150 3350
Connection ~ 7150 3350
Wire Wire Line
	7150 3350 7550 3350
$Comp
L pspice:CAP CF1
U 1 1 5D298CF9
P 6600 4250
F 0 "CF1" V 6285 4250 50  0000 C CNN
F 1 "220pF" V 6376 4250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6600 4250 50  0001 C CNN
F 3 "" H 6600 4250 50  0001 C CNN
	1    6600 4250
	0    1    1    0   
$EndComp
$Comp
L pspice:CAP C24
U 1 1 5D299613
P 7150 4700
F 0 "C24" H 7328 4746 50  0000 L CNN
F 1 "4.7uf" H 7328 4655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7150 4700 50  0001 C CNN
F 3 "" H 7150 4700 50  0001 C CNN
	1    7150 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3700 7150 4250
Connection ~ 7150 3700
Connection ~ 7150 4250
Wire Wire Line
	7150 4250 7150 4450
Connection ~ 6150 3700
Wire Wire Line
	6150 3700 6450 3700
$Comp
L Device:R R17
U 1 1 5D299962
P 6150 4650
F 0 "R17" H 6220 4696 50  0000 L CNN
F 1 "13.3K" H 6220 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6080 4650 50  0001 C CNN
F 3 "~" H 6150 4650 50  0001 C CNN
	1    6150 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3700 6150 4250
Wire Wire Line
	6150 4250 6350 4250
Wire Wire Line
	6850 4250 7150 4250
Connection ~ 6150 4250
Wire Wire Line
	6150 4250 6150 4500
Wire Wire Line
	6150 4800 6150 4950
Wire Wire Line
	6150 4950 7150 4950
Wire Wire Line
	5650 3950 5650 4950
Wire Wire Line
	5650 4950 6150 4950
Connection ~ 6150 4950
Wire Wire Line
	4600 3350 5250 3350
Wire Wire Line
	4600 3350 4600 2500
Wire Wire Line
	4600 2500 5350 2500
Connection ~ 4600 3350
Wire Wire Line
	4600 3350 4200 3350
Connection ~ 4200 3350
Wire Wire Line
	4200 3350 3950 3350
$Comp
L pspice:CAP C23
U 1 1 5D29BC43
P 4200 4200
F 0 "C23" H 4378 4246 50  0000 L CNN
F 1 "2.2uf" H 4378 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4200 4200 50  0001 C CNN
F 3 "" H 4200 4200 50  0001 C CNN
	1    4200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4450 4200 4950
Wire Wire Line
	4200 4950 5650 4950
Connection ~ 5650 4950
Wire Wire Line
	4200 4950 4000 4950
Connection ~ 4200 4950
$Comp
L power:GND #PWR037
U 1 1 5D29CC03
P 3800 4950
F 0 "#PWR037" H 3800 4700 50  0001 C CNN
F 1 "GND" V 3805 4822 50  0000 R CNN
F 2 "" H 3800 4950 50  0001 C CNN
F 3 "" H 3800 4950 50  0001 C CNN
	1    3800 4950
	0    1    1    0   
$EndComp
Text HLabel 3950 3350 0    50   Input ~ 0
5V
Text HLabel 7550 3350 2    50   Output ~ 0
12V
Text HLabel 3900 5150 0    50   Output ~ 0
GND
Wire Wire Line
	4000 4950 4000 5150
Wire Wire Line
	4000 5150 3900 5150
Connection ~ 4000 4950
Wire Wire Line
	4000 4950 3800 4950
$Comp
L power:+5V #PWR038
U 1 1 5D2AEFE1
P 4200 3350
F 0 "#PWR038" H 4200 3200 50  0001 C CNN
F 1 "+5V" H 4215 3523 50  0000 C CNN
F 2 "" H 4200 3350 50  0001 C CNN
F 3 "" H 4200 3350 50  0001 C CNN
	1    4200 3350
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR040
U 1 1 5D2AE7ED
P 7150 3350
F 0 "#PWR040" H 7150 3200 50  0001 C CNN
F 1 "+12V" H 7165 3523 50  0000 C CNN
F 2 "" H 7150 3350 50  0001 C CNN
F 3 "" H 7150 3350 50  0001 C CNN
	1    7150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2500 6150 2500
$Comp
L Mppt:LM27313 U9
U 1 1 5D3350DC
P 5700 3600
F 0 "U9" H 5625 4115 50  0000 C CNN
F 1 "LM27313" H 5625 4024 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5700 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm27313-q1.pdf" H 5700 4100 50  0001 C CNN
	1    5700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3350 4200 3950
$Comp
L power:+5V #PWR039
U 1 1 5D335CDE
P 5050 3650
F 0 "#PWR039" H 5050 3500 50  0001 C CNN
F 1 "+5V" H 5065 3823 50  0000 C CNN
F 2 "" H 5050 3650 50  0001 C CNN
F 3 "" H 5050 3650 50  0001 C CNN
	1    5050 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3700 5050 3700
Wire Wire Line
	5050 3700 5050 3650
Text Notes 7900 1300 0    50   ~ 0
Typical 5V to 12V step up circuit. Check the datasheet for the LM27313\nfor the circuit reference.\n\nCircuit Traps:\n- Inductor\n- High powers\n- Noise source
Text Notes 7900 1700 0    50   ~ 0
WARNING: This schematic and the PCB may be \ndifferent due to a late reannotation. Be careful when \nsoldering this part.
$EndSCHEMATC

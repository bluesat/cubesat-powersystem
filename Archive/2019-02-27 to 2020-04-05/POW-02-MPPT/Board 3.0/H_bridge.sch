EESchema Schematic File Version 4
LIBS:mppt-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "MPPT MOSFET H-Bridge"
Date "2019-07-14"
Rev "3.0"
Comp "BLUESat UNSW"
Comment1 "Ricky Qu"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3550 2400 1    60   ~ 0
HSA
Text Label 6400 3650 0    60   ~ 0
HSA
$Comp
L power:GND #GND_0102
U 1 1 5D193C19
P 5000 3850
F 0 "#GND_0102" H 5000 3990 20  0000 C CNN
F 1 "GND" H 5000 3960 30  0000 C CNN
F 2 "" H 5000 3850 70  0000 C CNN
F 3 "" H 5000 3850 70  0000 C CNN
	1    5000 3850
	1    0    0    -1  
$EndComp
Text Label 4300 5450 0    60   ~ 0
LOB
$Comp
L power:GND #GND_0103
U 1 1 5D193C21
P 5000 5050
F 0 "#GND_0103" H 5000 5190 20  0000 C CNN
F 1 "GND" H 5000 5160 30  0000 C CNN
F 2 "" H 5000 5050 70  0000 C CNN
F 3 "" H 5000 5050 70  0000 C CNN
	1    5000 5050
	1    0    0    -1  
$EndComp
Text Label 5400 2850 0    60   ~ 0
HOB
Text Label 6400 5450 0    60   ~ 0
HSB
Wire Wire Line
	6400 5250 6600 5250
$Comp
L Device:R R1
U 1 1 5D193C4F
P 3250 2800
F 0 "R1" V 3300 2850 60  0000 R TNN
F 1 "150O" V 3100 2950 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3110 2710 60  0001 C CNN
F 3 "" H 3110 2710 60  0000 C CNN
	1    3250 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5D193C56
P 4800 4250
F 0 "R4" V 4850 4350 60  0000 R TNN
F 1 "150O" V 4650 4350 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4660 4160 60  0001 C CNN
F 3 "" H 4660 4160 60  0000 C CNN
	1    4800 4250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5D193C64
P 5850 2850
F 0 "R5" V 5900 2900 60  0000 R TNN
F 1 "150O" V 5700 2950 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5710 2760 60  0001 C CNN
F 3 "" H 5710 2760 60  0000 C CNN
	1    5850 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D193C6B
P 4750 5450
F 0 "R3" V 4800 5550 60  0000 R TNN
F 1 "150O" V 4600 5550 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4610 5360 60  0001 C CNN
F 3 "" H 4610 5360 60  0000 C CNN
	1    4750 5450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L1
U 1 1 5D193C72
P 6600 4500
F 0 "L1" V 6700 4450 60  0000 L BNN
F 1 "33uH" V 6800 4400 60  0000 L BNN
F 2 "CustomFootprints:BigL" H 6599 4606 60  0001 C CNN
F 3 "" H 6599 4606 60  0000 C CNN
	1    6600 4500
	1    0    0    -1  
$EndComp
Text Label 4400 4250 0    60   ~ 0
LOA
Wire Wire Line
	6600 4050 6400 4050
Wire Wire Line
	2900 2800 3100 2800
Wire Wire Line
	3400 2800 3550 2800
Wire Wire Line
	6000 2850 6100 2850
Wire Wire Line
	4400 4250 4650 4250
Wire Wire Line
	4950 4250 5000 4250
Wire Wire Line
	4900 5450 5000 5450
Wire Wire Line
	4300 5450 4600 5450
Wire Wire Line
	6600 4050 6600 4350
Wire Wire Line
	6600 4650 6600 5250
Text HLabel 2900 2800 0    50   Input ~ 0
HOA
Text HLabel 5400 2850 0    50   Input ~ 0
HOB
Text HLabel 4400 4250 0    50   Input ~ 0
LOA
Text HLabel 4300 5450 0    50   Input ~ 0
LOB
Text HLabel 10250 6000 0    50   Input ~ 0
GND
$Comp
L power:GND #PWR0105
U 1 1 5D1A128C
P 10400 6150
F 0 "#PWR0105" H 10400 5900 50  0001 C CNN
F 1 "GND" H 10405 5977 50  0000 C CNN
F 2 "" H 10400 6150 50  0001 C CNN
F 3 "" H 10400 6150 50  0001 C CNN
	1    10400 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 6000 10400 6000
Wire Wire Line
	10400 6000 10400 6150
Text HLabel 7850 2050 2    50   Output ~ 0
Vbridge
Text HLabel 5200 2000 2    50   Input ~ 0
Vin
$Comp
L Mppt:GenericMosfet Q2
U 1 1 5D1E33ED
P 6100 4450
F 0 "Q2" H 5700 5639 60  0000 C CNN
F 1 "AO4752" H 5700 5533 60  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5700 5533 60  0001 C CNN
F 3 "" H 6100 4450 60  0000 C CNN
	1    6100 4450
	1    0    0    -1  
$EndComp
$Comp
L Mppt:GenericMosfet Q3
U 1 1 5D1E342D
P 6100 5650
F 0 "Q3" H 5700 6839 60  0000 C CNN
F 1 "AO4752" H 5700 6733 60  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5700 6733 60  0001 C CNN
F 3 "" H 6100 5650 60  0000 C CNN
	1    6100 5650
	1    0    0    -1  
$EndComp
Text Label 6100 2450 1    60   ~ 0
HSB
$Comp
L Mppt:GenericMosfet Q4
U 1 1 5D1E351E
P 7200 3050
F 0 "Q4" H 6800 4239 60  0000 C CNN
F 1 "AO4752" H 6800 4133 60  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6800 4133 60  0001 C CNN
F 3 "" H 7200 3050 60  0000 C CNN
	1    7200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2250 6100 2450
Connection ~ 6100 2450
Wire Wire Line
	6100 2450 6100 2650
Wire Wire Line
	6400 3650 6400 3850
Connection ~ 6400 3850
Wire Wire Line
	6400 3850 6400 4050
Connection ~ 6400 4050
Wire Wire Line
	6400 4050 6400 4250
Wire Wire Line
	6400 4850 6400 5050
Connection ~ 6400 5050
Wire Wire Line
	6400 5050 6400 5250
Connection ~ 6400 5250
Wire Wire Line
	6400 5250 6400 5450
Wire Wire Line
	5000 3650 5000 3850
Connection ~ 5000 3850
Wire Wire Line
	5000 3850 5000 4050
Wire Wire Line
	5000 4850 5000 5050
Connection ~ 5000 5050
Wire Wire Line
	5000 5050 5000 5250
$Comp
L Mppt:GenericMosfet Q1
U 1 1 5D1E4088
P 4650 3000
F 0 "Q1" H 4250 4189 60  0000 C CNN
F 1 "AO4752" H 4250 4083 60  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4250 4083 60  0001 C CNN
F 3 "" H 4650 3000 60  0000 C CNN
	1    4650 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2250 7500 2450
Connection ~ 7500 2450
Wire Wire Line
	7500 2450 7500 2650
Connection ~ 7500 2650
Wire Wire Line
	7500 2650 7500 2850
Wire Wire Line
	4950 2200 4950 2400
Connection ~ 4950 2400
Wire Wire Line
	4950 2400 4950 2600
Connection ~ 4950 2600
Wire Wire Line
	4950 2600 4950 2800
Wire Wire Line
	3550 2200 3550 2400
Connection ~ 3550 2400
Wire Wire Line
	3550 2400 3550 2600
Wire Wire Line
	5700 2850 5400 2850
Wire Wire Line
	5200 2000 4950 2000
Wire Wire Line
	4950 2000 4950 2200
Connection ~ 4950 2200
Wire Wire Line
	7850 2050 7500 2050
Wire Wire Line
	7500 2050 7500 2250
Connection ~ 7500 2250
Text Notes 8950 1700 0    50   ~ 0
This subcircuit is the H-bridge. It allows \nboth buck and boost conversion capabilities. See the \ndatasheet for the SM72445 for a brief description on \nhow this circuit is layed out.\n\nCircuit Traps\n- High power\n- Potential noise source\n- Inductor
$EndSCHEMATC

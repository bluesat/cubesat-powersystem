EESchema Schematic File Version 4
LIBS:regulation_rev1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L regulation_rev1-rescue:C_Small-Device C19
U 1 1 5F803A05
P 2200 3200
F 0 "C19" H 2292 3246 50  0000 L CNN
F 1 "47uF" H 2292 3155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2200 3200 50  0001 C CNN
F 3 "~" H 2200 3200 50  0001 C CNN
	1    2200 3200
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C20
U 1 1 5F803A06
P 2450 3200
F 0 "C20" H 2542 3246 50  0000 L CNN
F 1 "22uF" H 2542 3155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2450 3200 50  0001 C CNN
F 3 "~" H 2450 3200 50  0001 C CNN
	1    2450 3200
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C21
U 1 1 5F803A07
P 2700 3200
F 0 "C21" H 2792 3246 50  0000 L CNN
F 1 "47uF" H 2792 3155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2700 3200 50  0001 C CNN
F 3 "~" H 2700 3200 50  0001 C CNN
	1    2700 3200
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C22
U 1 1 5BAE4A36
P 2950 3200
F 0 "C22" H 3042 3246 50  0000 L CNN
F 1 "22uF" H 3042 3155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2950 3200 50  0001 C CNN
F 3 "~" H 2950 3200 50  0001 C CNN
	1    2950 3200
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C24
U 1 1 5BAE4A3D
P 3300 3200
F 0 "C24" H 3392 3246 50  0000 L CNN
F 1 "0.1uF" H 3392 3155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3300 3200 50  0001 C CNN
F 3 "~" H 3300 3200 50  0001 C CNN
	1    3300 3200
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C25
U 1 1 5BAE4A44
P 3550 3200
F 0 "C25" H 3642 3246 50  0000 L CNN
F 1 "0.1uF" H 3642 3155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3550 3200 50  0001 C CNN
F 3 "~" H 3550 3200 50  0001 C CNN
	1    3550 3200
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:TPS56C215-power_ic U2
U 1 1 5BAE4A4B
P 4800 3450
F 0 "U2" H 4775 4125 50  0000 C CNN
F 1 "TPS56C215" H 4775 4034 50  0000 C CNN
F 2 "power_ic:RNN 18-QFN" H 4800 3450 50  0001 C CNN
F 3 "" H 4800 3450 50  0001 C CNN
	1    4800 3450
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C27
U 1 1 5BAE4A52
P 5850 3000
F 0 "C27" V 5621 3000 50  0000 C CNN
F 1 "0.1uF" V 5712 3000 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5850 3000 50  0001 C CNN
F 3 "~" H 5850 3000 50  0001 C CNN
	1    5850 3000
	0    1    1    0   
$EndComp
$Comp
L regulation_rev1-rescue:L_Small-Device L2
U 1 1 5BAE4A59
P 6500 3000
F 0 "L2" V 6685 3000 50  0000 C CNN
F 1 "3.3uH" V 6594 3000 50  0000 C CNN
F 2 "power_ic:L_PA4343.XXXANLT" H 6500 3000 50  0001 C CNN
F 3 "~" H 6500 3000 50  0001 C CNN
	1    6500 3000
	0    -1   -1   0   
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C30
U 1 1 5BAE4A60
P 7200 3150
F 0 "C30" H 7292 3196 50  0000 L CNN
F 1 "0.1uF" H 7292 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7200 3150 50  0001 C CNN
F 3 "~" H 7200 3150 50  0001 C CNN
	1    7200 3150
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C31
U 1 1 5F803A0F
P 7500 3150
F 0 "C31" H 7592 3196 50  0000 L CNN
F 1 "47uF" H 7592 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7500 3150 50  0001 C CNN
F 3 "~" H 7500 3150 50  0001 C CNN
	1    7500 3150
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C33
U 1 1 5F803A10
P 8000 3150
F 0 "C33" H 8092 3196 50  0000 L CNN
F 1 "47uF" H 8092 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8000 3150 50  0001 C CNN
F 3 "~" H 8000 3150 50  0001 C CNN
	1    8000 3150
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C34
U 1 1 5F803A11
P 8250 3150
F 0 "C34" H 8342 3196 50  0000 L CNN
F 1 "47uF" H 8342 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8250 3150 50  0001 C CNN
F 3 "~" H 8250 3150 50  0001 C CNN
	1    8250 3150
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C28
U 1 1 5F803A12
P 6600 3800
F 0 "C28" V 6500 3800 50  0000 C CNN
F 1 "220pF" V 6700 3800 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6600 3800 50  0001 C CNN
F 3 "~" H 6600 3800 50  0001 C CNN
	1    6600 3800
	0    1    1    0   
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C26
U 1 1 5F803A13
P 3800 3500
F 0 "C26" V 3571 3500 50  0000 C CNN
F 1 "C_Small" V 3662 3500 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3800 3500 50  0001 C CNN
F 3 "~" H 3800 3500 50  0001 C CNN
	1    3800 3500
	0    1    1    0   
$EndComp
$Comp
L regulation_rev1-rescue:R_Small-Device R10
U 1 1 5BAE4A91
P 3550 4300
F 0 "R10" H 3609 4346 50  0000 L CNN
F 1 "300k" H 3609 4255 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4300 50  0001 C CNN
F 3 "~" H 3550 4300 50  0001 C CNN
	1    3550 4300
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C23
U 1 1 5BAE4A98
P 3250 4300
F 0 "C23" H 3342 4346 50  0000 L CNN
F 1 "4.7uF" H 3342 4255 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3250 4300 50  0001 C CNN
F 3 "~" H 3250 4300 50  0001 C CNN
	1    3250 4300
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:R_Small-Device R11
U 1 1 5BAE4A9F
P 3550 4700
F 0 "R11" H 3609 4746 50  0000 L CNN
F 1 "5k1" H 3609 4655 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4700 50  0001 C CNN
F 3 "~" H 3550 4700 50  0001 C CNN
	1    3550 4700
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:R_Small-Device R14
U 1 1 5F803A17
P 6400 3500
F 0 "R14" V 6596 3500 50  0000 C CNN
F 1 "18k" V 6505 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6400 3500 50  0001 C CNN
F 3 "~" H 6400 3500 50  0001 C CNN
	1    6400 3500
	0    -1   -1   0   
$EndComp
$Comp
L regulation_rev1-rescue:R_Small-Device R12
U 1 1 5BAE4AAD
P 6250 4050
F 0 "R12" H 6309 4096 50  0000 L CNN
F 1 "2k" H 6309 4005 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6250 4050 50  0001 C CNN
F 3 "~" H 6250 4050 50  0001 C CNN
	1    6250 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0117
U 1 1 5BAE4AB4
P 5650 4500
F 0 "#PWR0117" H 5650 4250 50  0001 C CNN
F 1 "GNDA" H 5655 4327 50  0000 C CNN
F 2 "" H 5650 4500 50  0001 C CNN
F 3 "" H 5650 4500 50  0001 C CNN
	1    5650 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0118
U 1 1 5BAE4ABA
P 3550 4900
F 0 "#PWR0118" H 3550 4650 50  0001 C CNN
F 1 "GNDA" H 3555 4727 50  0000 C CNN
F 2 "" H 3550 4900 50  0001 C CNN
F 3 "" H 3550 4900 50  0001 C CNN
	1    3550 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0119
U 1 1 5BAE4AC0
P 6250 4550
F 0 "#PWR0119" H 6250 4300 50  0001 C CNN
F 1 "GNDA" H 6255 4377 50  0000 C CNN
F 2 "" H 6250 4550 50  0001 C CNN
F 3 "" H 6250 4550 50  0001 C CNN
	1    6250 4550
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:GNDPWR-power #PWR0120
U 1 1 5BAE4AC6
P 5850 4250
F 0 "#PWR0120" H 5850 4050 50  0001 C CNN
F 1 "GNDPWR" H 5854 4096 50  0000 C CNN
F 2 "" H 5850 4200 50  0001 C CNN
F 3 "" H 5850 4200 50  0001 C CNN
	1    5850 4250
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:GNDPWR-power #PWR0121
U 1 1 5BAE4ACC
P 3100 3450
F 0 "#PWR0121" H 3100 3250 50  0001 C CNN
F 1 "GNDPWR" H 3104 3296 50  0000 C CNN
F 2 "" H 3100 3400 50  0001 C CNN
F 3 "" H 3100 3400 50  0001 C CNN
	1    3100 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0122
U 1 1 5BAE4AD2
P 3600 3550
F 0 "#PWR0122" H 3600 3300 50  0001 C CNN
F 1 "GNDA" H 3605 3377 50  0000 C CNN
F 2 "" H 3600 3550 50  0001 C CNN
F 3 "" H 3600 3550 50  0001 C CNN
	1    3600 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3100 4000 3000
Wire Wire Line
	2200 3100 2200 3000
Connection ~ 2200 3000
Wire Wire Line
	2200 3000 1800 3000
Wire Wire Line
	2450 3100 2450 3000
Wire Wire Line
	2200 3000 2450 3000
Connection ~ 2450 3000
Wire Wire Line
	2450 3000 2700 3000
Wire Wire Line
	2700 3100 2700 3000
Connection ~ 2700 3000
Wire Wire Line
	2700 3000 2950 3000
Wire Wire Line
	2950 3100 2950 3000
Connection ~ 2950 3000
Wire Wire Line
	2950 3000 3300 3000
Wire Wire Line
	3300 3100 3300 3000
Connection ~ 3300 3000
Wire Wire Line
	3300 3000 3550 3000
Wire Wire Line
	3550 3100 3550 3000
Connection ~ 3550 3000
Wire Wire Line
	3550 3000 4000 3000
Wire Wire Line
	2200 3300 2450 3300
Connection ~ 2450 3300
Wire Wire Line
	2450 3300 2700 3300
Connection ~ 2700 3300
Wire Wire Line
	2700 3300 2950 3300
Connection ~ 2950 3300
Wire Wire Line
	2950 3300 3100 3300
Connection ~ 3300 3300
Wire Wire Line
	3300 3300 3550 3300
Wire Wire Line
	3100 3450 3100 3300
Connection ~ 3100 3300
Wire Wire Line
	3100 3300 3300 3300
Wire Wire Line
	3900 3500 4000 3500
Wire Wire Line
	3700 3500 3600 3500
Wire Wire Line
	3600 3500 3600 3550
Wire Wire Line
	2350 3700 4000 3700
Wire Wire Line
	4000 4200 3550 4200
Connection ~ 3250 4200
Connection ~ 3550 4200
Wire Wire Line
	3550 4200 3250 4200
Wire Wire Line
	4000 4400 3550 4400
Connection ~ 3550 4400
Wire Wire Line
	3550 4600 3550 4400
Wire Wire Line
	3550 4900 3550 4800
Wire Wire Line
	5550 4400 5650 4400
Wire Wire Line
	5650 4400 5650 4500
Wire Wire Line
	5550 4200 5850 4200
Wire Wire Line
	5850 4200 5850 4250
Wire Wire Line
	5550 3000 5750 3000
Wire Wire Line
	5950 3000 6050 3000
Wire Wire Line
	5550 3200 6050 3200
Wire Wire Line
	6050 3200 6050 3000
Connection ~ 6050 3000
Wire Wire Line
	6050 3000 6400 3000
Wire Wire Line
	5550 3300 6050 3300
Wire Wire Line
	6050 3300 6050 3200
Connection ~ 6050 3200
Wire Wire Line
	6600 3000 6900 3000
Wire Wire Line
	8250 3000 8250 3050
Wire Wire Line
	7200 3050 7200 3000
Connection ~ 7200 3000
$Comp
L regulation_rev1-rescue:GNDPWR-power #PWR0123
U 1 1 5BAE4B25
P 7200 3250
F 0 "#PWR0123" H 7200 3050 50  0001 C CNN
F 1 "GNDPWR" H 7204 3096 50  0000 C CNN
F 2 "" H 7200 3200 50  0001 C CNN
F 3 "" H 7200 3200 50  0001 C CNN
	1    7200 3250
	1    0    0    -1  
$EndComp
Connection ~ 7200 3250
Wire Wire Line
	8250 3000 8500 3000
Wire Wire Line
	6900 3800 6700 3800
Connection ~ 6900 3000
Wire Wire Line
	6900 3000 7000 3000
Wire Wire Line
	6500 3800 6250 3800
Wire Wire Line
	6250 3800 6250 3950
Wire Wire Line
	6250 3800 6250 3500
Wire Wire Line
	5550 3500 6250 3500
Connection ~ 6250 3800
Text HLabel 2350 3700 0    50   Input ~ 0
EN
Text HLabel 2350 3900 0    50   Input ~ 0
PGOOD
Text HLabel 1600 3000 0    50   Input ~ 0
VIN
Text HLabel 9250 3000 2    50   Output ~ 0
VOUT
Text HLabel 8900 3250 2    50   Output ~ 0
GND
$Comp
L regulation_rev1-rescue:C_Small-Device C35
U 1 1 5BAE4B3E
P 8500 3150
F 0 "C35" H 8592 3196 50  0000 L CNN
F 1 "47uF" H 8592 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8500 3150 50  0001 C CNN
F 3 "~" H 8500 3150 50  0001 C CNN
	1    8500 3150
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:C_Small-Device C32
U 1 1 5F803A21
P 7750 3150
F 0 "C32" H 7842 3196 50  0000 L CNN
F 1 "47uF" H 7842 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7750 3150 50  0001 C CNN
F 3 "~" H 7750 3150 50  0001 C CNN
	1    7750 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3000 7500 3000
Wire Wire Line
	7500 3050 7500 3000
Connection ~ 7500 3000
Wire Wire Line
	7500 3000 7750 3000
Wire Wire Line
	7750 3050 7750 3000
Connection ~ 7750 3000
Wire Wire Line
	7750 3000 8000 3000
Connection ~ 8250 3000
Wire Wire Line
	8000 3050 8000 3000
Connection ~ 8000 3000
Wire Wire Line
	8000 3000 8250 3000
Wire Wire Line
	8500 3050 8500 3000
Connection ~ 8500 3000
Wire Wire Line
	3250 4500 3250 4400
Wire Wire Line
	8500 3000 8750 3000
Text Notes 8700 2850 2    50   ~ 0
Tantalum capacitors can be added\nto increase bulk capacitance, but \nthey have a limited lifespan of \n~2000 hrs at 80C
Text Notes 8850 3650 2    50   ~ 0
47uF capacitors can be added on to\nimprove ripple handling, but at greater cost
Wire Wire Line
	6900 3000 6900 3500
Wire Wire Line
	6250 3500 6300 3500
Connection ~ 6250 3500
Wire Wire Line
	6500 3500 6550 3500
Connection ~ 6900 3500
Wire Wire Line
	6900 3500 6900 3800
$Comp
L regulation_rev1-rescue:C_Small-Device C29
U 1 1 5F803A22
P 7000 3150
F 0 "C29" H 7092 3196 50  0000 L CNN
F 1 "0.1uF" H 7092 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7000 3150 50  0001 C CNN
F 3 "~" H 7000 3150 50  0001 C CNN
	1    7000 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3250 7200 3250
Wire Wire Line
	7000 3050 7000 3000
Connection ~ 7000 3000
Wire Wire Line
	7000 3000 7200 3000
$Comp
L regulation_rev1-rescue:C_Small-Device C36
U 1 1 5F803A23
P 8750 3150
F 0 "C36" H 8842 3196 50  0000 L CNN
F 1 "47uF" H 8842 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8750 3150 50  0001 C CNN
F 3 "~" H 8750 3150 50  0001 C CNN
	1    8750 3150
	1    0    0    -1  
$EndComp
Connection ~ 8750 3000
Wire Wire Line
	8750 3050 8750 3000
$Comp
L power:GNDA #PWR0124
U 1 1 5BAE4B93
P 4650 5250
F 0 "#PWR0124" H 4650 5000 50  0001 C CNN
F 1 "GNDA" H 4655 5077 50  0000 C CNN
F 2 "" H 4650 5250 50  0001 C CNN
F 3 "" H 4650 5250 50  0001 C CNN
	1    4650 5250
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:GNDPWR-power #PWR0125
U 1 1 5BAE4B99
P 5200 5250
F 0 "#PWR0125" H 5200 5050 50  0001 C CNN
F 1 "GNDPWR" H 5204 5096 50  0000 C CNN
F 2 "" H 5200 5200 50  0001 C CNN
F 3 "" H 5200 5200 50  0001 C CNN
	1    5200 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5250 4650 5150
Wire Wire Line
	5200 5150 5200 5250
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5BAE4BA2
P 4650 5150
F 0 "#FLG0105" H 4650 5225 50  0001 C CNN
F 1 "PWR_FLAG" H 4650 5324 50  0000 C CNN
F 2 "" H 4650 5150 50  0001 C CNN
F 3 "~" H 4650 5150 50  0001 C CNN
	1    4650 5150
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0106
U 1 1 5BAE4BA9
P 1800 3000
F 0 "#FLG0106" H 1800 3075 50  0001 C CNN
F 1 "PWR_FLAG" H 1800 3174 50  0000 C CNN
F 2 "" H 1800 3000 50  0001 C CNN
F 3 "~" H 1800 3000 50  0001 C CNN
	1    1800 3000
	1    0    0    -1  
$EndComp
Connection ~ 1800 3000
Wire Wire Line
	1800 3000 1600 3000
$Comp
L power:PWR_FLAG #FLG0107
U 1 1 5BAE4BB1
P 6900 3000
F 0 "#FLG0107" H 6900 3075 50  0001 C CNN
F 1 "PWR_FLAG" H 6900 3174 50  0000 C CNN
F 2 "" H 6900 3000 50  0001 C CNN
F 3 "~" H 6900 3000 50  0001 C CNN
	1    6900 3000
	1    0    0    -1  
$EndComp
Text Label 5700 3500 2    50   ~ 0
FB
Text Label 5700 3200 2    50   ~ 0
SW
Text Label 5700 3000 2    50   ~ 0
BOOT
Text Label 3950 4400 2    50   ~ 0
MODE
Text Label 3950 4200 2    50   ~ 0
VREG5
Text Label 3950 3900 2    50   ~ 0
PGOOD
Text Label 3950 3700 2    50   ~ 0
EN
Text Label 4000 3500 2    50   ~ 0
SS
$Comp
L regulation_rev1-rescue:R_Small-Device R15
U 1 1 5F803A02
P 6650 3500
F 0 "R15" V 6846 3500 50  0000 C CNN
F 1 "R" V 6755 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6650 3500 50  0001 C CNN
F 3 "~" H 6650 3500 50  0001 C CNN
	1    6650 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 3500 6900 3500
$Comp
L regulation_rev1-rescue:Net-Tie_2-Device NT2
U 1 1 5BAD25CF
P 4900 5150
F 0 "NT2" H 4900 5328 50  0000 C CNN
F 1 "Net-Tie_2" H 4900 5237 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 4900 5150 50  0001 C CNN
F 3 "~" H 4900 5150 50  0001 C CNN
	1    4900 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5150 4650 5150
Connection ~ 4650 5150
Wire Wire Line
	5000 5150 5200 5150
$Comp
L power:PWR_FLAG #FLG0108
U 1 1 5F803A04
P 5200 5150
F 0 "#FLG0108" H 5200 5225 50  0001 C CNN
F 1 "PWR_FLAG" H 5200 5324 50  0000 C CNN
F 2 "" H 5200 5150 50  0001 C CNN
F 3 "~" H 5200 5150 50  0001 C CNN
	1    5200 5150
	1    0    0    -1  
$EndComp
Connection ~ 5200 5150
$Comp
L power:GNDA #PWR0126
U 1 1 5BAF43BE
P 3250 4500
F 0 "#PWR0126" H 3250 4250 50  0001 C CNN
F 1 "GNDA" H 3255 4327 50  0000 C CNN
F 2 "" H 3250 4500 50  0001 C CNN
F 3 "" H 3250 4500 50  0001 C CNN
	1    3250 4500
	1    0    0    -1  
$EndComp
Connection ~ 8750 3250
Wire Wire Line
	8750 3250 8900 3250
Wire Wire Line
	8500 3250 8750 3250
Wire Wire Line
	7750 3250 8000 3250
Connection ~ 7750 3250
Wire Wire Line
	7500 3250 7750 3250
Wire Wire Line
	7500 3250 7200 3250
Connection ~ 7500 3250
Connection ~ 8500 3250
Connection ~ 8000 3250
Connection ~ 8250 3250
Wire Wire Line
	8500 3250 8250 3250
Wire Wire Line
	8250 3250 8000 3250
Text HLabel 2800 4200 0    50   Input ~ 0
VREG5
Wire Wire Line
	2350 3900 3050 3900
Wire Wire Line
	2800 4200 3050 4200
Wire Wire Line
	5550 3700 5550 4200
$Comp
L regulation_rev1-rescue:R-Device R8
U 1 1 5C838EE2
P 3050 4050
F 0 "R8" H 3120 4096 50  0000 L CNN
F 1 "10k" H 3120 4005 50  0000 L CNN
F 2 "Custom footprints:R_0805_HandSoldering" V 2980 4050 50  0001 C CNN
F 3 "~" H 3050 4050 50  0001 C CNN
	1    3050 4050
	1    0    0    -1  
$EndComp
Connection ~ 3050 3900
Wire Wire Line
	3050 3900 4000 3900
Connection ~ 3050 4200
Wire Wire Line
	3050 4200 3250 4200
$Comp
L regulation_rev1-rescue:LED_Small-Device D?
U 1 1 5C83AF84
P 9150 3600
AR Path="/5C83AF84" Ref="D?"  Part="1" 
AR Path="/5BAE459F/5C83AF84" Ref="D?"  Part="1" 
AR Path="/5C8449CD/5C83AF84" Ref="D2"  Part="1" 
F 0 "D2" H 9150 3395 50  0000 C CNN
F 1 "VOUT" H 9150 3486 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" V 9150 3600 50  0001 C CNN
F 3 "~" V 9150 3600 50  0001 C CNN
	1    9150 3600
	0    -1   -1   0   
$EndComp
$Comp
L regulation_rev1-rescue:R_Small-Device R?
U 1 1 5C83AF8B
P 9150 4000
AR Path="/5C83AF8B" Ref="R?"  Part="1" 
AR Path="/5BAE459F/5C83AF8B" Ref="R?"  Part="1" 
AR Path="/5C8449CD/5C83AF8B" Ref="R16"  Part="1" 
F 0 "R16" V 9346 4000 50  0000 C CNN
F 1 "1k" V 9255 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9150 4000 50  0001 C CNN
F 3 "~" H 9150 4000 50  0001 C CNN
	1    9150 4000
	1    0    0    -1  
$EndComp
$Comp
L regulation_rev1-rescue:GNDPWR-power #PWR?
U 1 1 5C83AF92
P 9150 4200
AR Path="/5C83AF92" Ref="#PWR?"  Part="1" 
AR Path="/5BAE459F/5C83AF92" Ref="#PWR?"  Part="1" 
AR Path="/5C8449CD/5C83AF92" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 9150 4000 50  0001 C CNN
F 1 "GNDPWR" H 9154 4046 50  0000 C CNN
F 2 "" H 9150 4150 50  0001 C CNN
F 3 "" H 9150 4150 50  0001 C CNN
	1    9150 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3900 9150 3700
Wire Wire Line
	9150 4100 9150 4200
Wire Wire Line
	9150 3500 9150 3000
Wire Wire Line
	8750 3000 9150 3000
Wire Wire Line
	9250 3000 9150 3000
Connection ~ 9150 3000
$Comp
L regulation_rev1-rescue:R_Small-Device R13
U 1 1 5C846DA8
P 6250 4400
F 0 "R13" H 6309 4446 50  0000 L CNN
F 1 "2k" H 6309 4355 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6250 4400 50  0001 C CNN
F 3 "~" H 6250 4400 50  0001 C CNN
	1    6250 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 4150 6250 4300
Wire Wire Line
	6250 4500 6250 4550
$EndSCHEMATC

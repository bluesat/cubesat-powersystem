// control board
// communicate with TCA9554 and INA226 via I2C.

#define INA226_ADDRESS 0x40
#define INA226_MAX_VOLTAGE 5

#include <Wire.h>
#include "INA226.h"
#include "Pca9554.h"

void checkConfig(void);

INA226 ina;

void setup()
{
  Wire.begin();
  Serial.begin(9600);
  while (!Serial); // wait for serial monitor // COMMENT OUT when final

  // PART 1: INA226
  Serial.println("Initialize INA226");
  Serial.println("-----------------------------------------------");
  
  ina.begin(INA226_ADDRESS);
 
  // Configure INA226
  ina.configure(INA226_AVERAGES_1, INA226_BUS_CONV_TIME_1100US, INA226_SHUNT_CONV_TIME_1100US, INA226_MODE_SHUNT_BUS_CONT);
 
  // Calibrate INA226. Rshunt = 0.001 ohm, Max excepted current = 2A
  // ********* The overcurrent limit has been set at 2A (N.B. these resistance values should be checked by at least one other person).
  ina.calibrate(0.001, 2);

  // Enable Shunt Over-Voltage Alert
  //ina.enableShuntOverLimitAlert();
  //ina.setShuntVoltageLimit(INA226_MAX_VOLTAGE);
 
  // Display configuration
  checkConfig();
 
  Serial.println("-----------------------------------------------");
  
  // PART 2: TCA9554
  Serial.println("Initialize TCA9554");
  Serial.println("-----------------------------------------------");

  // Initialize the Pca9554 library
  Pca9554.begin();
  
  // Set pin modes
  Pca9554.pinMode(1, OUTPUT);
  Pca9554.pinMode(6, OUTPUT);  // MANUAL
  Pca9554.pinMode(7, INPUT);  // ALERT
  
  // Make them high
  Pca9554.digitalWrite(1, HIGH);
  Pca9554.digitalWrite(6, HIGH);

  Serial.println("-----------------------------------------------");
}

void loop()
{
  Serial.print("Bus voltage:   ");
  Serial.print(ina.readBusVoltage(), 5);
  Serial.println(" V");
 
  Serial.print("Bus power:     ");
  Serial.print(ina.readBusPower(), 5);
  Serial.println(" W");
 
 
  Serial.print("Shunt voltage: ");
  Serial.print(ina.readShuntVoltage(), 5);
  Serial.println(" V");
 
  Serial.print("Shunt current: ");
  Serial.print(ina.readShuntCurrent(), 5);
  Serial.println(" A");
 
  Serial.println("");

  // Make pin 1 high for a while
  Pca9554.digitalWrite(1, HIGH);
  Serial.println("1 is high");
  delay(500);

  // And bring it low again
  Pca9554.digitalWrite(1, LOW);
  Serial.println("1 is low");
  delay(500);

  // check alert
  boolean alert = Pca9554.digitalRead(7);
  if (alert) {
    Serial.println("********************\nAlert on detected. Set manual to off to cut off the circuit.\n********************");
  } else {
    Serial.println("Alert off detected.");
  }
}

void checkConfig()
{
  Serial.print("Mode:                  ");
  switch (ina.getMode())
  {
    case INA226_MODE_POWER_DOWN:      Serial.println("Power-Down"); break;
    case INA226_MODE_SHUNT_TRIG:      Serial.println("Shunt Voltage, Triggered"); break;
    case INA226_MODE_BUS_TRIG:        Serial.println("Bus Voltage, Triggered"); break;
    case INA226_MODE_SHUNT_BUS_TRIG:  Serial.println("Shunt and Bus, Triggered"); break;
    case INA226_MODE_ADC_OFF:         Serial.println("ADC Off"); break;
    case INA226_MODE_SHUNT_CONT:      Serial.println("Shunt Voltage, Continuous"); break;
    case INA226_MODE_BUS_CONT:        Serial.println("Bus Voltage, Continuous"); break;
    case INA226_MODE_SHUNT_BUS_CONT:  Serial.println("Shunt and Bus, Continuous"); break;
    default: Serial.println("unknown");
  }
 
  Serial.print("Samples average:       ");
  switch (ina.getAverages())
  {
    case INA226_AVERAGES_1:           Serial.println("1 sample"); break;
    case INA226_AVERAGES_4:           Serial.println("4 samples"); break;
    case INA226_AVERAGES_16:          Serial.println("16 samples"); break;
    case INA226_AVERAGES_64:          Serial.println("64 samples"); break;
    case INA226_AVERAGES_128:         Serial.println("128 samples"); break;
    case INA226_AVERAGES_256:         Serial.println("256 samples"); break;
    case INA226_AVERAGES_512:         Serial.println("512 samples"); break;
    case INA226_AVERAGES_1024:        Serial.println("1024 samples"); break;
    default: Serial.println("unknown");
  }
 
  Serial.print("Bus conversion time:   ");
  switch (ina.getBusConversionTime())
  {
    case INA226_BUS_CONV_TIME_140US:  Serial.println("140uS"); break;
    case INA226_BUS_CONV_TIME_204US:  Serial.println("204uS"); break;
    case INA226_BUS_CONV_TIME_332US:  Serial.println("332uS"); break;
    case INA226_BUS_CONV_TIME_588US:  Serial.println("558uS"); break;
    case INA226_BUS_CONV_TIME_1100US: Serial.println("1.100ms"); break;
    case INA226_BUS_CONV_TIME_2116US: Serial.println("2.116ms"); break;
    case INA226_BUS_CONV_TIME_4156US: Serial.println("4.156ms"); break;
    case INA226_BUS_CONV_TIME_8244US: Serial.println("8.244ms"); break;
    default: Serial.println("unknown");
  }
 
  Serial.print("Shunt conversion time: ");
  switch (ina.getShuntConversionTime())
  {
    case INA226_SHUNT_CONV_TIME_140US:  Serial.println("140uS"); break;
    case INA226_SHUNT_CONV_TIME_204US:  Serial.println("204uS"); break;
    case INA226_SHUNT_CONV_TIME_332US:  Serial.println("332uS"); break;
    case INA226_SHUNT_CONV_TIME_588US:  Serial.println("558uS"); break;
    case INA226_SHUNT_CONV_TIME_1100US: Serial.println("1.100ms"); break;
    case INA226_SHUNT_CONV_TIME_2116US: Serial.println("2.116ms"); break;
    case INA226_SHUNT_CONV_TIME_4156US: Serial.println("4.156ms"); break;
    case INA226_SHUNT_CONV_TIME_8244US: Serial.println("8.244ms"); break;
    default: Serial.println("unknown");
  }
 
  Serial.print("Max possible current:  ");
  Serial.print(ina.getMaxPossibleCurrent());
  Serial.println(" A");
 
  Serial.print("Max current:           ");
  Serial.print(ina.getMaxCurrent());
  Serial.println(" A");
 
  Serial.print("Max shunt voltage:     ");
  Serial.print(ina.getMaxShuntVoltage());
  Serial.println(" V");
 
  Serial.print("Max power:             ");
  Serial.print(ina.getMaxPower());
  Serial.println(" W");
}

// END

// This is a I2C scanner for debug purpose.
/* ************************************************************************************************************************
byte start_address = 0;
byte end_address = 127;

void setup()
{
  Wire.begin();
 
  Serial.begin(9600);
  while (!Serial);             // Leonardo: wait for serial monitor
  Serial.println("\nI2C Scanner");
}
 
 
void loop()
{
  byte rc;
  Wire.begin();

  Serial.begin(9600);
  Serial.println("\nI2C Scanner");

  Serial.print("Scanning I2C bus from ");
  Serial.print(start_address,DEC);  Serial.print(" to ");  Serial.print(end_address,DEC);
  Serial.println("...");

  for( byte addr  = start_address;
            addr <= end_address;
            addr++ ) {
      Wire.beginTransmission(addr);
      rc = Wire.endTransmission();

      if (addr<16) Serial.print("0");
      Serial.print(addr,HEX);
      if (rc==0) {
        Serial.print(" found!");
      } else {
        Serial.print(" "); Serial.print(rc); Serial.print("     ");
      }
      Serial.print( (addr%8)==7 ? "\n":" ");
  }

  Serial.println("\n-------------------------------\nPossible devices:");

  for( byte addr  = start_address;
            addr <= end_address;
            addr++ ) {
      Wire.beginTransmission(addr);
      rc = Wire.endTransmission();
      if (rc == 0) {
        Serial.print(addr,HEX); Serial.print(" = ");
        switch (addr) {
          case 0x27: Serial.println("LiquidCrystal_I2C"); break;
          case 0x50: Serial.println("AT24C32/AT24C64 - EEPROM"); break;
          case 0x68: Serial.println("DS1307"); break;
          default: Serial.println("Unknown"); break;
        }
      }
  }

  Serial.println("\ndone");
}
  ************************************************************************************************************************
*/

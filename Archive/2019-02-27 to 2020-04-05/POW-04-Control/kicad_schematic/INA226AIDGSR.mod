PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SOP50P490X110-10N
$EndINDEX
$MODULE SOP50P490X110-10N
Po 0 0 0 15 00000000 00000000 ~~
Li SOP50P490X110-10N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -0.66015 -2.63559 1.00023 1.00023 0 0.05 N V 21 "SOP50P490X110-10N"
T1 -0.02502 2.36712 1.0009 1.0009 0 0.05 N V 21 "VAL**"
DS -1.15 -1.5 1.15 -1.5 0.2 21
DS 1.15 -1.5 1.15 1.5 0.2 21
DS 1.15 1.5 -1.15 1.5 0.2 21
DS -1.15 1.5 -1.15 -1.5 0.2 21
DS -3.2 -1.8 3.2 -1.8 0.05 26
DS 3.2 -1.8 3.2 1.8 0.05 26
DS 3.2 1.8 -3.2 1.8 0.05 26
DS -3.2 1.8 -3.2 -1.8 0.05 26
DC -2.2 -1.6 -1.97639 -1.6 0.05 21
$PAD
Sh "1" R 1.45 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 -1
$EndPAD
$PAD
Sh "2" R 1.45 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 -0.5
$EndPAD
$PAD
Sh "3" R 1.45 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 0
$EndPAD
$PAD
Sh "4" R 1.45 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 0.5
$EndPAD
$PAD
Sh "5" R 1.45 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 1
$EndPAD
$PAD
Sh "6" R 1.45 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 1
$EndPAD
$PAD
Sh "7" R 1.45 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 0.5
$EndPAD
$PAD
Sh "8" R 1.45 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 0
$EndPAD
$PAD
Sh "9" R 1.45 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 -0.5
$EndPAD
$PAD
Sh "10" R 1.45 0.3 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 -1
$EndPAD
$EndMODULE SOP50P490X110-10N

EESchema Schematic File Version 4
LIBS:ATMEGA128-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega128-16AU U1
U 1 1 5D296ABF
P 4450 3500
F 0 "U1" H 4450 1414 50  0000 C CNN
F 1 "ATmega128-16AU" H 4450 1323 50  0000 C CNN
F 2 "ATMEGA128L-8AU:QFP80P1600X1600X120-64N" H 4450 3500 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2467.pdf" H 4450 3500 50  0001 C CNN
	1    4450 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2800 6200 2800
Wire Wire Line
	5050 2900 6200 2900
Wire Wire Line
	5050 3000 6200 3000
Wire Wire Line
	5050 2700 6200 2700
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 5D296F04
P 6400 2900
F 0 "J2" H 6427 2926 50  0000 L CNN
F 1 "SPI OBC Communications" H 6427 2835 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 6400 2900 50  0001 C CNN
F 3 "~" H 6400 2900 50  0001 C CNN
	1    6400 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5500 6200 5500
Wire Wire Line
	2400 1500 2800 1500
Connection ~ 4450 1500
Wire Wire Line
	4450 1500 4550 1500
Wire Wire Line
	4450 5500 2300 5500
Wire Wire Line
	2300 5500 2300 1650
Connection ~ 4450 5500
Wire Wire Line
	5050 4500 6600 4500
Wire Wire Line
	5050 4600 6600 4600
Text Label 6250 4500 0    50   ~ 0
SCL(I2C)
Text Label 6250 4600 0    50   ~ 0
SDA(I2C)
Text Label 5850 2700 0    50   ~ 0
SS(SPI)
Text Label 5850 2800 0    50   ~ 0
SCK(SPI)
Text Label 5850 2900 0    50   ~ 0
MOSI(SPI)
Text Label 5850 3000 0    50   ~ 0
MISO(SPI)
Text Label 3000 1500 0    50   ~ 0
VCC
NoConn ~ 3850 1800
NoConn ~ 3850 2000
NoConn ~ 3850 2200
NoConn ~ 3850 2400
NoConn ~ 3850 2600
NoConn ~ 3850 4700
NoConn ~ 3850 4800
NoConn ~ 3850 5000
NoConn ~ 3850 5100
NoConn ~ 3850 5200
NoConn ~ 5050 5200
NoConn ~ 5050 5100
NoConn ~ 5050 5000
NoConn ~ 5050 4900
NoConn ~ 5050 4800
NoConn ~ 5050 4700
NoConn ~ 5050 3400
NoConn ~ 5050 3300
NoConn ~ 5050 3200
NoConn ~ 5050 3100
NoConn ~ 5050 2500
NoConn ~ 5050 2400
NoConn ~ 5050 2300
NoConn ~ 5050 2200
NoConn ~ 5050 2100
$Comp
L Analog_ADC:INA226 U2
U 1 1 5D3C23A4
P 6700 1250
F 0 "U2" H 6350 1900 50  0000 C CNN
F 1 "INA226" H 6350 1800 50  0000 C CNN
F 2 "INA226AIDGSR:SOP50P490X110-10N" H 6750 1350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina226.pdf" H 7050 1150 50  0001 C CNN
	1    6700 1250
	1    0    0    -1  
$EndComp
$Comp
L MicroController:CD74HC4051-EP_8-1AnalogMultiplexer AfterResistor1
U 1 1 5D3C2B4A
P 1150 5600
F 0 "AfterResistor1" H 1425 5775 50  0000 C CNN
F 1 "CD74HC4051-EP_8-1AnalogMultiplexer" H 1425 5684 50  0000 C CNN
F 2 "CD74HC4051MT:SOIC127P600X175-16N" H 1150 5600 50  0001 C CNN
F 3 "" H 1150 5600 50  0001 C CNN
	1    1150 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 1350 10450 1300
Wire Wire Line
	10450 1450 10450 1500
Text GLabel 6600 4500 2    50   BiDi ~ 0
SCL(I2C)
Text GLabel 6600 4600 2    50   BiDi ~ 0
SDA(I2C)
Text GLabel 950  5650 0    50   Input ~ 0
3V3_1_A
Text GLabel 10250 1000 1    50   Output ~ 0
3V3_1_A
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5D40B016
P 2800 1500
F 0 "#FLG0101" H 2800 1575 50  0001 C CNN
F 1 "PWR_FLAG" H 2800 1674 50  0000 C CNN
F 2 "" H 2800 1500 50  0001 C CNN
F 3 "~" H 2800 1500 50  0001 C CNN
	1    2800 1500
	1    0    0    -1  
$EndComp
Connection ~ 2800 1500
Wire Wire Line
	2800 1500 4450 1500
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5D40B4CF
P 2300 1650
F 0 "#FLG0102" H 2300 1725 50  0001 C CNN
F 1 "PWR_FLAG" V 2300 1778 50  0000 L CNN
F 2 "" H 2300 1650 50  0001 C CNN
F 3 "~" H 2300 1650 50  0001 C CNN
	1    2300 1650
	0    -1   -1   0   
$EndComp
Connection ~ 2300 1650
Wire Wire Line
	6200 3100 6200 5500
Wire Wire Line
	7100 1250 7200 1250
Wire Wire Line
	7100 1350 7200 1350
Text GLabel 7350 1250 2    50   BiDi ~ 0
SDA(I2C)
Text GLabel 7350 1350 2    50   BiDi ~ 0
SCL(I2C)
Text GLabel 950  5750 0    50   Input ~ 0
5V_1_A
Text GLabel 950  5850 0    50   Input ~ 0
3V3_2_A
Text GLabel 950  5950 0    50   Input ~ 0
5V_2_A
Text GLabel 950  6050 0    50   Input ~ 0
3V3_3_A
Text GLabel 950  6150 0    50   Input ~ 0
5V_3_A
Text GLabel 950  6250 0    50   Input ~ 0
3V3_4_A
Text GLabel 950  6350 0    50   Input ~ 0
5V_4_A
$Comp
L Device:R R3
U 1 1 5D419B1B
P 2300 5950
F 0 "R3" V 2093 5950 50  0000 C CNN
F 1 "10K OHMS" V 2184 5950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2230 5950 50  0001 C CNN
F 3 "~" H 2300 5950 50  0001 C CNN
	1    2300 5950
	0    1    1    0   
$EndComp
$Comp
L MicroController:CD74HC4051-EP_8-1AnalogMultiplexer BeforeResistor1
U 1 1 5D419E27
P 1150 6950
F 0 "BeforeResistor1" H 1425 7125 50  0000 C CNN
F 1 "CD74HC4051-EP_8-1AnalogMultiplexer" H 1425 7034 50  0000 C CNN
F 2 "CD74HC4051MT:SOIC127P600X175-16N" H 1150 6950 50  0001 C CNN
F 3 "" H 1150 6950 50  0001 C CNN
	1    1150 6950
	1    0    0    -1  
$EndComp
Text GLabel 950  7000 0    50   Input ~ 0
3V3_1_B
Text GLabel 950  7100 0    50   Input ~ 0
5V_1_B
Text GLabel 950  7200 0    50   Input ~ 0
3V3_2_B
Text GLabel 950  7300 0    50   Input ~ 0
5V_2_B
Text GLabel 950  7400 0    50   Input ~ 0
3V3_3_B
Text GLabel 950  7500 0    50   Input ~ 0
5V_3_B
Text GLabel 950  7600 0    50   Input ~ 0
3V3_4_B
Text GLabel 950  7700 0    50   Input ~ 0
5V_4_B
$Comp
L Device:R R4
U 1 1 5D419E36
P 2300 7300
F 0 "R4" V 2093 7300 50  0000 C CNN
F 1 "10K OHMS" V 2184 7300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2230 7300 50  0001 C CNN
F 3 "~" H 2300 7300 50  0001 C CNN
	1    2300 7300
	0    1    1    0   
$EndComp
Text GLabel 2450 5950 2    50   Output ~ 0
AfterResistorOut
Text GLabel 6050 1450 0    50   Input ~ 0
AfterResistorOut
Text GLabel 6050 1350 0    50   Input ~ 0
BeforeResistorOut
Text GLabel 2450 7300 2    50   Output ~ 0
BeforeResistorOut
Text GLabel 3600 1500 1    50   Output ~ 0
5V_IN
Text GLabel 10250 1750 3    50   Output ~ 0
5V_1_A
Text GLabel 1900 7600 2    50   Input ~ 0
GND
Text GLabel 1900 6250 2    50   Input ~ 0
GND
Text GLabel 2300 2150 2    50   Output ~ 0
GND
Wire Wire Line
	9100 1000 9500 1000
Wire Wire Line
	9000 1400 9400 1400
Text GLabel 9750 1000 1    50   Output ~ 0
3V3_1_B
Text GLabel 9750 1750 3    50   Output ~ 0
5V_1_B
Wire Wire Line
	9300 1300 9100 1300
Wire Wire Line
	9200 1700 9000 1700
Text GLabel 9100 1300 0    50   Input ~ 0
GND
Text GLabel 9000 1700 0    50   Input ~ 0
GND
Wire Wire Line
	7100 950  7100 1050
Text GLabel 7100 950  2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 5D468695
P 2400 800
F 0 "J1" V 2340 612 50  0000 R CNN
F 1 "GND/5V_IN/3V_IN" V 2249 612 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2400 800 50  0001 C CNN
F 3 "~" H 2400 800 50  0001 C CNN
	1    2400 800 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 1000 2300 1650
Wire Wire Line
	2400 1000 2400 1500
Text GLabel 6300 950  0    50   Input ~ 0
AfterResistorOut
Text GLabel 1900 7700 2    50   Input ~ 0
5V_IN
Text GLabel 1900 6350 2    50   Input ~ 0
5V_IN
Wire Wire Line
	2150 5950 1900 5950
Wire Wire Line
	2150 7300 1900 7300
Wire Wire Line
	2500 1000 2500 1250
Wire Wire Line
	2500 1250 2600 1250
Text GLabel 3050 1250 2    50   Output ~ 0
3V_IN
Wire Wire Line
	1900 7600 1900 7500
Wire Wire Line
	1900 7500 1900 7400
Connection ~ 1900 7500
Wire Wire Line
	1900 6250 1900 6150
Wire Wire Line
	1900 6150 1900 6050
Connection ~ 1900 6150
Wire Wire Line
	6050 1350 6300 1350
Wire Wire Line
	6050 1450 6300 1450
$Comp
L pspice:CAP C2
U 1 1 5D4A3F80
P 7100 1900
F 0 "C2" H 6800 1950 50  0000 L CNN
F 1 "0.1 uF" H 6750 1800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 7100 1900 50  0001 C CNN
F 3 "" H 7100 1900 50  0001 C CNN
	1    7100 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 1750 6700 1900
Wire Wire Line
	6700 1900 6850 1900
Text GLabel 6700 1900 3    50   Input ~ 0
GND
Text GLabel 6700 750  1    50   Input ~ 0
3V_IN
Wire Wire Line
	7350 1900 7900 1900
Wire Wire Line
	7900 1900 7900 1500
Wire Wire Line
	7900 750  6700 750 
$Comp
L Device:R R11
U 1 1 5D4B35E2
P 7550 1100
F 0 "R11" V 7343 1100 50  0000 C CNN
F 1 "10K OHMS" V 7434 1100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 7480 1100 50  0001 C CNN
F 3 "~" H 7550 1100 50  0001 C CNN
	1    7550 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5D4B36DB
P 7550 1500
F 0 "R12" V 7700 1500 50  0000 C CNN
F 1 "10K OHMS" V 7650 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 7480 1500 50  0001 C CNN
F 3 "~" H 7550 1500 50  0001 C CNN
	1    7550 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 1100 7200 1100
Wire Wire Line
	7200 1100 7200 1250
Connection ~ 7200 1250
Wire Wire Line
	7200 1250 7350 1250
Wire Wire Line
	7400 1500 7200 1500
Wire Wire Line
	7200 1500 7200 1350
Connection ~ 7200 1350
Wire Wire Line
	7200 1350 7350 1350
Wire Wire Line
	7700 1100 7900 1100
Connection ~ 7900 1100
Wire Wire Line
	7900 1100 7900 750 
Wire Wire Line
	7700 1500 7900 1500
Connection ~ 7900 1500
Wire Wire Line
	7900 1500 7900 1100
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5D4C7DD5
P 2600 1250
F 0 "#FLG0103" H 2600 1325 50  0001 C CNN
F 1 "PWR_FLAG" H 2600 1424 50  0000 C CNN
F 2 "" H 2600 1250 50  0001 C CNN
F 3 "~" H 2600 1250 50  0001 C CNN
	1    2600 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1250 3050 1250
Connection ~ 2600 1250
Text GLabel 5050 3600 2    50   Output ~ 0
3V3_1_SWITCH
Text GLabel 5050 3700 2    50   Output ~ 0
5V_1_SWITCH
Text GLabel 5050 3800 2    50   Output ~ 0
3V3_2_SWITCH
Text GLabel 5050 3900 2    50   Output ~ 0
5V_2_SWITCH
Text GLabel 5050 4000 2    50   Output ~ 0
3V3_3_SWITCH
Text GLabel 5050 4100 2    50   Output ~ 0
5V_3_SWITCH
Text GLabel 5050 4200 2    50   Output ~ 0
3V3_4_SWITCH
Text GLabel 5050 4300 2    50   Output ~ 0
5V_4_SWITCH
Text GLabel 9100 1000 0    50   Input ~ 0
3V3_1_SWITCH
Text GLabel 9000 1400 0    50   Input ~ 0
5V_1_SWITCH
Text GLabel 9100 2450 0    50   Input ~ 0
3V3_2_SWITCH
Text GLabel 9050 2900 0    50   Input ~ 0
5V_2_SWITCH
Text GLabel 9100 3900 0    50   Input ~ 0
3V3_3_SWITCH
Text GLabel 9000 4400 0    50   Input ~ 0
5V_3_SWITCH
Text GLabel 9050 5250 2    50   Input ~ 0
3V3_4_SWITCH
Wire Wire Line
	10450 2800 10450 2750
Wire Wire Line
	10450 2900 10450 2950
Text GLabel 10250 2450 1    50   Output ~ 0
3V3_2_A
Text GLabel 10250 3200 3    50   Output ~ 0
5V_2_A
Wire Wire Line
	9100 2450 9500 2450
Wire Wire Line
	9050 2900 9450 2900
Text GLabel 9750 2450 1    50   Output ~ 0
3V3_2_B
Text GLabel 9750 3200 3    50   Output ~ 0
5V_2_B
Wire Wire Line
	9300 2750 9100 2750
Wire Wire Line
	9250 3200 9050 3200
Text GLabel 9100 2750 0    50   Input ~ 0
3V_IN
Text GLabel 9050 3200 0    50   Input ~ 0
5V_IN
Wire Wire Line
	10450 4250 10450 4200
Wire Wire Line
	10450 4350 10450 4400
Text GLabel 10250 3900 1    50   Output ~ 0
3V3_3_A
Text GLabel 10250 4650 3    50   Output ~ 0
5V_3_A
Wire Wire Line
	9100 3900 9500 3900
Wire Wire Line
	9000 4400 9400 4400
Text GLabel 9750 3900 1    50   Output ~ 0
3V3_3_B
Text GLabel 9750 4650 3    50   Output ~ 0
5V_3_B
Wire Wire Line
	9300 4200 9100 4200
Wire Wire Line
	9200 4700 9000 4700
Text GLabel 9100 4200 0    50   Input ~ 0
3V_IN
Text GLabel 9000 4700 0    50   Input ~ 0
5V_IN
Wire Wire Line
	10300 6050 10300 6000
Wire Wire Line
	10300 6150 10300 6200
Text GLabel 10100 5700 1    50   Output ~ 0
3V3_4_A
Text GLabel 10100 6450 3    50   Output ~ 0
5V_4_A
Text GLabel 9600 5700 1    50   Output ~ 0
3V3_4_B
Text GLabel 9600 6450 3    50   Output ~ 0
5V_4_B
Text GLabel 8000 6000 0    50   Input ~ 0
3V_IN
Wire Wire Line
	10150 1300 10450 1300
Wire Wire Line
	9700 1300 9850 1300
Wire Wire Line
	9750 1200 9750 1100
Wire Wire Line
	10250 1200 10250 1100
Wire Wire Line
	10150 1500 10450 1500
Wire Wire Line
	10250 1600 10250 1700
Wire Wire Line
	9750 1600 9750 1700
Wire Wire Line
	10150 2750 10450 2750
Wire Wire Line
	9700 2750 9850 2750
Wire Wire Line
	10150 2950 10450 2950
Wire Wire Line
	10250 2650 10250 2550
Wire Wire Line
	9750 2650 9750 2550
Wire Wire Line
	10250 3050 10250 3150
Wire Wire Line
	9750 3050 9750 3150
Wire Wire Line
	10150 4200 10450 4200
Wire Wire Line
	9700 4200 9850 4200
Wire Wire Line
	10250 4100 10250 4000
Wire Wire Line
	9750 4100 9750 4000
Wire Wire Line
	10150 4400 10450 4400
Wire Wire Line
	10250 4500 10250 4600
Wire Wire Line
	9750 4500 9750 4600
Wire Wire Line
	10000 6000 10300 6000
Wire Wire Line
	10000 6200 10300 6200
Wire Wire Line
	10100 5700 10100 5800
Wire Wire Line
	10100 5900 10000 5900
Wire Wire Line
	9600 5700 9600 5800
Wire Wire Line
	9600 5900 9700 5900
Wire Wire Line
	9600 6450 9600 6400
Wire Wire Line
	9600 6300 9700 6300
Wire Wire Line
	10100 6300 10100 6400
Text GLabel 1900 5650 2    50   Input ~ 0
SEL0
Text GLabel 1900 5750 2    50   Input ~ 0
SEL1
Text GLabel 1900 5850 2    50   Input ~ 0
SEL2
Text GLabel 1900 7000 2    50   Input ~ 0
SEL0
Text GLabel 1900 7100 2    50   Input ~ 0
SEL1
Text GLabel 1900 7200 2    50   Input ~ 0
SEL2
Text GLabel 5050 1800 2    50   Output ~ 0
SEL0
Text GLabel 5050 1900 2    50   Output ~ 0
SEL1
Text GLabel 5050 2000 2    50   Output ~ 0
SEL2
Text GLabel 7100 1550 2    50   Output ~ 0
ALERT
Text GLabel 3850 4900 0    50   Input ~ 0
ALERT
$Comp
L Connector:Conn_01x02_Female J7
U 1 1 5D46656E
P 2700 4600
F 0 "J7" H 2594 4275 50  0000 C CNN
F 1 "TX RX serial lines" H 2594 4366 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2700 4600 50  0001 C CNN
F 3 "~" H 2700 4600 50  0001 C CNN
	1    2700 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 4500 2900 4500
Wire Wire Line
	3850 4600 2900 4600
Text Label 3350 4600 0    50   ~ 0
PDO
Text Label 3350 4500 0    50   ~ 0
PDI
Wire Wire Line
	9250 6000 9700 6000
$Comp
L Device:R R1
U 1 1 5D52CDD5
P 8200 5850
F 0 "R1" H 8270 5896 50  0000 L CNN
F 1 "R" H 8270 5805 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 8130 5850 50  0001 C CNN
F 3 "~" H 8200 5850 50  0001 C CNN
	1    8200 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D52CEA2
P 7700 6850
F 0 "R2" H 7770 6896 50  0000 L CNN
F 1 "R" H 7770 6805 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 7630 6850 50  0001 C CNN
F 3 "~" H 7700 6850 50  0001 C CNN
	1    7700 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 6000 8550 6000
Wire Wire Line
	8750 5700 8200 5700
$Comp
L Device:C C1
U 1 1 5D546854
P 9250 5850
F 0 "C1" H 9135 5804 50  0000 R CNN
F 1 "C" H 9135 5895 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 9288 5700 50  0001 C CNN
F 3 "~" H 9250 5850 50  0001 C CNN
	1    9250 5850
	-1   0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 5D546A3D
P 8750 6850
F 0 "C3" H 8865 6896 50  0000 L CNN
F 1 "C" H 8865 6805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8788 6700 50  0001 C CNN
F 3 "~" H 8750 6850 50  0001 C CNN
	1    8750 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5D547A70
P 10000 1100
F 0 "C6" V 9748 1100 50  0000 C CNN
F 1 "C" V 9839 1100 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 10038 950 50  0001 C CNN
F 3 "~" H 10000 1100 50  0001 C CNN
	1    10000 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 1100 9850 1100
Connection ~ 9750 1100
Wire Wire Line
	9750 1100 9750 1000
Wire Wire Line
	10150 1100 10250 1100
Connection ~ 10250 1100
Wire Wire Line
	10250 1100 10250 1000
$Comp
L Device:C C7
U 1 1 5D550BE3
P 10000 1700
F 0 "C7" H 9748 1700 50  0000 C CNN
F 1 "C" V 9839 1700 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 10038 1550 50  0001 C CNN
F 3 "~" H 10000 1700 50  0001 C CNN
	1    10000 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10250 1700 10150 1700
Wire Wire Line
	9850 1700 9750 1700
Connection ~ 9750 1700
Wire Wire Line
	9750 1700 9750 1750
Connection ~ 10250 1700
Wire Wire Line
	10250 1700 10250 1750
$Comp
L Device:C C8
U 1 1 5D56E419
P 10000 2550
F 0 "C8" V 9748 2550 50  0000 C CNN
F 1 "C" V 9839 2550 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 10038 2400 50  0001 C CNN
F 3 "~" H 10000 2550 50  0001 C CNN
	1    10000 2550
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 5D56E4F2
P 10000 4000
F 0 "C10" V 9748 4000 50  0000 C CNN
F 1 "C" V 9839 4000 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 10038 3850 50  0001 C CNN
F 3 "~" H 10000 4000 50  0001 C CNN
	1    10000 4000
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5D582692
P 10000 3150
F 0 "C9" V 10252 3150 50  0000 C CNN
F 1 "C" V 10161 3150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 10038 3000 50  0001 C CNN
F 3 "~" H 10000 3150 50  0001 C CNN
	1    10000 3150
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5D59115B
P 10000 4600
F 0 "C11" V 10252 4600 50  0000 C CNN
F 1 "C" V 10161 4600 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 10038 4450 50  0001 C CNN
F 3 "~" H 10000 4600 50  0001 C CNN
	1    10000 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 2550 10250 2550
Connection ~ 10250 2550
Wire Wire Line
	10250 2550 10250 2450
Wire Wire Line
	9850 2550 9750 2550
Connection ~ 9750 2550
Wire Wire Line
	9750 2550 9750 2450
Wire Wire Line
	10150 3150 10250 3150
Connection ~ 10250 3150
Wire Wire Line
	10250 3150 10250 3200
Wire Wire Line
	9850 3150 9750 3150
Connection ~ 9750 3150
Wire Wire Line
	9750 3150 9750 3200
Wire Wire Line
	10150 4000 10250 4000
Connection ~ 10250 4000
Wire Wire Line
	10250 4000 10250 3900
Wire Wire Line
	9850 4000 9750 4000
Connection ~ 9750 4000
Wire Wire Line
	9750 4000 9750 3900
Wire Wire Line
	10150 4600 10250 4600
Connection ~ 10250 4600
Wire Wire Line
	10250 4600 10250 4650
Wire Wire Line
	9850 4600 9750 4600
Connection ~ 9750 4600
Wire Wire Line
	9750 4600 9750 4650
$Comp
L Device:C C4
U 1 1 5D5C1E0C
P 9850 5800
F 0 "C4" V 9598 5800 50  0000 C CNN
F 1 "C" V 9689 5800 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 9888 5650 50  0001 C CNN
F 3 "~" H 9850 5800 50  0001 C CNN
	1    9850 5800
	0    1    1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 5D5C7BFD
P 9850 6400
F 0 "C5" V 10102 6400 50  0000 C CNN
F 1 "C" V 10011 6400 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 9888 6250 50  0001 C CNN
F 3 "~" H 9850 6400 50  0001 C CNN
	1    9850 6400
	0    1    1    0   
$EndComp
Wire Wire Line
	10000 5800 10100 5800
Connection ~ 10100 5800
Wire Wire Line
	10100 5800 10100 5900
Wire Wire Line
	9700 5800 9600 5800
Connection ~ 9600 5800
Wire Wire Line
	9600 5800 9600 5900
Wire Wire Line
	9700 6400 9600 6400
Connection ~ 9600 6400
Wire Wire Line
	9600 6400 9600 6300
Wire Wire Line
	10000 6400 10100 6400
Connection ~ 10100 6400
Wire Wire Line
	10100 6400 10100 6450
Wire Wire Line
	9250 5700 8750 5700
Connection ~ 8750 5700
Wire Wire Line
	9250 6000 8950 6000
Connection ~ 9250 6000
Text GLabel 8750 5300 1    50   Input ~ 0
GND
Wire Wire Line
	8000 6000 8200 6000
Connection ~ 8200 6000
$Comp
L Device:R R7
U 1 1 5D65F628
P 10000 1300
F 0 "R7" V 9793 1300 50  0000 C CNN
F 1 "SENSE" V 9884 1300 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9930 1300 50  0001 C CNN
F 3 "~" H 10000 1300 50  0001 C CNN
	1    10000 1300
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5D6602CF
P 10000 1500
F 0 "R8" V 9793 1500 50  0000 C CNN
F 1 "SENSE" V 9884 1500 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9930 1500 50  0001 C CNN
F 3 "~" H 10000 1500 50  0001 C CNN
	1    10000 1500
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5D660428
P 10000 2750
F 0 "R9" V 9793 2750 50  0000 C CNN
F 1 "SENSE" V 9884 2750 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9930 2750 50  0001 C CNN
F 3 "~" H 10000 2750 50  0001 C CNN
	1    10000 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5D6604FB
P 10000 2950
F 0 "R10" V 9793 2950 50  0000 C CNN
F 1 "SENSE" V 9884 2950 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9930 2950 50  0001 C CNN
F 3 "~" H 10000 2950 50  0001 C CNN
	1    10000 2950
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5D6605D7
P 10000 4200
F 0 "R13" V 9793 4200 50  0000 C CNN
F 1 "SENSE" V 9884 4200 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9930 4200 50  0001 C CNN
F 3 "~" H 10000 4200 50  0001 C CNN
	1    10000 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5D660E93
P 10000 4400
F 0 "R14" V 9793 4400 50  0000 C CNN
F 1 "SENSE" V 9884 4400 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9930 4400 50  0001 C CNN
F 3 "~" H 10000 4400 50  0001 C CNN
	1    10000 4400
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5D661231
P 9850 6000
F 0 "R5" V 9643 6000 50  0000 C CNN
F 1 "SENSE" V 9734 6000 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9780 6000 50  0001 C CNN
F 3 "~" H 9850 6000 50  0001 C CNN
	1    9850 6000
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5D661320
P 9850 6200
F 0 "R6" V 9643 6200 50  0000 C CNN
F 1 "SENSE" V 9734 6200 50  0000 C CNN
F 2 "RESC3216X62:RESC3216X62" V 9780 6200 50  0001 C CNN
F 3 "~" H 9850 6200 50  0001 C CNN
	1    9850 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 4100 10150 4200
Wire Wire Line
	10150 4100 10250 4100
Connection ~ 10150 4200
Wire Wire Line
	9850 4100 9850 4200
Wire Wire Line
	9850 4100 9750 4100
Connection ~ 9850 4200
Wire Wire Line
	10150 4400 10150 4500
Connection ~ 10150 4400
Wire Wire Line
	10150 4500 10250 4500
Wire Wire Line
	9850 4400 9850 4500
Connection ~ 9850 4400
Wire Wire Line
	9850 4500 9750 4500
Wire Wire Line
	10000 6000 10000 5900
Connection ~ 10000 6000
Wire Wire Line
	9700 6000 9700 5900
Connection ~ 9700 6000
Wire Wire Line
	9700 6200 9700 6300
Wire Wire Line
	10000 6200 10000 6300
Connection ~ 10000 6200
Wire Wire Line
	10000 6300 10100 6300
Wire Wire Line
	10150 2750 10150 2650
Connection ~ 10150 2750
Wire Wire Line
	10150 2650 10250 2650
Wire Wire Line
	9850 2750 9850 2650
Connection ~ 9850 2750
Wire Wire Line
	9850 2650 9750 2650
Wire Wire Line
	10150 1500 10150 1600
Connection ~ 10150 1500
Wire Wire Line
	10150 1600 10250 1600
Wire Wire Line
	9850 1500 9850 1600
Connection ~ 9850 1500
Wire Wire Line
	9850 1600 9750 1600
Wire Wire Line
	10150 1300 10150 1200
Connection ~ 10150 1300
Wire Wire Line
	10150 1200 10250 1200
Wire Wire Line
	9850 1300 9850 1200
Connection ~ 9850 1300
Wire Wire Line
	9850 1200 9750 1200
Wire Wire Line
	10150 2950 10150 3050
Connection ~ 10150 2950
Wire Wire Line
	10150 3050 10250 3050
Wire Wire Line
	9850 2950 9850 3050
Connection ~ 9850 2950
Wire Wire Line
	9850 3050 9750 3050
$Comp
L Device:Q_PMOS_GDS Q10
U 1 1 5D7B458D
P 9500 4100
F 0 "Q10" V 9450 4050 50  0000 C CNN
F 1 "Q_PMOS_GDS" V 9350 4050 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9700 4200 50  0001 C CNN
F 3 "~" H 9500 4100 50  0001 C CNN
	1    9500 4100
	0    1    1    0   
$EndComp
$Comp
L Device:Q_PMOS_GDS Q8
U 1 1 5D7B48C1
P 9400 4600
F 0 "Q8" V 9650 4600 50  0000 C CNN
F 1 "Q_PMOS_GDS" V 9741 4600 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9600 4700 50  0001 C CNN
F 3 "~" H 9400 4600 50  0001 C CNN
	1    9400 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4700 9600 4400
Wire Wire Line
	9600 4400 9850 4400
$Comp
L Device:Q_NMOS_GDS Q9
U 1 1 5D7EC379
P 9500 1200
F 0 "Q9" V 9450 1100 50  0000 C CNN
F 1 "Q_NMOS_GDS" V 9350 1150 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9700 1300 50  0001 C CNN
F 3 "~" H 9500 1200 50  0001 C CNN
	1    9500 1200
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q7
U 1 1 5D7FCEE5
P 9400 1600
F 0 "Q7" V 9650 1600 50  0000 C CNN
F 1 "Q_NMOS_GDS" V 9741 1600 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9600 1700 50  0001 C CNN
F 3 "~" H 9400 1600 50  0001 C CNN
	1    9400 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 1700 9600 1500
Wire Wire Line
	9600 1500 9850 1500
$Comp
L Device:Q_NMOS_GDS Q6
U 1 1 5D82D580
P 8850 5500
F 0 "Q6" H 9056 5454 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 9056 5545 50  0000 L CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9050 5600 50  0001 C CNN
F 3 "~" H 8850 5500 50  0001 C CNN
	1    8850 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9050 5250 9050 5500
$Comp
L Device:Q_PMOS_GDS Q5
U 1 1 5D84F53A
P 8750 5900
F 0 "Q5" V 8750 6050 50  0000 C CNN
F 1 "Q_PMOS_GDS" V 8650 5900 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 8950 6000 50  0001 C CNN
F 3 "~" H 8750 5900 50  0001 C CNN
	1    8750 5900
	0    1    1    0   
$EndComp
Text GLabel 8550 6250 2    50   Input ~ 0
5V_4_SWITCH
Text GLabel 7500 7000 0    50   Input ~ 0
5V_IN
Wire Wire Line
	8250 6700 7700 6700
Wire Wire Line
	8750 6700 8250 6700
Connection ~ 8250 6700
Text GLabel 8250 6300 1    50   Input ~ 0
GND
$Comp
L Device:Q_NMOS_GDS Q2
U 1 1 5D84FD30
P 8350 6500
F 0 "Q2" H 8556 6454 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 8556 6545 50  0000 L CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 8550 6600 50  0001 C CNN
F 3 "~" H 8350 6500 50  0001 C CNN
	1    8350 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	8550 6250 8550 6500
$Comp
L Device:Q_PMOS_GDS Q1
U 1 1 5D84FD38
P 8250 6900
F 0 "Q1" V 8250 7050 50  0000 C CNN
F 1 "Q_PMOS_GDS" V 8150 6900 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 8450 7000 50  0001 C CNN
F 3 "~" H 8250 6900 50  0001 C CNN
	1    8250 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	8450 7000 8750 7000
Connection ~ 8750 7000
Wire Wire Line
	8750 7000 9200 7000
Wire Wire Line
	7500 7000 7700 7000
Connection ~ 7700 7000
Wire Wire Line
	7700 7000 8050 7000
Wire Wire Line
	9700 6200 9200 6200
Wire Wire Line
	9200 6200 9200 7000
Connection ~ 9700 6200
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5D4F8B12
P 11050 1450
F 0 "J4" H 11077 1426 50  0000 L CNN
F 1 "GND NMOS" H 11077 1335 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 11050 1450 50  0001 C CNN
F 3 "~" H 11050 1450 50  0001 C CNN
	1    11050 1450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 5D4F8C1A
P 11100 2900
F 0 "J6" H 11127 2876 50  0000 L CNN
F 1 "POS NMOS" H 11127 2785 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 11100 2900 50  0001 C CNN
F 3 "~" H 11100 2900 50  0001 C CNN
	1    11100 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J5
U 1 1 5D501B65
P 11050 4350
F 0 "J5" H 11078 4326 50  0000 L CNN
F 1 "POS PMOS" H 11078 4235 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 11050 4350 50  0001 C CNN
F 3 "~" H 11050 4350 50  0001 C CNN
	1    11050 4350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 5D50C034
P 10950 6150
F 0 "J3" H 10978 6126 50  0000 L CNN
F 1 "LOAD SWITCH" H 10978 6035 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 10950 6150 50  0001 C CNN
F 3 "~" H 10950 6150 50  0001 C CNN
	1    10950 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 1350 10850 1350
Wire Wire Line
	10450 1450 10850 1450
Wire Wire Line
	10450 2800 10900 2800
Wire Wire Line
	10450 2900 10900 2900
Wire Wire Line
	10450 4250 10850 4250
Wire Wire Line
	10450 4350 10850 4350
Text GLabel 10900 3000 0    50   BiDi ~ 0
SCL(I2C)
Text GLabel 10900 3100 0    50   BiDi ~ 0
SDA(I2C)
Text GLabel 10850 4450 0    50   BiDi ~ 0
SCL(I2C)
Text GLabel 10850 4550 0    50   BiDi ~ 0
SDA(I2C)
Text GLabel 10750 6250 0    50   BiDi ~ 0
SCL(I2C)
Text GLabel 10750 6350 0    50   BiDi ~ 0
SDA(I2C)
Text GLabel 10850 1550 0    50   BiDi ~ 0
SCL(I2C)
Text GLabel 10850 1650 0    50   BiDi ~ 0
SDA(I2C)
Wire Wire Line
	10300 6050 10750 6050
Wire Wire Line
	10300 6150 10750 6150
$Comp
L Connector:Conn_01x05_Female J8
U 1 1 5D5E2A5D
P 3650 3000
F 0 "J8" H 3850 2950 50  0000 C CNN
F 1 "PORT G" H 3850 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 3650 3000 50  0001 C CNN
F 3 "~" H 3650 3000 50  0001 C CNN
	1    3650 3000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J9
U 1 1 5D5E2D26
P 3650 4000
F 0 "J9" H 3850 4000 50  0000 C CNN
F 1 "PORT F" H 3850 4100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 3650 4000 50  0001 C CNN
F 3 "~" H 3650 4000 50  0001 C CNN
	1    3650 4000
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q3
U 1 1 5D5F7BDB
P 9500 2650
F 0 "Q3" V 9450 2550 50  0000 C CNN
F 1 "Q_NMOS_GDS" V 9350 2600 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9700 2750 50  0001 C CNN
F 3 "~" H 9500 2650 50  0001 C CNN
	1    9500 2650
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q4
U 1 1 5D6005BB
P 9450 3100
F 0 "Q4" V 9400 3000 50  0000 C CNN
F 1 "Q_NMOS_GDS" V 9300 3050 50  0000 C CNN
F 2 "TO254P1052X465X1989-3:TO254P1052X465X1989-3" H 9650 3200 50  0001 C CNN
F 3 "~" H 9450 3100 50  0001 C CNN
	1    9450 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 2950 9650 3200
Wire Wire Line
	9650 2950 9850 2950
$EndSCHEMATC

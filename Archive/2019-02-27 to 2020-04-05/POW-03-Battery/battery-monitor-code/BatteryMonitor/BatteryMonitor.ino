#include <I2C.h>

// TODO
// change relevant functions to return unsigned int instead of int

// address of the BQ28Z610
const int IC_addr = 0x55; 

// addresses for each command, in order of address
// they are in pairs
// get time (min) before empty
const int TIME_LSB = 0x04;
const int TIME_MSB = 0x05;
// get temperature
const int TEMP_LSB = 0x06;
const int TEMP_MSB = 0x07;
// get voltage
const int VOLT_LSB = 0x08;
const int VOLT_MSB = 0x09;
// get current
const int CURR_LSB = 0x0C;
const int CURR_MSB = 0x0D;
// get remaining capacity
const int CAPC_LSB = 0x10;
const int CAPC_MSB = 0x11;
// get capacity when fully charged
const int FULL_CAPC_LSB = 0x12;
const int FULL_CAPC_MSB = 0x13;
// get time til empty at standby discharge rate 
const int STAND_TIME_LSB = 0x1C;
const int STAND_TIME_MSB = 0x1D;
// get average power
const int PWR_LSB = 0x22;
const int PWR_MSB = 0x23;
// get number of charge cycles
const int CYCLE_LSB = 0x2A;
const int CYCLE_MSB = 0x2B;
// get percentage charge
const int CHARGE_LSB = 0x2C;
const int CHARGE_MSB = 0x2D;
// get battery health, actual capacity / design capacity %
const int HEALTH_LSB = 0x2E;
const int HEALTH_MSB = 0x2F;
// get voltage while charging
const int CHARGE_VOLT_LSB = 0x30;
const int CHARGE_VOLT_MSB = 0x31;
// get current while charging
const int CHARGE_CURR_LSB = 0x32;
const int CHARGE_CURR_MSB = 0x33;

// takes the address pair of the instruction and the LSB & MSB
void getValues(int lsb_addr, int msb_addr, byte* lsb, byte* msb);

unsigned int getTimeRemain();

// return temperature in celsius
unsigned int getTemp();
unsigned int getVoltage();
int getCurrent();
unsigned int getCapacity();
unsigned int getFullCapacity();
unsigned int getStandbyTime();
int getAveragePower();
unsigned int getCycles();
unsigned int getChargePercent();
unsigned int getHealth();
int getChargeVoltage();
int getChargeCurrent();

void setup(){
  pinMode(A4, INPUT);      // define as input for now, will get redifined by I2C functions
  pinMode(A5, INPUT);      // define as input for now, will get redifined by I2C functions

  Serial.begin(115200);
  while(!Serial);

  I2c.begin();
}

void loop(){

  unsigned int temp = getTemp();

  delay(1000);

  unsigned int voltage = getVoltage();

  delay(1000);

  int current = getCurrent();

  delay(1000);

  unsigned int timeRemain = getTimeRemain();

  delay(1000);

  unsigned int capacity = getCapacity();

  delay(1000);

  unsigned int health = getHealth();

  delay(1000);

  unsigned int fullCapacity = getFullCapacity();

  delay(1000);

  unsigned int cycle = getCycles();

  delay(1000);

  unsigned int standbyTime = getStandbyTime();

  delay(1000);

  int power = getAveragePower();

  delay(1000);

  unsigned int charge = getChargePercent();

  delay(1000);

  int chargeVolt = getChargeVoltage();

  delay(1000);

  int chargeCurrent = getChargeCurrent();

  delay(1000);
}

void getValues(int lsbAddr, int msbAddr, byte* lsb, byte* msb) {
  int returnValue = 0;

  // get the least significant bit
  returnValue = I2c.write(IC_addr, lsbAddr);

  if (returnValue == 0) {
    I2c.read(IC_addr, lsbAddr, 1);
    *lsb = I2c.receive();
  }

  //get the most significant bit
  returnValue = I2c.write(IC_addr, msbAddr);

  if (returnValue == 0) {
    I2c.read(IC_addr, msbAddr, 1);
    *msb = I2c.receive();
  }
}

unsigned int getTimeRemain() {
  byte LSB = 0, MSB = 0;

  getValues(TIME_LSB, TIME_MSB, &LSB, &MSB);

  unsigned int timeRemain = MSB*256 + LSB;

  Serial.print("TimeRemain(min): ");
  Serial.println(timeRemain);

  return timeRemain;
}

unsigned int getTemp() {
  byte LSB = 0, MSB = 0;

  getValues(TEMP_LSB, TEMP_MSB, &LSB, &MSB);

  Serial.print("LSB:");
  Serial.println(LSB, DEC);
  Serial.print("MSB:");
  Serial.println(MSB, DEC);

  unsigned int temp = MSB*256 + LSB;

  Serial.print("Temperature(K): ");
  Serial.println(temp/10);
  Serial.print("Temperature(C): ");
  Serial.println(temp/10 - 272);

  return temp/10 - 272;
}

unsigned int getVoltage() {
  byte LSB = 0, MSB = 0;

  getValues(VOLT_LSB, VOLT_MSB, &LSB, &MSB);

  unsigned int volt = MSB*256 + LSB;

  Serial.print("Voltage(mV): ");
  Serial.println(volt);

  return volt;
}

int getCurrent() {
  byte LSB = 0, MSB = 0;

  getValues(CURR_LSB, CURR_MSB, &LSB, &MSB);

  int current = MSB*256 + LSB;

  Serial.print("Current(mA): ");
  Serial.println(current);

  return current;
}

unsigned int getCapacity() {
  byte LSB = 0, MSB = 0;

  getValues(CAPC_LSB, CAPC_MSB, &LSB, &MSB);

  unsigned int capacity = MSB*256 + LSB;

  Serial.print("Capacity(mAh): ");
  Serial.println(capacity);

  return capacity;
}

unsigned int getFullCapacity() {
  byte LSB = 0, MSB = 0;

  getValues(FULL_CAPC_LSB, FULL_CAPC_MSB, &LSB, &MSB);

  unsigned int fullCapacity = MSB*256 + LSB;

  Serial.print("Full Capacity(mAh): ");
  Serial.println(fullCapacity);

  return fullCapacity;
}

unsigned int getStandbyTime() {
  byte LSB = 0, MSB = 0;

  getValues(STAND_TIME_LSB, STAND_TIME_MSB, &LSB, &MSB);

  unsigned int standbyTime = MSB*256 + LSB;

  Serial.print("Standby Time: ");
  Serial.println(standbyTime);

  return standbyTime;
}

int getAveragePower() {
  byte LSB = 0, MSB = 0;

  getValues(PWR_LSB, PWR_MSB, &LSB, &MSB);

  int power = MSB*256 + LSB;

  Serial.print("Average Power(W): ");
  Serial.println(power);

  return power;
}

unsigned int getCycles() {
  byte LSB = 0, MSB = 0;

  getValues(CYCLE_LSB, CYCLE_MSB, &LSB, &MSB);

  unsigned int cycle = MSB*256 + LSB;

  Serial.print("Charge cycles: ");
  Serial.println(cycle);

  return cycle;
}

unsigned int getChargePercent() {
  byte LSB = 0, MSB = 0;

  getValues(CHARGE_LSB, CHARGE_MSB, &LSB, &MSB);

  unsigned int charge = MSB*256 + LSB;

  Serial.print("Charge Percent(%): ");
  Serial.println(charge);

  return charge;
}

unsigned int getHealth() {
  byte LSB = 0, MSB = 0;

  getValues(HEALTH_LSB, HEALTH_MSB, &LSB, &MSB);

  unsigned int health = MSB*256 + LSB;

  Serial.print("Health(%): ");
  Serial.println(health);

  return health;
}

int getChargeVoltage() {
  byte LSB = 0, MSB = 0;

  getValues(CHARGE_VOLT_LSB, CHARGE_VOLT_MSB, &LSB, &MSB);

  int voltage = MSB*256 + LSB;

  Serial.print("Charge Voltage(mV): ");
  Serial.println(voltage);

  return voltage;
}

int getChargeCurrent() {
  byte LSB = 0, MSB = 0;

  getValues(CHARGE_CURR_LSB, CHARGE_CURR_MSB, &LSB, &MSB);

  int current = MSB*256 + LSB;

  Serial.print("Charge Current(mA): ");
  Serial.println(current);

  return current;
}
































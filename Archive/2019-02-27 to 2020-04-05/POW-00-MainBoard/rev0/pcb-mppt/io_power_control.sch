EESchema Schematic File Version 4
LIBS:control_board_combination-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x03_Male J_I2C?
U 1 1 5C83AE04
P 6300 2750
AR Path="/5C83AE04" Ref="J_I2C?"  Part="1" 
AR Path="/5C8547B3/5C83AE04" Ref="J_I2C?"  Part="1" 
AR Path="/5C83AC92/5C83AE04" Ref="J_I2C1"  Part="1" 
F 0 "J_I2C1" H 6273 2773 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6273 2682 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6300 2750 50  0001 C CNN
F 3 "~" H 6300 2750 50  0001 C CNN
	1    6300 2750
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R_SCL?
U 1 1 5C83AE0B
P 5500 2350
AR Path="/5C83AE0B" Ref="R_SCL?"  Part="1" 
AR Path="/5C8547B3/5C83AE0B" Ref="R_SCL?"  Part="1" 
AR Path="/5C83AC92/5C83AE0B" Ref="R_SCL1"  Part="1" 
F 0 "R_SCL1" H 5559 2396 50  0000 L CNN
F 1 "4k7" H 5559 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5500 2350 50  0001 C CNN
F 3 "~" H 5500 2350 50  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R_SDA?
U 1 1 5C83AE12
P 5900 2350
AR Path="/5C83AE12" Ref="R_SDA?"  Part="1" 
AR Path="/5C8547B3/5C83AE12" Ref="R_SDA?"  Part="1" 
AR Path="/5C83AC92/5C83AE12" Ref="R_SDA1"  Part="1" 
F 0 "R_SDA1" H 5959 2396 50  0000 L CNN
F 1 "4v7" H 5959 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5900 2350 50  0001 C CNN
F 3 "~" H 5900 2350 50  0001 C CNN
	1    5900 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2650 5500 2650
Wire Wire Line
	5500 2650 5500 2450
Wire Wire Line
	5500 2650 5050 2650
Connection ~ 5500 2650
Wire Wire Line
	6100 2750 5900 2750
Wire Wire Line
	5900 2750 5900 2450
Wire Wire Line
	5900 2750 5050 2750
Connection ~ 5900 2750
$Comp
L Connector:Conn_01x02_Male J_OUT?
U 1 1 5C83AE21
P 3550 1200
AR Path="/5C83AE21" Ref="J_OUT?"  Part="1" 
AR Path="/5C8547B3/5C83AE21" Ref="J_OUT?"  Part="1" 
AR Path="/5C83AC92/5C83AE21" Ref="J_OUT1"  Part="1" 
F 0 "J_OUT1" H 3522 1173 50  0000 R CNN
F 1 "5V_OUT" H 3522 1082 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3550 1200 50  0001 C CNN
F 3 "~" H 3550 1200 50  0001 C CNN
	1    3550 1200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R_LED_IN?
U 1 1 5C83AE28
P 2700 1750
AR Path="/5C83AE28" Ref="R_LED_IN?"  Part="1" 
AR Path="/5C8547B3/5C83AE28" Ref="R_LED_IN?"  Part="1" 
AR Path="/5C83AC92/5C83AE28" Ref="R_LED_IN1"  Part="1" 
F 0 "R_LED_IN1" H 2759 1796 50  0000 L CNN
F 1 "220R" H 2759 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2700 1750 50  0001 C CNN
F 3 "~" H 2700 1750 50  0001 C CNN
	1    2700 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R_LED_OUT?
U 1 1 5C83AE2F
P 3150 1750
AR Path="/5C83AE2F" Ref="R_LED_OUT?"  Part="1" 
AR Path="/5C8547B3/5C83AE2F" Ref="R_LED_OUT?"  Part="1" 
AR Path="/5C83AC92/5C83AE2F" Ref="R_LED_OUT1"  Part="1" 
F 0 "R_LED_OUT1" H 3209 1796 50  0000 L CNN
F 1 "220R" H 3209 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 1750 50  0001 C CNN
F 3 "~" H 3150 1750 50  0001 C CNN
	1    3150 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5C83AE36
P 2700 2200
AR Path="/5C83AE36" Ref="D?"  Part="1" 
AR Path="/5C8547B3/5C83AE36" Ref="D?"  Part="1" 
AR Path="/5C83AC92/5C83AE36" Ref="D5"  Part="1" 
F 0 "D5" V 2738 2083 50  0000 R CNN
F 1 "LED" V 2647 2083 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2700 2200 50  0001 C CNN
F 3 "~" H 2700 2200 50  0001 C CNN
	1    2700 2200
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C83AE3D
P 3150 2200
AR Path="/5C83AE3D" Ref="D?"  Part="1" 
AR Path="/5C8547B3/5C83AE3D" Ref="D?"  Part="1" 
AR Path="/5C83AC92/5C83AE3D" Ref="D7"  Part="1" 
F 0 "D7" V 3188 2083 50  0000 R CNN
F 1 "LED" V 3097 2083 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 2200 50  0001 C CNN
F 3 "~" H 3150 2200 50  0001 C CNN
	1    3150 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 1850 2700 2050
Wire Wire Line
	3150 2050 3150 1850
Text Label 2700 1650 0    50   ~ 0
5V_VCC
Text Label 1850 1200 0    50   ~ 0
5V_VCC
$Comp
L Connector:Conn_01x02_Male J1_INPUT?
U 1 1 5C83AE49
P 2050 1200
AR Path="/5C83AE49" Ref="J1_INPUT?"  Part="1" 
AR Path="/5C8547B3/5C83AE49" Ref="J1_INPUT?"  Part="1" 
AR Path="/5C83AC92/5C83AE49" Ref="J1_INPUT1"  Part="1" 
F 0 "J1_INPUT1" H 2023 1173 50  0000 R CNN
F 1 "Conn_01x02_Male" H 2023 1082 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2050 1200 50  0001 C CNN
F 3 "~" H 2050 1200 50  0001 C CNN
	1    2050 1200
	-1   0    0    -1  
$EndComp
Text Label 1850 1300 0    50   ~ 0
GND
Text Label 5050 2650 0    50   ~ 0
SCL
Text Label 5050 2750 0    50   ~ 0
SDA
Text Label 6100 2850 0    50   ~ 0
GND
Text Label 5500 2250 0    50   ~ 0
5V_VCC
Text Label 5900 2250 0    50   ~ 0
5V_VCC
Text Label 2700 2350 0    50   ~ 0
GND
Text Label 3150 2350 0    50   ~ 0
GND
Text Label 3150 1650 0    50   ~ 0
5V_CONTROL_OUT
$Comp
L Connector:Conn_01x02_Male J_OUT?
U 1 1 5C83B201
P 3550 2850
AR Path="/5C83B201" Ref="J_OUT?"  Part="1" 
AR Path="/5C8547B3/5C83B201" Ref="J_OUT?"  Part="1" 
AR Path="/5C83AC92/5C83B201" Ref="J_OUT2"  Part="1" 
F 0 "J_OUT2" H 3522 2823 50  0000 R CNN
F 1 "5V_OUT" H 3522 2732 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3550 2850 50  0001 C CNN
F 3 "~" H 3550 2850 50  0001 C CNN
	1    3550 2850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R_LED_IN?
U 1 1 5C83B208
P 2700 3350
AR Path="/5C83B208" Ref="R_LED_IN?"  Part="1" 
AR Path="/5C8547B3/5C83B208" Ref="R_LED_IN?"  Part="1" 
AR Path="/5C83AC92/5C83B208" Ref="R_LED_IN2"  Part="1" 
F 0 "R_LED_IN2" H 2759 3396 50  0000 L CNN
F 1 "220R" H 2759 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2700 3350 50  0001 C CNN
F 3 "~" H 2700 3350 50  0001 C CNN
	1    2700 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R_LED_OUT?
U 1 1 5C83B20F
P 3150 3350
AR Path="/5C83B20F" Ref="R_LED_OUT?"  Part="1" 
AR Path="/5C8547B3/5C83B20F" Ref="R_LED_OUT?"  Part="1" 
AR Path="/5C83AC92/5C83B20F" Ref="R_LED_OUT2"  Part="1" 
F 0 "R_LED_OUT2" H 3209 3396 50  0000 L CNN
F 1 "220R" H 3209 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 3350 50  0001 C CNN
F 3 "~" H 3150 3350 50  0001 C CNN
	1    3150 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5C83B216
P 2700 3800
AR Path="/5C83B216" Ref="D?"  Part="1" 
AR Path="/5C8547B3/5C83B216" Ref="D?"  Part="1" 
AR Path="/5C83AC92/5C83B216" Ref="D6"  Part="1" 
F 0 "D6" V 2738 3683 50  0000 R CNN
F 1 "LED" V 2647 3683 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2700 3800 50  0001 C CNN
F 3 "~" H 2700 3800 50  0001 C CNN
	1    2700 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5C83B21D
P 3150 3800
AR Path="/5C83B21D" Ref="D?"  Part="1" 
AR Path="/5C8547B3/5C83B21D" Ref="D?"  Part="1" 
AR Path="/5C83AC92/5C83B21D" Ref="D8"  Part="1" 
F 0 "D8" V 3188 3683 50  0000 R CNN
F 1 "LED" V 3097 3683 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 3800 50  0001 C CNN
F 3 "~" H 3150 3800 50  0001 C CNN
	1    3150 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 3450 2700 3650
Wire Wire Line
	3150 3650 3150 3450
Text Label 2700 3250 0    50   ~ 0
3P3V_VCC
Text Label 3350 2850 0    50   ~ 0
3P3V_CONTROL_OUT
Text Label 1950 2850 0    50   ~ 0
3P3V_VCC
$Comp
L Connector:Conn_01x02_Male J1_INPUT?
U 1 1 5C83B229
P 2150 2850
AR Path="/5C83B229" Ref="J1_INPUT?"  Part="1" 
AR Path="/5C8547B3/5C83B229" Ref="J1_INPUT?"  Part="1" 
AR Path="/5C83AC92/5C83B229" Ref="J1_INPUT2"  Part="1" 
F 0 "J1_INPUT2" H 2123 2823 50  0000 R CNN
F 1 "Conn_01x02_Male" H 2123 2732 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2150 2850 50  0001 C CNN
F 3 "~" H 2150 2850 50  0001 C CNN
	1    2150 2850
	-1   0    0    -1  
$EndComp
Text Label 1950 2950 0    50   ~ 0
GND
Text Label 3350 2950 0    50   ~ 0
GND
Text Label 2700 3950 0    50   ~ 0
GND
Text Label 3150 3950 0    50   ~ 0
GND
Text Label 3150 3250 0    50   ~ 0
3P3V_CONTROL_OUT
Text Label 3350 1200 0    50   ~ 0
5V_CONTROL_OUT
Text HLabel 1850 1200 0    50   Output ~ 0
5V_VCC
Text HLabel 1850 1300 0    50   Output ~ 0
GND
Text Label 3350 1300 0    50   ~ 0
GND
Text HLabel 3350 1200 0    50   Input ~ 0
5V_CONTROL_OUT
Text HLabel 1950 2850 0    50   Output ~ 0
3P3V_VCC
Text HLabel 3350 2850 0    50   Input ~ 0
3P3V_CONTROL_OUT
Text HLabel 5050 2650 0    50   Input ~ 0
SCL
Text HLabel 5050 2750 0    50   Input ~ 0
SDA
$EndSCHEMATC

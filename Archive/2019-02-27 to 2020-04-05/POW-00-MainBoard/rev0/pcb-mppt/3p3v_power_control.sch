EESchema Schematic File Version 4
LIBS:control_board_combination-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Analog_ADC:INA226 U?
U 1 1 5C8B71C7
P 4800 3050
AR Path="/5C8B71C7" Ref="U?"  Part="1" 
AR Path="/5C8547B3/5C8B71C7" Ref="U?"  Part="1" 
AR Path="/5C880DB9/5C8B71C7" Ref="U5"  Part="1" 
F 0 "U5" V 4754 2509 50  0000 R CNN
F 1 "INA226" V 4845 2509 50  0000 R CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 4850 3150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina226.pdf" H 5150 2950 50  0001 C CNN
	1    4800 3050
	0    -1   1    0   
$EndComp
$Comp
L control_board_combination-rescue:NC7SZ08-power_ic-ControlMk3_kicad-rescue U?
U 1 1 5C855FB9
P 3850 3900
AR Path="/5C855FB9" Ref="U?"  Part="1" 
AR Path="/5C8547B3/5C855FB9" Ref="U?"  Part="1" 
AR Path="/5C880DB9/5C855FB9" Ref="U4"  Part="1" 
F 0 "U4" H 3850 3975 50  0000 C CNN
F 1 "NC7SZ08" H 3850 3884 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3850 3900 50  0001 C CNN
F 3 "" H 3850 3900 50  0001 C CNN
	1    3850 3900
	1    0    0    -1  
$EndComp
$Comp
L control_board_combination-rescue:TCA9554-power_ic-ControlMk3_kicad-rescue U?
U 1 1 5C8B71C9
P 5150 4850
AR Path="/5C8B71C9" Ref="U?"  Part="1" 
AR Path="/5C8547B3/5C8B71C9" Ref="U?"  Part="1" 
AR Path="/5C880DB9/5C8B71C9" Ref="U6"  Part="1" 
F 0 "U6" H 5150 5015 50  0000 C CNN
F 1 "TCA9554" H 5150 4924 50  0000 C CNN
F 2 "Package_SO:SSOP-16_3.9x4.9mm_P0.635mm" H 5150 4850 50  0001 C CNN
F 3 "" H 5150 4850 50  0001 C CNN
	1    5150 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J_LOAD?
U 1 1 5C8B71CA
P 7200 2100
AR Path="/5C8B71CA" Ref="J_LOAD?"  Part="1" 
AR Path="/5C8547B3/5C8B71CA" Ref="J_LOAD?"  Part="1" 
AR Path="/5C880DB9/5C8B71CA" Ref="J_LOAD2"  Part="1" 
F 0 "J_LOAD2" H 7172 2073 50  0000 R CNN
F 1 "5V_OUT" H 7172 1982 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7200 2100 50  0001 C CNN
F 3 "~" H 7200 2100 50  0001 C CNN
	1    7200 2100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5C8B71CB
P 4700 4450
AR Path="/5C8B71CB" Ref="J?"  Part="1" 
AR Path="/5C8547B3/5C8B71CB" Ref="J?"  Part="1" 
AR Path="/5C880DB9/5C8B71CB" Ref="J2"  Part="1" 
F 0 "J2" V 4853 4490 50  0000 L CNN
F 1 "P_AND_SWITCH" V 4762 4490 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4700 4450 50  0001 C CNN
F 3 "~" H 4700 4450 50  0001 C CNN
	1    4700 4450
	0    1    -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J3_LED?
U 1 1 5C8592CC
P 3300 5150
AR Path="/5C8592CC" Ref="J3_LED?"  Part="1" 
AR Path="/5C8547B3/5C8592CC" Ref="J3_LED?"  Part="1" 
AR Path="/5C880DB9/5C8592CC" Ref="J3_LED2"  Part="1" 
F 0 "J3_LED2" H 3273 5123 50  0000 R CNN
F 1 "Conn_01x02_Male" H 3273 5032 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3300 5150 50  0001 C CNN
F 3 "~" H 3300 5150 50  0001 C CNN
	1    3300 5150
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J4_ALERT_IO?
U 1 1 5C855FEA
P 6200 5100
AR Path="/5C855FEA" Ref="J4_ALERT_IO?"  Part="1" 
AR Path="/5C8547B3/5C855FEA" Ref="J4_ALERT_IO?"  Part="1" 
AR Path="/5C880DB9/5C855FEA" Ref="J4_ALERT_IO3"  Part="1" 
F 0 "J4_ALERT_IO3" H 6173 5073 50  0000 R CNN
F 1 "Conn_01x02_Male" H 6173 4982 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6200 5100 50  0001 C CNN
F 3 "~" H 6200 5100 50  0001 C CNN
	1    6200 5100
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small R1_?
U 1 1 5C855FF8
P 3300 3800
AR Path="/5C855FF8" Ref="R1_?"  Part="1" 
AR Path="/5C8547B3/5C855FF8" Ref="R1_?"  Part="1" 
AR Path="/5C880DB9/5C855FF8" Ref="R1_2"  Part="1" 
F 0 "R1_2" H 3241 3754 50  0000 R CNN
F 1 "R_Small" H 3241 3845 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3300 3800 50  0001 C CNN
F 3 "~" H 3300 3800 50  0001 C CNN
	1    3300 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R3_?
U 1 1 5C855FFF
P 5900 2150
AR Path="/5C855FFF" Ref="R3_?"  Part="1" 
AR Path="/5C8547B3/5C855FFF" Ref="R3_?"  Part="1" 
AR Path="/5C880DB9/5C855FFF" Ref="R3_2"  Part="1" 
F 0 "R3_2" H 5841 2104 50  0000 R CNN
F 1 "R_Small" H 5841 2195 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5900 2150 50  0001 C CNN
F 3 "~" H 5900 2150 50  0001 C CNN
	1    5900 2150
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small Cbyp?
U 1 1 5C856006
P 4000 3250
AR Path="/5C856006" Ref="Cbyp?"  Part="1" 
AR Path="/5C8547B3/5C856006" Ref="Cbyp?"  Part="1" 
AR Path="/5C880DB9/5C856006" Ref="Cbyp2"  Part="1" 
F 0 "Cbyp2" H 4092 3296 50  0000 L CNN
F 1 "100nF" H 4092 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4000 3250 50  0001 C CNN
F 3 "~" H 4000 3250 50  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C8592CD
P 4350 3750
AR Path="/5C8592CD" Ref="C?"  Part="1" 
AR Path="/5C8547B3/5C8592CD" Ref="C?"  Part="1" 
AR Path="/5C880DB9/5C8592CD" Ref="C5"  Part="1" 
F 0 "C5" H 4442 3796 50  0000 L CNN
F 1 "10uF" H 4442 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4350 3750 50  0001 C CNN
F 3 "~" H 4350 3750 50  0001 C CNN
	1    4350 3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R_LED_CONT?
U 1 1 5C8592CE
P 8300 2950
AR Path="/5C8592CE" Ref="R_LED_CONT?"  Part="1" 
AR Path="/5C8547B3/5C8592CE" Ref="R_LED_CONT?"  Part="1" 
AR Path="/5C880DB9/5C8592CE" Ref="R_LED_CONT2"  Part="1" 
F 0 "R_LED_CONT2" H 8359 2996 50  0000 L CNN
F 1 "220R" H 8359 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8300 2950 50  0001 C CNN
F 3 "~" H 8300 2950 50  0001 C CNN
	1    8300 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5C856045
P 8300 3400
AR Path="/5C856045" Ref="D?"  Part="1" 
AR Path="/5C8547B3/5C856045" Ref="D?"  Part="1" 
AR Path="/5C880DB9/5C856045" Ref="D4"  Part="1" 
F 0 "D4" V 8338 3283 50  0000 R CNN
F 1 "LED" V 8247 3283 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8300 3400 50  0001 C CNN
F 3 "~" H 8300 3400 50  0001 C CNN
	1    8300 3400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R4_?
U 1 1 5C8B71D4
P 5900 2600
AR Path="/5C8B71D4" Ref="R4_?"  Part="1" 
AR Path="/5C8547B3/5C8B71D4" Ref="R4_?"  Part="1" 
AR Path="/5C880DB9/5C8B71D4" Ref="R4_2"  Part="1" 
F 0 "R4_2" H 5841 2554 50  0000 R CNN
F 1 "R_Small" H 5841 2645 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5900 2600 50  0001 C CNN
F 3 "~" H 5900 2600 50  0001 C CNN
	1    5900 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_PMOS_SGD Q1_?
U 1 1 5C8592D0
P 6500 1800
AR Path="/5C8592D0" Ref="Q1_?"  Part="1" 
AR Path="/5C8547B3/5C8592D0" Ref="Q1_?"  Part="1" 
AR Path="/5C880DB9/5C8592D0" Ref="Q1_4"  Part="1" 
F 0 "Q1_4" V 6750 1800 50  0000 C CNN
F 1 "Q_PMOS_SGD" V 6841 1800 50  0000 C CNN
F 2 "Package_SO:PowerPAK_SO-8_Single" H 6700 1900 50  0001 C CNN
F 3 "~" H 6500 1800 50  0001 C CNN
	1    6500 1800
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_DGS Q1_?
U 1 1 5C8592BD
P 5800 3300
AR Path="/5C8592BD" Ref="Q1_?"  Part="1" 
AR Path="/5C8547B3/5C8592BD" Ref="Q1_?"  Part="1" 
AR Path="/5C880DB9/5C8592BD" Ref="Q1_3"  Part="1" 
F 0 "Q1_3" H 6005 3346 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 6005 3255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 6000 3400 50  0001 C CNN
F 3 "~" H 5800 3300 50  0001 C CNN
	1    5800 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Shunt R_SENS_?
U 1 1 5C8592BE
P 4950 1900
AR Path="/5C8592BE" Ref="R_SENS_?"  Part="1" 
AR Path="/5C8547B3/5C8592BE" Ref="R_SENS_?"  Part="1" 
AR Path="/5C880DB9/5C8592BE" Ref="R_SENS_2"  Part="1" 
F 0 "R_SENS_2" V 4725 1900 50  0000 C CNN
F 1 "R_Shunt" V 4816 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4880 1900 50  0001 C CNN
F 3 "~" H 4950 1900 50  0001 C CNN
	1    4950 1900
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CF?
U 1 1 5C8592D1
P 4950 2250
AR Path="/5C8592D1" Ref="CF?"  Part="1" 
AR Path="/5C8547B3/5C8592D1" Ref="CF?"  Part="1" 
AR Path="/5C880DB9/5C8592D1" Ref="CF2"  Part="1" 
F 0 "CF2" V 4721 2250 50  0000 C CNN
F 1 "100nF" V 4812 2250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4950 2250 50  0001 C CNN
F 3 "~" H 4950 2250 50  0001 C CNN
	1    4950 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 2050 4850 2250
Wire Wire Line
	4900 2650 4900 2550
Wire Wire Line
	4900 2550 4850 2550
Wire Wire Line
	5000 2650 5000 2550
Wire Wire Line
	5000 2550 5050 2550
Wire Wire Line
	5050 2250 5050 2050
$Comp
L Device:R_Small R_HI?
U 1 1 5C8B71D9
P 4850 2400
AR Path="/5C8B71D9" Ref="R_HI?"  Part="1" 
AR Path="/5C8547B3/5C8B71D9" Ref="R_HI?"  Part="1" 
AR Path="/5C880DB9/5C8B71D9" Ref="R_HI2"  Part="1" 
F 0 "R_HI2" H 5000 2350 50  0000 R CNN
F 1 "10R" H 5100 2450 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4850 2400 50  0001 C CNN
F 3 "~" H 4850 2400 50  0001 C CNN
	1    4850 2400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R_LO?
U 1 1 5C8B71DA
P 5050 2400
AR Path="/5C8B71DA" Ref="R_LO?"  Part="1" 
AR Path="/5C8547B3/5C8B71DA" Ref="R_LO?"  Part="1" 
AR Path="/5C880DB9/5C8B71DA" Ref="R_LO2"  Part="1" 
F 0 "R_LO2" H 5109 2446 50  0000 L CNN
F 1 "10R" H 5109 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5050 2400 50  0001 C CNN
F 3 "~" H 5050 2400 50  0001 C CNN
	1    5050 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2550 4850 2500
Wire Wire Line
	4850 2300 4850 2250
Connection ~ 4850 2250
Wire Wire Line
	5050 2300 5050 2250
Connection ~ 5050 2250
Wire Wire Line
	5050 2550 5050 2500
Wire Wire Line
	5150 1900 5900 1900
Wire Wire Line
	5900 2050 5900 1900
Connection ~ 5900 1900
Wire Wire Line
	5900 1900 6300 1900
Wire Wire Line
	5900 3100 5900 2700
Wire Wire Line
	4250 4250 4600 4250
Wire Wire Line
	4700 4250 5600 4250
Wire Wire Line
	5600 4250 5600 3300
Wire Wire Line
	3450 4050 3300 4050
Wire Wire Line
	3300 4050 3300 3900
Wire Wire Line
	4250 4050 4250 3750
Wire Wire Line
	4250 3650 3300 3650
Wire Wire Line
	3300 3650 3300 3700
Wire Wire Line
	3300 3650 3200 3650
Connection ~ 3300 3650
Wire Wire Line
	3450 4150 3200 4150
Wire Wire Line
	4800 3450 4800 3600
Wire Wire Line
	4900 3450 4900 3600
Wire Wire Line
	4300 3050 4000 3050
Wire Wire Line
	4000 3050 4000 3150
Wire Wire Line
	4000 3050 3650 3050
Connection ~ 4000 3050
Text Label 3650 3050 2    50   ~ 0
5V_VCC
Text Label 3200 3650 2    50   ~ 0
5V_VCC
Wire Wire Line
	6700 1900 7000 1900
Wire Wire Line
	4650 5050 4650 5150
$Comp
L Device:R R_LED?
U 1 1 5C8592BF
P 3700 5350
AR Path="/5C8592BF" Ref="R_LED?"  Part="1" 
AR Path="/5C8547B3/5C8592BF" Ref="R_LED?"  Part="1" 
AR Path="/5C880DB9/5C8592BF" Ref="R_LED2"  Part="1" 
F 0 "R_LED2" V 3493 5350 50  0000 C CNN
F 1 "220R" V 3584 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3630 5350 50  0001 C CNN
F 3 "~" H 3700 5350 50  0001 C CNN
	1    3700 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 5350 4300 5350
Wire Wire Line
	4000 5350 3850 5350
Wire Wire Line
	3550 5350 3400 5350
$Comp
L Device:LED D?
U 1 1 5C8B71DC
P 4150 5350
AR Path="/5C8B71DC" Ref="D?"  Part="1" 
AR Path="/5C8547B3/5C8B71DC" Ref="D?"  Part="1" 
AR Path="/5C880DB9/5C8B71DC" Ref="D3"  Part="1" 
F 0 "D3" H 4142 5095 50  0000 C CNN
F 1 "LED" H 4142 5186 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4150 5350 50  0001 C CNN
F 3 "~" H 4150 5350 50  0001 C CNN
	1    4150 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 4950 5850 4950
$Comp
L Device:C C?
U 1 1 5C8592C0
P 5850 4800
AR Path="/5C8592C0" Ref="C?"  Part="1" 
AR Path="/5C8547B3/5C8592C0" Ref="C?"  Part="1" 
AR Path="/5C880DB9/5C8592C0" Ref="C7"  Part="1" 
F 0 "C7" H 5965 4846 50  0000 L CNN
F 1 "100nF" H 5965 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5888 4650 50  0001 C CNN
F 3 "~" H 5850 4800 50  0001 C CNN
	1    5850 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C8592C1
P 6250 4800
AR Path="/5C8592C1" Ref="C?"  Part="1" 
AR Path="/5C8547B3/5C8592C1" Ref="C?"  Part="1" 
AR Path="/5C880DB9/5C8592C1" Ref="C8"  Part="1" 
F 0 "C8" H 6365 4846 50  0000 L CNN
F 1 "10uF" H 6365 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6288 4650 50  0001 C CNN
F 3 "~" H 6250 4800 50  0001 C CNN
	1    6250 4800
	1    0    0    -1  
$EndComp
Connection ~ 6250 4950
Wire Wire Line
	6250 4950 6600 4950
Connection ~ 5850 4950
Wire Wire Line
	5850 4950 6250 4950
Wire Wire Line
	5650 5350 6200 5350
Wire Wire Line
	6200 5350 6200 5300
Wire Wire Line
	6300 5300 6300 5350
Wire Wire Line
	6300 5350 6600 5350
$Comp
L Connector:Conn_01x02_Male J4_ALERT_IO?
U 1 1 5C8592C4
P 6300 5650
AR Path="/5C8592C4" Ref="J4_ALERT_IO?"  Part="1" 
AR Path="/5C8547B3/5C8592C4" Ref="J4_ALERT_IO?"  Part="1" 
AR Path="/5C880DB9/5C8592C4" Ref="J4_ALERT_IO4"  Part="1" 
F 0 "J4_ALERT_IO4" H 6273 5623 50  0000 R CNN
F 1 "Conn_01x02_Male" H 6273 5532 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6300 5650 50  0001 C CNN
F 3 "~" H 6300 5650 50  0001 C CNN
	1    6300 5650
	0    1    -1   0   
$EndComp
Wire Wire Line
	5650 5450 6200 5450
Wire Wire Line
	6300 5450 6600 5450
$Comp
L Connector:Conn_01x02_Male J7_ALERT_CONTROL?
U 1 1 5C8592C5
P 2100 3850
AR Path="/5C8592C5" Ref="J7_ALERT_CONTROL?"  Part="1" 
AR Path="/5C8547B3/5C8592C5" Ref="J7_ALERT_CONTROL?"  Part="1" 
AR Path="/5C880DB9/5C8592C5" Ref="J7_ALERT_CONTROL2"  Part="1" 
F 0 "J7_ALERT_CONTROL2" H 2073 3823 50  0000 R CNN
F 1 "Conn_01x02_Male" H 2073 3732 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2100 3850 50  0001 C CNN
F 3 "~" H 2100 3850 50  0001 C CNN
	1    2100 3850
	0    -1   1    0   
$EndComp
Wire Wire Line
	2200 4050 3300 4050
Connection ~ 3300 4050
Wire Wire Line
	2100 4050 1850 4050
Connection ~ 4250 3750
Wire Wire Line
	4250 3750 4250 3650
Wire Wire Line
	3300 2050 3300 3650
$Comp
L Device:R R_LOAD?
U 1 1 5C8592C6
P 7000 2350
AR Path="/5C8592C6" Ref="R_LOAD?"  Part="1" 
AR Path="/5C8547B3/5C8592C6" Ref="R_LOAD?"  Part="1" 
AR Path="/5C880DB9/5C8592C6" Ref="R_LOAD2"  Part="1" 
F 0 "R_LOAD2" H 7070 2396 50  0000 L CNN
F 1 "2512R" H 7070 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6930 2350 50  0001 C CNN
F 3 "~" H 7000 2350 50  0001 C CNN
	1    7000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2100 7000 1900
Wire Wire Line
	7000 1900 7150 1900
Wire Wire Line
	8300 3050 8300 3250
NoConn ~ 5650 5650
NoConn ~ 5650 5550
NoConn ~ 4650 5550
NoConn ~ 4650 5450
NoConn ~ 4650 5250
$Comp
L Device:C_Small C?
U 1 1 5C8592C7
P 4350 4050
AR Path="/5C8592C7" Ref="C?"  Part="1" 
AR Path="/5C8547B3/5C8592C7" Ref="C?"  Part="1" 
AR Path="/5C880DB9/5C8592C7" Ref="C6"  Part="1" 
F 0 "C6" H 4442 4096 50  0000 L CNN
F 1 "100nF" H 4442 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4350 4050 50  0001 C CNN
F 3 "~" H 4350 4050 50  0001 C CNN
	1    4350 4050
	0    -1   -1   0   
$EndComp
Connection ~ 4250 4050
NoConn ~ 4500 2650
Wire Wire Line
	5900 2250 5900 2350
Wire Wire Line
	6500 1600 6150 1600
Wire Wire Line
	6150 1600 6150 2350
Wire Wire Line
	6150 2350 5900 2350
Connection ~ 5900 2350
Wire Wire Line
	5900 2350 5900 2500
Text HLabel 1150 1450 0    50   Input ~ 0
5V_VCC
Text HLabel 1150 1600 0    50   Input ~ 0
GND
Text HLabel 7150 1900 2    50   Output ~ 0
3P3V_CONTROL_OUT
Text HLabel 2200 2000 2    50   Input ~ 0
SCL
Text HLabel 2200 2100 2    50   Input ~ 0
SDA
Wire Wire Line
	1150 2000 2200 2000
Wire Wire Line
	1150 2100 2200 2100
$Comp
L Connector:Conn_01x02_Male J2_INT?
U 1 1 5C8592D5
P 1700 4550
AR Path="/5C8592D5" Ref="J2_INT?"  Part="1" 
AR Path="/5C8547B3/5C8592D5" Ref="J2_INT?"  Part="1" 
AR Path="/5C880DB9/5C8592D5" Ref="J2_INT2"  Part="1" 
F 0 "J2_INT2" H 1673 4523 50  0000 R CNN
F 1 "Conn_01x02_Male" H 1673 4432 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1700 4550 50  0001 C CNN
F 3 "~" H 1700 4550 50  0001 C CNN
	1    1700 4550
	-1   0    0    -1  
$EndComp
Text Label 1150 1450 0    50   ~ 0
5V_VCC
Text Label 1150 2000 0    50   ~ 0
SCL
Text Label 1150 2100 0    50   ~ 0
SDA
Text Label 3300 2050 0    50   ~ 0
5V_VCC
Text Label 1850 4050 0    50   ~ 0
ALERT_CONTROL
Text Label 1150 1600 0    50   ~ 0
GND
Text Label 1500 4550 0    50   ~ 0
ALERT_CONTROL
Text Label 1500 4650 0    50   ~ 0
GND
Text Label 3200 4150 0    50   ~ 0
5V_MANUAL
Connection ~ 7000 1900
Text Label 5100 3450 0    50   ~ 0
ALERT_CONTROL
Text Label 4800 3600 0    50   ~ 0
SDA
Text Label 4900 3600 0    50   ~ 0
SCL
Text Label 3450 4250 0    50   ~ 0
GND
Text Label 4000 3350 0    50   ~ 0
GND
Text Label 4450 3750 0    50   ~ 0
GND
Text Label 4450 4050 0    50   ~ 0
GND
Text Label 4650 5650 0    50   ~ 0
GND
Text Label 3300 5350 0    50   ~ 0
5V_VCC
Text Label 5650 5050 0    50   ~ 0
SDA
Text Label 5650 5150 0    50   ~ 0
SCL
Text Label 5650 5250 0    50   ~ 0
INT
Text Label 6600 4950 0    50   ~ 0
5V_VCC
Text Label 6600 5350 0    50   ~ 0
5V_ALERT
Text Label 6600 5450 0    50   ~ 0
5V_MANUAL
Text Label 8300 2850 0    50   ~ 0
5V_VCC
Text Label 8300 3550 0    50   ~ 0
5V_ALERT
Text Label 7000 2500 0    50   ~ 0
GND
Text Label 5900 3500 0    50   ~ 0
GND
Text Label 4650 5050 0    50   ~ 0
GND
Text Label 4650 4950 0    50   ~ 0
5V_VCC
Text Label 4500 3450 0    50   ~ 0
GND
Text Label 4600 3450 0    50   ~ 0
5V_VCC
Wire Wire Line
	4750 1900 4300 1900
Text Label 4300 1900 0    50   ~ 0
3P3V_VCC
Text HLabel 1150 1300 0    50   Input ~ 0
3P3V_VCC
Text Label 1150 1300 0    50   ~ 0
3P3V_VCC
NoConn ~ 5650 5250
Text Label 5850 4650 0    50   ~ 0
GND
Text Label 6250 4650 0    50   ~ 0
GND
Wire Wire Line
	5300 3050 5550 3050
Wire Wire Line
	5550 3050 5550 2800
Text Label 5550 2800 0    50   ~ 0
GND
$EndSCHEMATC

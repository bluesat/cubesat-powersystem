EESchema Schematic File Version 4
LIBS:main_board2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole MH1
U 1 1 5C70847C
P 3550 1100
F 0 "MH1" H 3650 1146 50  0000 L CNN
F 1 "MountingHole" H 3650 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 3550 1100 50  0001 C CNN
F 3 "~" H 3550 1100 50  0001 C CNN
	1    3550 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH3
U 1 1 5C70850E
P 5000 1100
F 0 "MH3" H 5100 1146 50  0000 L CNN
F 1 "MountingHole" H 5100 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 5000 1100 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH4
U 1 1 5C70857C
P 5700 1100
F 0 "MH4" H 5800 1146 50  0000 L CNN
F 1 "MountingHole" H 5800 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 5700 1100 50  0001 C CNN
F 3 "~" H 5700 1100 50  0001 C CNN
	1    5700 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH2
U 1 1 5C7085DC
P 4300 1100
F 0 "MH2" H 4400 1146 50  0000 L CNN
F 1 "MountingHole" H 4400 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 4300 1100 50  0001 C CNN
F 3 "~" H 4300 1100 50  0001 C CNN
	1    4300 1100
	1    0    0    -1  
$EndComp
$Comp
L main_board2-rescue:PCbus-SOLAR U1
U 1 1 5C713480
P 2100 1650
F 0 "U1" H 3141 1321 50  0000 L CNN
F 1 "PCbus" H 3141 1230 50  0000 L CNN
F 2 "PC BUS:BUS PC104" H 2100 1650 50  0001 C CNN
F 3 "" H 2100 1650 50  0001 C CNN
	1    2100 1650
	1    0    0    -1  
$EndComp
NoConn ~ 3650 2950
NoConn ~ 3650 2850
NoConn ~ 3650 2800
NoConn ~ 3650 2750
NoConn ~ 3650 2000
NoConn ~ 3400 3200
NoConn ~ 3350 3200
NoConn ~ 3000 1450
NoConn ~ 3050 1450
NoConn ~ 3200 1450
NoConn ~ 3300 1450
NoConn ~ 3400 1450
Wire Wire Line
	3650 1800 3850 1800
Wire Wire Line
	2150 1450 2150 1250
Wire Wire Line
	3350 1250 3350 1450
Wire Wire Line
	3150 1450 3150 1250
Connection ~ 3150 1250
Wire Wire Line
	3150 1250 3350 1250
Wire Wire Line
	2950 1450 2950 1250
Connection ~ 2950 1250
Wire Wire Line
	2950 1250 3150 1250
Wire Wire Line
	2750 1450 2750 1250
Connection ~ 2750 1250
Wire Wire Line
	2750 1250 2950 1250
Wire Wire Line
	2550 1450 2550 1250
Connection ~ 2550 1250
Wire Wire Line
	2550 1250 2750 1250
Wire Wire Line
	2350 1450 2350 1250
Wire Wire Line
	2150 1250 2350 1250
Connection ~ 2350 1250
Wire Wire Line
	2350 1250 2550 1250
Wire Wire Line
	2150 1250 1950 1250
Wire Wire Line
	1950 1250 1950 1400
Connection ~ 2150 1250
$Comp
L power:GND #PWR02
U 1 1 5C7BAF9B
P 1950 1400
F 0 "#PWR02" H 1950 1150 50  0001 C CNN
F 1 "GND" H 1955 1227 50  0000 C CNN
F 2 "" H 1950 1400 50  0001 C CNN
F 3 "" H 1950 1400 50  0001 C CNN
	1    1950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2100 3850 2100
Wire Wire Line
	3850 2100 3850 1800
Wire Wire Line
	3650 2900 3850 2900
Connection ~ 3850 2100
Wire Wire Line
	3650 2300 3850 2300
Connection ~ 3850 2300
Wire Wire Line
	3850 2300 3850 2100
Wire Wire Line
	3650 2500 3850 2500
Connection ~ 3850 2500
Wire Wire Line
	3850 2500 3850 2300
Wire Wire Line
	3650 2700 3850 2700
Wire Wire Line
	3850 2500 3850 2700
Connection ~ 3850 2700
Wire Wire Line
	3850 2700 3850 2900
Wire Wire Line
	2900 3200 2900 3400
Wire Wire Line
	3850 3400 3850 2900
Connection ~ 3850 2900
Wire Wire Line
	3300 3200 3300 3400
Connection ~ 3300 3400
Wire Wire Line
	3300 3400 3850 3400
$Comp
L power:GND #PWR03
U 1 1 5C7BC2F5
P 3850 3400
F 0 "#PWR03" H 3850 3150 50  0001 C CNN
F 1 "GND" H 3855 3227 50  0000 C CNN
F 2 "" H 3850 3400 50  0001 C CNN
F 3 "" H 3850 3400 50  0001 C CNN
	1    3850 3400
	1    0    0    -1  
$EndComp
Connection ~ 3850 3400
Wire Wire Line
	2750 3200 2750 3400
Wire Wire Line
	2750 3400 2800 3400
Connection ~ 2900 3400
Wire Wire Line
	2800 3200 2800 3400
Connection ~ 2800 3400
Wire Wire Line
	2800 3400 2900 3400
Wire Wire Line
	1900 1700 1700 1700
Wire Wire Line
	1700 1700 1700 1750
Wire Wire Line
	1900 2750 1700 2750
Connection ~ 1700 2750
Wire Wire Line
	1700 2750 1700 2950
Wire Wire Line
	1900 2700 1700 2700
Connection ~ 1700 2700
Wire Wire Line
	1700 2700 1700 2750
Wire Wire Line
	1900 2650 1700 2650
Connection ~ 1700 2650
Wire Wire Line
	1700 2650 1700 2700
Wire Wire Line
	1900 2600 1700 2600
Connection ~ 1700 2600
Wire Wire Line
	1700 2600 1700 2650
Wire Wire Line
	1900 1750 1700 1750
Connection ~ 1700 1750
Wire Wire Line
	1700 1750 1700 2600
$Comp
L power:GND #PWR01
U 1 1 5C7BFE55
P 1700 2950
F 0 "#PWR01" H 1700 2700 50  0001 C CNN
F 1 "GND" H 1705 2777 50  0000 C CNN
F 2 "" H 1700 2950 50  0001 C CNN
F 3 "" H 1700 2950 50  0001 C CNN
	1    1700 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3200 2300 3200
Connection ~ 2300 3200
Wire Wire Line
	2300 3200 2350 3200
Connection ~ 2350 3200
Wire Wire Line
	2350 3200 2400 3200
Connection ~ 2400 3200
Wire Wire Line
	2400 3200 2450 3200
Connection ~ 2450 3200
Wire Wire Line
	2450 3200 2500 3200
Connection ~ 2500 3200
Wire Wire Line
	2500 3200 2550 3200
Connection ~ 2550 3200
Wire Wire Line
	2550 3200 2600 3200
Connection ~ 2600 3200
Wire Wire Line
	2600 3200 2650 3200
Connection ~ 2650 3200
Wire Wire Line
	2650 3200 2700 3200
Wire Wire Line
	2500 3200 2500 3400
Text Label 2500 3400 0    30   ~ 0
5V
Wire Wire Line
	1900 2550 1900 2500
Connection ~ 1900 2150
Wire Wire Line
	1900 2150 1900 2100
Connection ~ 1900 2200
Wire Wire Line
	1900 2200 1900 2150
Connection ~ 1900 2250
Wire Wire Line
	1900 2250 1900 2200
Connection ~ 1900 2300
Wire Wire Line
	1900 2300 1900 2250
Connection ~ 1900 2350
Wire Wire Line
	1900 2350 1900 2300
Connection ~ 1900 2400
Wire Wire Line
	1900 2400 1900 2350
Connection ~ 1900 2450
Wire Wire Line
	1900 2450 1900 2400
Connection ~ 1900 2500
Wire Wire Line
	1900 2500 1900 2450
Wire Wire Line
	1900 2250 1850 2250
Text Label 1850 2250 2    50   ~ 0
5V
Wire Wire Line
	1900 2800 1900 2850
Wire Wire Line
	1900 3200 2150 3200
Connection ~ 1900 2850
Wire Wire Line
	1900 2850 1900 2900
Connection ~ 1900 2900
Wire Wire Line
	1900 2900 1900 2950
Connection ~ 1900 2950
Wire Wire Line
	1900 2950 1900 3200
Connection ~ 2150 3200
Wire Wire Line
	2150 3200 2200 3200
Wire Wire Line
	1900 3200 1900 3400
Connection ~ 1900 3200
Wire Wire Line
	1900 1800 1900 1850
Connection ~ 1900 1850
Wire Wire Line
	1900 1850 1900 1900
Connection ~ 1900 1900
Wire Wire Line
	1900 1900 1900 1950
Connection ~ 1900 1950
Wire Wire Line
	1900 1950 1900 2000
Connection ~ 1900 2000
Wire Wire Line
	1900 2000 1900 2050
Wire Wire Line
	1900 1900 1850 1900
Text Label 1850 1900 2    50   ~ 0
3V3
Text Label 1900 3400 2    30   ~ 0
3V3
Text Label 2200 1450 1    30   ~ 0
5V
Text Label 3650 2050 0    30   ~ 0
3V3
Text Label 3650 1900 0    30   ~ 0
5V
Text Label 2850 3200 3    30   ~ 0
3V3
Text Label 2650 1450 1    30   ~ 0
5V
Text Label 3250 1450 1    30   ~ 0
5V
Text Label 3100 1450 1    30   ~ 0
3V3
Text Label 2800 1450 1    30   ~ 0
3V3
Text Label 2250 1450 1    30   ~ 0
OBC1
Text Label 2300 1450 1    30   ~ 0
OBC2
Text Label 2400 1450 1    30   ~ 0
OBC3
Text Label 2450 1450 1    30   ~ 0
OBC4
Text Label 2500 1450 1    30   ~ 0
OBC5
Text Label 3650 2150 0    30   ~ 0
OBC6
Text Label 3650 2200 0    30   ~ 0
OBC7
Text Label 3650 2250 0    30   ~ 0
OBC8
Text Label 3650 2350 0    30   ~ 0
POW1
Text Label 3650 2400 0    30   ~ 0
POW2
Text Label 3650 2450 0    30   ~ 0
POW3
Text Label 2600 1450 1    30   ~ 0
POW4
Text Label 2700 1450 1    30   ~ 0
POW5
Text Label 3650 2550 0    30   ~ 0
POW6
Text Label 3650 2600 0    30   ~ 0
POW7
Text Label 3650 2650 0    30   ~ 0
POW8
Text Label 2850 1450 1    30   ~ 0
POW9
Text Label 2900 1450 1    30   ~ 0
POW10
Text Label 3050 3200 3    30   ~ 0
POW11
Text Label 3000 3200 3    30   ~ 0
POW12
Text Label 2950 3200 3    30   ~ 0
POW13
Text Label 3650 1700 0    30   ~ 0
POW14
Text Label 3650 1750 0    30   ~ 0
POW15
Text Label 3650 1850 0    30   ~ 0
POW16
Text Label 3650 1950 0    30   ~ 0
POW17
Wire Wire Line
	3100 3400 3300 3400
Wire Wire Line
	2900 3400 3100 3400
Connection ~ 3100 3400
Wire Wire Line
	3100 3200 3100 3400
NoConn ~ 3150 3200
NoConn ~ 3200 3200
NoConn ~ 3250 3200
$Sheet
S 5800 1700 2000 1300
U 5C8866DE
F0 "mppt1" 50
F1 "mppt1.sch" 50
F2 "V0" O R 7800 1900 50 
F3 "VIN" I L 5800 1900 50 
F4 "V_5V" I L 5800 2100 50 
F5 "SCL" I L 5800 2300 50 
F6 "SDA" I L 5800 2400 50 
F7 "V_12V" I L 5800 2700 50 
F8 "GND" I L 5800 2900 50 
F9 "RST" I L 5800 2550 50 
$EndSheet
$Sheet
S 5800 3600 2000 1200
U 5C8872A1
F0 "mppt2" 50
F1 "mppt2.sch" 50
F2 "V0" O R 7800 3800 50 
F3 "VIN" I L 5800 3800 50 
F4 "V_5V" I L 5800 4000 50 
F5 "SCL" I L 5800 4200 50 
F6 "SDA" I L 5800 4300 50 
F7 "V_12V" I L 5800 4550 50 
F8 "GND" I L 5800 4700 50 
F9 "RST" I L 5800 4450 50 
$EndSheet
$Sheet
S 2400 3850 1750 1200
U 5C8959A2
F0 "boost_converter" 50
F1 "boost_converter.sch" 50
F2 "5V_Bat" I L 2400 4050 50 
F3 "12V_Out" I R 4150 4050 50 
F4 "GND" I L 2400 4350 50 
$EndSheet
Text Label 5850 5350 0    30   ~ 0
SOFT_SDA
Text Label 4650 5950 2    30   ~ 0
OBC2
Text Label 4650 5850 2    30   ~ 0
OBC3
$Comp
L main_board2-rescue:Conn_02x03_Odd_Even-conn J5
U 1 1 5C8D0D07
P 3300 6000
F 0 "J5" H 3350 6317 50  0000 C CNN
F 1 "Arduino ISP" H 3350 6226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 3300 6000 50  0001 C CNN
F 3 "~" H 3300 6000 50  0001 C CNN
	1    3300 6000
	1    0    0    -1  
$EndComp
Text Label 3100 5900 2    30   ~ 0
GND
Text Label 3100 6000 2    30   ~ 0
MOSI
Text Label 3100 6100 2    30   ~ 0
5V
Text Label 3600 5900 0    30   ~ 0
RESET
Text Label 3600 6100 0    30   ~ 0
MISO
Text Label 3600 6000 0    30   ~ 0
SCK
Text Label 4650 6050 2    30   ~ 0
MISO
Text Label 4650 6150 2    30   ~ 0
MOSI
Text Label 4650 6250 2    30   ~ 0
RESET
Text Label 4650 6350 2    30   ~ 0
SCK
Text Label 5850 5750 0    30   ~ 0
V_LOG
Text Label 5850 5650 0    30   ~ 0
CHG'
Text Label 5850 5550 0    30   ~ 0
ACP
Text Label 5850 5450 0    30   ~ 0
I_MON
Text Label 5800 4200 2    30   ~ 0
SOFT_SCL
Text Label 5800 2300 2    30   ~ 0
SOFT_SCL
Text Label 5800 4300 2    30   ~ 0
SOFT_SDA
Text Label 5800 2400 2    30   ~ 0
SOFT_SDA
Text Label 5800 2550 2    30   ~ 0
MPPT1_RST
Text Label 5800 4450 2    30   ~ 0
MPPT2_RST
Text Label 5850 5850 0    30   ~ 0
MPPT2_RST
Text Label 5850 5950 0    30   ~ 0
MPPT1_RST
Text Label 5850 6800 0    30   ~ 0
PGOOD3V3
Text Label 5850 6700 0    30   ~ 0
EN3V3
Text Label 5850 6600 0    30   ~ 0
PGOOD5V
Text Label 5850 6500 0    30   ~ 0
EN5V
Text Label 4650 5400 2    30   ~ 0
5V
Text Label 4650 5550 2    30   ~ 0
3V3
Text Label 4650 5700 2    30   ~ 0
GND
Text Label 5800 4000 2    30   ~ 0
5V
Text Label 5800 2100 2    30   ~ 0
5V
Wire Wire Line
	4150 4050 5100 4050
Wire Wire Line
	5100 4050 5100 4550
Wire Wire Line
	5100 4550 5800 4550
Wire Wire Line
	5800 2700 5100 2700
Wire Wire Line
	5100 2700 5100 4050
Connection ~ 5100 4050
$Comp
L main_board2-rescue:Screw_Terminal_01x02-conn J6
U 1 1 5C8D6D2A
P 5200 1900
F 0 "J6" H 5120 2117 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 5120 2026 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5200 1900 50  0001 C CNN
F 3 "~" H 5200 1900 50  0001 C CNN
	1    5200 1900
	-1   0    0    -1  
$EndComp
$Comp
L main_board2-rescue:Screw_Terminal_01x02-conn J7
U 1 1 5C8D6E07
P 5200 3800
F 0 "J7" H 5120 4017 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 5120 3926 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5200 3800 50  0001 C CNN
F 3 "~" H 5200 3800 50  0001 C CNN
	1    5200 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 1900 5400 1900
Wire Wire Line
	5400 2000 5400 2900
Wire Wire Line
	5400 2900 5800 2900
Wire Wire Line
	5800 3800 5400 3800
Wire Wire Line
	5400 3900 5400 4700
Wire Wire Line
	5400 4700 5800 4700
$Comp
L power:GND #PWR0133
U 1 1 5C8DFD32
P 5400 2950
F 0 "#PWR0133" H 5400 2700 50  0001 C CNN
F 1 "GND" H 5405 2777 50  0000 C CNN
F 2 "" H 5400 2950 50  0001 C CNN
F 3 "" H 5400 2950 50  0001 C CNN
	1    5400 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0134
U 1 1 5C8DFD54
P 5400 4750
F 0 "#PWR0134" H 5400 4500 50  0001 C CNN
F 1 "GND" H 5405 4577 50  0000 C CNN
F 2 "" H 5400 4750 50  0001 C CNN
F 3 "" H 5400 4750 50  0001 C CNN
	1    5400 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4750 5400 4700
Connection ~ 5400 4700
Wire Wire Line
	5400 2950 5400 2900
Connection ~ 5400 2900
Wire Wire Line
	900  5400 1150 5400
Wire Wire Line
	900  5450 1150 5450
Wire Wire Line
	1150 5500 900  5500
Wire Wire Line
	900  5550 1150 5550
Wire Wire Line
	1150 5600 900  5600
Wire Wire Line
	900  5650 1150 5650
Wire Wire Line
	900  5700 1150 5700
Wire Wire Line
	900  5750 1150 5750
Wire Wire Line
	1150 5800 900  5800
Wire Wire Line
	900  5850 1150 5850
Wire Wire Line
	1150 5900 900  5900
Wire Wire Line
	900  5950 1150 5950
Wire Wire Line
	900  6000 1150 6000
Wire Wire Line
	900  6050 1150 6050
Wire Wire Line
	1150 6100 900  6100
Wire Wire Line
	900  6150 1150 6150
Wire Wire Line
	1150 6200 900  6200
Text Label 900  5400 2    30   ~ 0
POW1
Text Label 900  5450 2    30   ~ 0
POW2
Text Label 900  5500 2    30   ~ 0
POW3
Text Label 900  5550 2    30   ~ 0
POW4
Text Label 900  5600 2    30   ~ 0
POW5
Text Label 900  5650 2    30   ~ 0
POW6
Text Label 900  5700 2    30   ~ 0
POW7
Text Label 900  5750 2    30   ~ 0
POW8
Text Label 900  5800 2    30   ~ 0
POW9
Text Label 900  5850 2    30   ~ 0
POW10
Text Label 900  5900 2    30   ~ 0
POW11
Text Label 900  5950 2    30   ~ 0
POW12
Text Label 900  6000 2    30   ~ 0
POW13
Text Label 900  6050 2    30   ~ 0
POW14
Text Label 900  6100 2    30   ~ 0
POW15
Text Label 900  6150 2    30   ~ 0
POW16
Text Label 900  6200 2    30   ~ 0
POW17
Wire Wire Line
	900  6350 1150 6350
Wire Wire Line
	900  6400 1150 6400
Wire Wire Line
	1150 6450 900  6450
Wire Wire Line
	900  6500 1150 6500
Wire Wire Line
	1150 6550 900  6550
Wire Wire Line
	900  6600 1150 6600
Wire Wire Line
	900  6650 1150 6650
Wire Wire Line
	900  6700 1150 6700
Text Label 900  6350 2    30   ~ 0
OBC1
Text Label 900  6400 2    30   ~ 0
OBC2
Text Label 900  6450 2    30   ~ 0
OBC3
Text Label 900  6500 2    30   ~ 0
OBC4
Text Label 900  6550 2    30   ~ 0
OBC5
Text Label 900  6600 2    30   ~ 0
OBC6
Text Label 900  6650 2    30   ~ 0
OBC7
Text Label 900  6700 2    30   ~ 0
OBC8
Text Label 1150 6400 0    30   ~ 0
SCL
Text Label 1150 6450 0    30   ~ 0
SDA
Wire Wire Line
	1150 5900 1150 5950
Connection ~ 1150 5950
Wire Wire Line
	1150 5950 1150 6000
Wire Wire Line
	1150 5950 1350 5950
Text Label 1150 5800 0    30   ~ 0
SOFT_SDA
Text Label 1150 5850 0    30   ~ 0
SOFT_SCL
Text Label 1150 6050 0    30   ~ 0
V_LOG
Text Label 1150 6100 0    30   ~ 0
CHG'
Text Label 1150 6150 0    30   ~ 0
ACP
Text Label 1150 6200 0    30   ~ 0
I_MON
Text Label 1350 5950 0    30   ~ 0
VO
Text Label 7800 3800 0    30   ~ 0
VO
Text Label 7800 1900 0    30   ~ 0
VO
Text Label 1150 5750 0    30   ~ 0
PGOOD3V3
Text Label 1150 5700 0    30   ~ 0
EN3V3
Text Label 1150 5650 0    30   ~ 0
PGOOD5V
Text Label 1150 5600 0    30   ~ 0
EN5V
Text Label 2400 4050 2    30   ~ 0
5V
$Comp
L power:GND #PWR0135
U 1 1 5C91F17E
P 2250 4450
F 0 "#PWR0135" H 2250 4200 50  0001 C CNN
F 1 "GND" H 2255 4277 50  0000 C CNN
F 2 "" H 2250 4450 50  0001 C CNN
F 3 "" H 2250 4450 50  0001 C CNN
	1    2250 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 4350 2250 4350
Wire Wire Line
	2250 4350 2250 4450
NoConn ~ 1150 6700
NoConn ~ 1150 6650
NoConn ~ 1150 6600
NoConn ~ 1150 6550
NoConn ~ 1150 6500
NoConn ~ 1150 6350
NoConn ~ 1150 5400
Text Label 5850 6150 0    30   ~ 0
RX
Text Label 5850 6250 0    30   ~ 0
TX
Text Label 1150 5450 0    30   ~ 0
D2
Text Label 1150 5500 0    30   ~ 0
RX
Text Label 1150 5550 0    30   ~ 0
TX
$Sheet
S 4650 5200 1200 2100
U 5C8C41FE
F0 "control_board_combination" 50
F1 "control_board_combination.sch" 50
F2 "5V_VCC" I L 4650 5400 50 
F3 "3P3V_VCC" I L 4650 5550 50 
F4 "GND" I L 4650 5700 50 
F5 "SDA" O L 4650 5850 50 
F6 "SCL" O L 4650 5950 50 
F7 "MISO" O L 4650 6050 50 
F8 "MOSI" O L 4650 6150 50 
F9 "RESET" O L 4650 6250 50 
F10 "SCK" O L 4650 6350 50 
F11 "TX" O R 5850 6250 50 
F12 "RX" O R 5850 6150 50 
F13 "D3" O R 5850 5950 50 
F14 "D2" O R 5850 6050 50 
F15 "D5" O R 5850 5750 50 
F16 "D4" O R 5850 5850 50 
F17 "D7" O R 5850 5550 50 
F18 "D6" O R 5850 5650 50 
F19 "D9" O R 5850 5350 50 
F20 "D8" O R 5850 5450 50 
F21 "A0" O R 5850 6800 50 
F22 "A2" O R 5850 6600 50 
F23 "A1" O R 5850 6700 50 
F24 "A3" O R 5850 6500 50 
$EndSheet
Text Label 5850 6050 0    30   ~ 0
SOFT_SCL
$EndSCHEMATC

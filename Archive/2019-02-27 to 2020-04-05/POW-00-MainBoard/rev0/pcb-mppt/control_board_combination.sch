EESchema Schematic File Version 4
LIBS:main_board2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2800 1300 1400 3250
U 5C85C76C
F0 "microcontroller" 50
F1 "microcontroller.sch" 50
F2 "MOSI" O L 2800 1750 50 
F3 "MISO" O L 2800 1900 50 
F4 "SCK" O L 2800 2100 50 
F5 "RESET" O L 2800 2350 50 
F6 "GND" I L 2800 2800 50 
F7 "5V_VCC" I L 2800 3000 50 
F8 "SCL" O L 2800 1450 50 
F9 "SDA" O L 2800 1600 50 
F10 "RX" O L 2800 3150 50 
F11 "TX" O L 2800 3250 50 
F12 "D2" I L 2800 3400 50 
F13 "D3" I L 2800 3500 50 
F14 "D4" I L 2800 3600 50 
F15 "D5" I L 2800 3700 50 
F16 "D6" I L 2800 3800 50 
F17 "D7" I L 2800 3900 50 
F18 "D8" I L 2800 4000 50 
F19 "D9" I L 2800 4100 50 
F20 "D10" I L 2800 4200 50 
F21 "A0" I L 2800 4300 50 
F22 "A1" I L 2800 4400 50 
F23 "A2" I L 2800 4500 50 
F24 "A3" I R 4200 4500 50 
$EndSheet
$Sheet
S 5500 2050 1200 900 
U 5C85CA47
F0 "sd_card" 50
F1 "sd_card.sch" 50
F2 "GND" I L 5500 2150 50 
F3 "5V_VCC" I L 5500 2250 50 
F4 "CS" I L 5500 2350 50 
F5 "MOSI" I L 5500 2450 50 
F6 "SCK" I L 5500 2550 50 
F7 "MISO" I L 5500 2650 50 
$EndSheet
Wire Wire Line
	2800 1450 2350 1450
Wire Wire Line
	2800 1600 2350 1600
Wire Wire Line
	2800 1750 2350 1750
Wire Wire Line
	2800 1900 2350 1900
Wire Wire Line
	2800 2100 2350 2100
Wire Wire Line
	2800 2350 2350 2350
Wire Wire Line
	2800 2800 2350 2800
Wire Wire Line
	2800 3000 2350 3000
Wire Wire Line
	5500 2150 5150 2150
Wire Wire Line
	5500 2250 5150 2250
Wire Wire Line
	5500 2350 5150 2350
Wire Wire Line
	5500 2450 5150 2450
Wire Wire Line
	5500 2550 5150 2550
Wire Wire Line
	5500 2650 5150 2650
Text Label 5150 2150 0    50   ~ 0
GND
Text Label 5150 2250 0    50   ~ 0
5V_VCC
Text Label 5150 2350 0    50   ~ 0
D10
Text Label 5150 2450 0    50   ~ 0
MOSI
Text Label 5150 2550 0    50   ~ 0
SCK
Text Label 5150 2650 0    50   ~ 0
MISO
Text Label 2350 1450 0    50   ~ 0
SCL
Text Label 2350 1600 0    50   ~ 0
SDA
Text Label 2350 1750 0    50   ~ 0
MOSI
Text Label 2350 1900 0    50   ~ 0
MISO
Text Label 2350 2100 0    50   ~ 0
SCK
Text Label 2350 2350 0    50   ~ 0
RESET
Text Label 2350 2800 0    50   ~ 0
GND
Text Label 2350 3000 0    50   ~ 0
5V_VCC
Text HLabel 1400 4950 0    50   Input ~ 0
5V_VCC
Text HLabel 1400 5050 0    50   Input ~ 0
3P3V_VCC
Text HLabel 1400 5150 0    50   Input ~ 0
GND
Text HLabel 1400 5450 0    50   Output ~ 0
SDA
Text Label 1400 4950 0    50   ~ 0
5V_VCC
Text Label 1400 5050 0    50   ~ 0
3P3V_VCC
Text Label 1400 5150 0    50   ~ 0
GND
Text Label 1400 5350 0    50   ~ 0
SCL
Text Label 1400 5450 0    50   ~ 0
SDA
Text HLabel 1400 5350 0    50   Output ~ 0
SCL
Wire Wire Line
	2800 3150 2350 3150
Wire Wire Line
	2800 3250 2350 3250
Wire Wire Line
	2800 3400 2350 3400
Wire Wire Line
	2800 3500 2350 3500
Wire Wire Line
	2800 3600 2350 3600
Wire Wire Line
	2800 3700 2350 3700
Wire Wire Line
	2800 3800 2350 3800
Wire Wire Line
	2800 3900 2350 3900
Wire Wire Line
	2800 4000 2350 4000
Wire Wire Line
	2800 4100 2350 4100
Wire Wire Line
	2800 4200 2350 4200
Wire Wire Line
	2800 4300 2350 4300
Wire Wire Line
	2800 4400 2350 4400
Wire Wire Line
	2800 4500 2350 4500
Wire Wire Line
	4200 4500 4500 4500
Text Label 2350 3150 0    50   ~ 0
RX
Text Label 2350 3250 0    50   ~ 0
TX
Text Label 2350 3400 0    50   ~ 0
D2
Text Label 2350 3500 0    50   ~ 0
D3
Text Label 2350 3600 0    50   ~ 0
D4
Text Label 2350 3700 0    50   ~ 0
D5
Text Label 2350 3800 0    50   ~ 0
D6
Text Label 2350 3900 0    50   ~ 0
D7
Text Label 2350 4000 0    50   ~ 0
D8
Text Label 2350 4100 0    50   ~ 0
D9
Text Label 2350 4200 0    50   ~ 0
D10
Text Label 2350 4300 0    50   ~ 0
A0
Text Label 2350 4400 0    50   ~ 0
A1
Text Label 2350 4500 0    50   ~ 0
A2
Text Label 4500 4500 0    50   ~ 0
A3
Text HLabel 1400 5650 0    50   Output ~ 0
MISO
Text Label 1400 5550 0    50   ~ 0
MOSI
Text Label 1400 5650 0    50   ~ 0
MISO
Text HLabel 1400 5550 0    50   Output ~ 0
MOSI
Text HLabel 1400 5850 0    50   Output ~ 0
RESET
Text Label 1400 5750 0    50   ~ 0
SCK
Text Label 1400 5850 0    50   ~ 0
RESET
Text HLabel 1400 5750 0    50   Output ~ 0
SCK
Text HLabel 1400 6050 0    50   Output ~ 0
TX
Text Label 1400 5950 0    50   ~ 0
RX
Text Label 1400 6050 0    50   ~ 0
TX
Text HLabel 1400 5950 0    50   Output ~ 0
RX
Text HLabel 1400 6250 0    50   Output ~ 0
D3
Text Label 1400 6150 0    50   ~ 0
D2
Text Label 1400 6250 0    50   ~ 0
D3
Text HLabel 1400 6150 0    50   Output ~ 0
D2
Text HLabel 1400 6450 0    50   Output ~ 0
D5
Text Label 1400 6350 0    50   ~ 0
D4
Text Label 1400 6450 0    50   ~ 0
D5
Text HLabel 1400 6350 0    50   Output ~ 0
D4
Text HLabel 1400 6650 0    50   Output ~ 0
D7
Text Label 1400 6550 0    50   ~ 0
D6
Text Label 1400 6650 0    50   ~ 0
D7
Text HLabel 1400 6550 0    50   Output ~ 0
D6
Text HLabel 1400 6850 0    50   Output ~ 0
D9
Text Label 1400 6750 0    50   ~ 0
D8
Text Label 1400 6850 0    50   ~ 0
D9
Text HLabel 1400 6750 0    50   Output ~ 0
D8
Text HLabel 1400 7050 0    50   Output ~ 0
A0
Text Label 1400 6950 0    50   ~ 0
D10
Text Label 1400 7050 0    50   ~ 0
A0
Text HLabel 1400 6950 0    50   Output ~ 0
D10
Text HLabel 1400 7250 0    50   Output ~ 0
A2
Text Label 1400 7150 0    50   ~ 0
A1
Text Label 1400 7250 0    50   ~ 0
A2
Text HLabel 1400 7150 0    50   Output ~ 0
A1
Text HLabel 1400 7350 0    50   Output ~ 0
A3
Text Label 1400 7350 0    50   ~ 0
A3
Wire Wire Line
	2650 5600 3150 5600
$Comp
L Device:LED D_ARDUINO_TEST_1
U 1 1 5C8E3860
P 3300 5600
F 0 "D_ARDUINO_TEST_1" H 3291 5816 50  0000 C CNN
F 1 "LED" H 3291 5725 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3300 5600 50  0001 C CNN
F 3 "~" H 3300 5600 50  0001 C CNN
	1    3300 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 5600 3750 5600
$Comp
L Device:R R53
U 1 1 5C8EC61F
P 3900 5600
F 0 "R53" V 3693 5600 50  0000 C CNN
F 1 "220" V 3784 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3830 5600 50  0001 C CNN
F 3 "~" H 3900 5600 50  0001 C CNN
	1    3900 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 5600 4250 5600
Text Label 4250 5600 0    50   ~ 0
5V_VCC
Text Label 2650 5600 0    50   ~ 0
D2
Wire Wire Line
	2650 6000 3150 6000
$Comp
L Device:LED D_ARDUINO_TEST_2
U 1 1 5C8F05DE
P 3300 6000
F 0 "D_ARDUINO_TEST_2" H 3291 6216 50  0000 C CNN
F 1 "LED" H 3291 6125 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3300 6000 50  0001 C CNN
F 3 "~" H 3300 6000 50  0001 C CNN
	1    3300 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6000 3750 6000
$Comp
L Device:R R54
U 1 1 5C8F05E6
P 3900 6000
F 0 "R54" V 3693 6000 50  0000 C CNN
F 1 "220" V 3784 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3830 6000 50  0001 C CNN
F 3 "~" H 3900 6000 50  0001 C CNN
	1    3900 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 6000 4250 6000
Text Label 4250 6000 0    50   ~ 0
5V_VCC
Text Label 2650 6000 0    50   ~ 0
A0
Wire Wire Line
	1400 4950 1950 4950
Wire Wire Line
	1400 5050 1950 5050
Wire Wire Line
	1400 5150 1950 5150
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5C9077DA
P 1950 4950
F 0 "#FLG0102" H 1950 5025 50  0001 C CNN
F 1 "PWR_FLAG" H 1950 5124 50  0000 C CNN
F 2 "" H 1950 4950 50  0001 C CNN
F 3 "~" H 1950 4950 50  0001 C CNN
	1    1950 4950
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5C90781D
P 1950 5050
F 0 "#FLG0103" H 1950 5125 50  0001 C CNN
F 1 "PWR_FLAG" H 1950 5224 50  0000 C CNN
F 2 "" H 1950 5050 50  0001 C CNN
F 3 "~" H 1950 5050 50  0001 C CNN
	1    1950 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0136
U 1 1 5C9078AD
P 1950 5150
F 0 "#PWR0136" H 1950 4950 50  0001 C CNN
F 1 "GNDPWR" H 1954 4996 50  0000 C CNN
F 2 "" H 1950 5100 50  0001 C CNN
F 3 "" H 1950 5100 50  0001 C CNN
	1    1950 5150
	1    0    0    -1  
$EndComp
$EndSCHEMATC

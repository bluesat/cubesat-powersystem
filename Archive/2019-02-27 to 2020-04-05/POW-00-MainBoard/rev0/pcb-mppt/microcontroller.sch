EESchema Schematic File Version 4
LIBS:main_board2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328-AU U3
U 1 1 5C7D5737
P 4900 3700
F 0 "U3" H 4900 2114 50  0000 C CNN
F 1 "ATmega328-AU" H 4900 2023 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 4900 3700 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 4900 3700 50  0001 C CNN
	1    4900 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5C7E06F1
P 6200 3100
F 0 "Y1" H 6200 3368 50  0000 C CNN
F 1 "16MHz" H 6200 3277 50  0000 C CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 6200 3100 50  0001 C CNN
F 3 "~" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C_CRYSTAL_1
U 1 1 5C7E0CB4
P 6050 3250
F 0 "C_CRYSTAL_1" H 6165 3296 50  0000 L CNN
F 1 "18pF" H 6165 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6088 3100 50  0001 C CNN
F 3 "~" H 6050 3250 50  0001 C CNN
	1    6050 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C_CRYSTAL_2
U 1 1 5C7E23A8
P 6700 3250
F 0 "C_CRYSTAL_2" H 6815 3296 50  0000 L CNN
F 1 "18pF" H 6815 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6738 3100 50  0001 C CNN
F 3 "~" H 6700 3250 50  0001 C CNN
	1    6700 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3100 6700 3100
Wire Wire Line
	6050 3100 5500 3100
Connection ~ 6050 3100
Wire Wire Line
	5500 3200 5900 3200
Wire Wire Line
	5900 3200 5900 3650
Wire Wire Line
	5900 3650 7550 3650
Wire Wire Line
	7550 3650 7550 3100
Wire Wire Line
	7550 3100 6700 3100
Connection ~ 6700 3100
$Comp
L Device:R R_RESET1
U 1 1 5C7EC2F3
P 6800 4000
F 0 "R_RESET1" V 6593 4000 50  0000 C CNN
F 1 "10k" V 6684 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6730 4000 50  0001 C CNN
F 3 "~" H 6800 4000 50  0001 C CNN
	1    6800 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6600 4000 6600 4100
Connection ~ 6600 4000
Wire Wire Line
	6600 4000 6650 4000
Text HLabel 3350 3950 0    50   Output ~ 0
SCL
Text HLabel 3350 4050 0    50   Output ~ 0
SDA
Text HLabel 3400 3450 0    50   Output ~ 0
MOSI
Text HLabel 3400 3550 0    50   Output ~ 0
MISO
Text HLabel 3400 3650 0    50   Output ~ 0
SCK
Text HLabel 3400 3750 0    50   Output ~ 0
RESET
Text Label 6950 4000 0    50   ~ 0
5V_VCC
Text Label 6700 3400 0    50   ~ 0
GND
Text Label 6050 3400 0    50   ~ 0
GND
Text HLabel 3400 3150 0    50   Input ~ 0
5V_VCC
Text HLabel 3400 3050 0    50   Input ~ 0
GND
Text Label 3400 3050 0    50   ~ 0
GND
Text Label 3400 3150 0    50   ~ 0
5V_VCC
Text Label 3400 3450 0    50   ~ 0
MOSI
Text Label 3400 3550 0    50   ~ 0
MISO
Text Label 3400 3650 0    50   ~ 0
SCK
Text Label 3400 3750 0    50   ~ 0
RESET
Wire Wire Line
	3350 3950 3550 3950
Wire Wire Line
	3350 4050 3550 4050
Text Label 3550 3950 0    50   ~ 0
SCL
Text Label 3550 4050 0    50   ~ 0
SDA
Wire Wire Line
	4900 2200 4900 1800
Wire Wire Line
	4900 1800 4950 1800
Wire Wire Line
	4950 1800 4950 1650
Connection ~ 4950 1800
Wire Wire Line
	4950 1800 5000 1800
$Comp
L Device:C C60
U 1 1 5C8A88D2
P 5100 1650
F 0 "C60" H 5215 1696 50  0000 L CNN
F 1 "100pF" H 5215 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5138 1500 50  0001 C CNN
F 3 "~" H 5100 1650 50  0001 C CNN
	1    5100 1650
	0    -1   -1   0   
$EndComp
Text Label 5250 1650 0    50   ~ 0
GND
Text Label 4950 1800 0    50   ~ 0
5V_VCC
Wire Wire Line
	5500 4000 6600 4000
Wire Wire Line
	6600 4100 7450 4100
Text Label 7450 4100 0    50   ~ 0
RESET
NoConn ~ 4300 2500
NoConn ~ 4300 2700
NoConn ~ 4300 2800
Wire Wire Line
	5000 2200 5000 1800
Wire Wire Line
	5500 2500 5700 2500
Wire Wire Line
	5500 2600 5700 2600
Wire Wire Line
	5500 2700 5700 2700
Wire Wire Line
	5500 2800 5700 2800
Wire Wire Line
	5500 2900 5700 2900
Wire Wire Line
	5500 3000 5700 3000
Wire Wire Line
	5500 3400 5700 3400
Wire Wire Line
	5500 3600 5700 3600
Wire Wire Line
	5500 3700 5700 3700
Wire Wire Line
	5500 3800 5700 3800
Wire Wire Line
	5500 3900 5700 3900
Wire Wire Line
	5500 4200 5700 4200
Wire Wire Line
	5500 4300 5700 4300
Wire Wire Line
	5500 4400 5700 4400
Wire Wire Line
	5500 4500 5700 4500
Wire Wire Line
	5500 4600 5700 4600
Wire Wire Line
	5500 4700 5700 4700
Wire Wire Line
	5500 4800 5700 4800
Wire Wire Line
	5500 4900 5700 4900
Text Label 5700 2500 0    50   ~ 0
D8
Text Label 5700 2600 0    50   ~ 0
D9
Text Label 5700 2700 0    50   ~ 0
D10
Text Label 5700 2800 0    50   ~ 0
MOSI
Text Label 5700 2900 0    50   ~ 0
MISO
Text Label 5700 3000 0    50   ~ 0
SCK
Text Label 5700 3400 0    50   ~ 0
A0
Text Label 5700 3600 0    50   ~ 0
A2
Text Label 5700 3700 0    50   ~ 0
A3
Text Label 5700 3800 0    50   ~ 0
SDA
Text Label 5700 3900 0    50   ~ 0
SCL
Text Label 5700 4200 0    50   ~ 0
RX
Text Label 5700 4300 0    50   ~ 0
TX
Text Label 5700 4400 0    50   ~ 0
D2
Text Label 5700 4500 0    50   ~ 0
D3
Text Label 5700 4600 0    50   ~ 0
D4
Text Label 5700 4700 0    50   ~ 0
D5
Text Label 5700 4800 0    50   ~ 0
D6
Text Label 5700 4900 0    50   ~ 0
D7
Text HLabel 3350 4200 0    50   Input ~ 0
RX
Text HLabel 3350 4300 0    50   Input ~ 0
TX
Text HLabel 3350 4400 0    50   Input ~ 0
D2
Text HLabel 3350 4500 0    50   Input ~ 0
D3
Text HLabel 3350 4600 0    50   Input ~ 0
D4
Text HLabel 3350 4700 0    50   Input ~ 0
D5
Text HLabel 3350 4800 0    50   Input ~ 0
D6
Text HLabel 3350 4900 0    50   Input ~ 0
D7
Text HLabel 3350 5000 0    50   Input ~ 0
D8
Text HLabel 3350 5100 0    50   Input ~ 0
D9
Text HLabel 3350 5200 0    50   Input ~ 0
D10
Text HLabel 3350 5300 0    50   Input ~ 0
A0
Text HLabel 3350 5400 0    50   Input ~ 0
A1
Text HLabel 3350 5500 0    50   Input ~ 0
A2
Text HLabel 3350 5600 0    50   Input ~ 0
A3
Wire Wire Line
	3350 4200 3550 4200
Wire Wire Line
	3350 4300 3550 4300
Wire Wire Line
	3350 4400 3550 4400
Wire Wire Line
	3350 4500 3550 4500
Wire Wire Line
	3350 4600 3550 4600
Wire Wire Line
	3350 4700 3550 4700
Wire Wire Line
	3350 4800 3550 4800
Wire Wire Line
	3350 4900 3550 4900
Wire Wire Line
	3350 5000 3550 5000
Wire Wire Line
	3350 5100 3550 5100
Wire Wire Line
	3350 5200 3550 5200
Wire Wire Line
	3350 5300 3550 5300
Wire Wire Line
	3350 5400 3550 5400
Wire Wire Line
	3350 5500 3550 5500
Wire Wire Line
	3350 5600 3550 5600
Text Label 3550 4200 0    50   ~ 0
RX
Text Label 3550 4300 0    50   ~ 0
TX
Text Label 3550 4400 0    50   ~ 0
D2
Text Label 3550 4500 0    50   ~ 0
D3
Text Label 3550 4600 0    50   ~ 0
D4
Text Label 3550 4700 0    50   ~ 0
D5
Text Label 3550 4800 0    50   ~ 0
D6
Text Label 3550 4900 0    50   ~ 0
D7
Text Label 3550 5000 0    50   ~ 0
D8
Text Label 3550 5100 0    50   ~ 0
D9
Text Label 3550 5200 0    50   ~ 0
D10
Text Label 3550 5300 0    50   ~ 0
A0
Text Label 3550 5400 0    50   ~ 0
A1
Text Label 3550 5500 0    50   ~ 0
A2
Text Label 3550 5600 0    50   ~ 0
A3
Wire Wire Line
	5500 3500 5700 3500
Text Label 5700 3500 0    50   ~ 0
A1
Text Label 4900 5200 0    50   ~ 0
GND
$EndSCHEMATC

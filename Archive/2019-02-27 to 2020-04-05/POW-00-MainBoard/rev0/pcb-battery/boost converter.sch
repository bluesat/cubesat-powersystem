EESchema Schematic File Version 4
LIBS:boost converter-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L L1
U 1 1 5C0B9DB8
P 6400 2850
F 0 "L1" V 6590 2850 50  0000 C CNN
F 1 "33u" V 6499 2850 50  0000 C CNN
F 2 "CustomFootprints:L1" H 6400 2850 50  0001 C CNN
F 3 "~" H 6400 2850 50  0001 C CNN
	1    6400 2850
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Switching:LT1301 U1
U 1 1 5C0B9FF0
P 5950 3450
F 0 "U1" H 6150 4000 50  0000 C CNN
F 1 "LT1301" H 6150 3900 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5950 3450 50  0001 C CNN
F 3 "http://www.linear.com/docs/3451" H 5950 3450 50  0001 C CNN
	1    5950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2850 6650 2850
Wire Wire Line
	6650 2850 6650 3350
Wire Wire Line
	6650 3350 6350 3350
Wire Wire Line
	6250 2850 5950 2850
Wire Wire Line
	5950 2850 5950 2950
$Comp
L Device:D_Schottky D1
U 1 1 5C0BA0ED
P 6950 2850
F 0 "D1" H 6950 2634 50  0000 C CNN
F 1 "1N4004" H 6950 2725 50  0000 C CNN
F 2 "Diode_THT:D_DO-201AD_P12.70mm_Horizontal" H 6950 2850 50  0001 C CNN
F 3 "~" H 6950 2850 50  0001 C CNN
	1    6950 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6650 2850 6800 2850
Connection ~ 6650 2850
Wire Wire Line
	7100 2850 7100 3550
Wire Wire Line
	7100 3550 6750 3550
Wire Wire Line
	5550 3250 5400 3250
Wire Wire Line
	5400 3250 5400 2850
Wire Wire Line
	5400 2850 5950 2850
Connection ~ 5950 2850
$Comp
L Device:C C4
U 1 1 5C0BA388
P 5000 3400
F 0 "C4" H 5115 3446 50  0000 L CNN
F 1 "10u" H 5100 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5038 3250 50  0001 C CNN
F 3 "~" H 5000 3400 50  0001 C CNN
	1    5000 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5C0BA3B6
P 7100 3800
F 0 "C7" H 7215 3846 50  0000 L CNN
F 1 "10u" H 7150 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7138 3650 50  0001 C CNN
F 3 "~" H 7100 3800 50  0001 C CNN
	1    7100 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5C0BA410
P 6750 3800
F 0 "C6" H 6865 3846 50  0000 L CNN
F 1 "10u" H 6800 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6788 3650 50  0001 C CNN
F 3 "~" H 6750 3800 50  0001 C CNN
	1    6750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3650 7100 3550
Connection ~ 7100 3550
Wire Wire Line
	6750 3650 6750 3550
Connection ~ 6750 3550
Wire Wire Line
	6750 3550 6450 3550
Wire Wire Line
	5400 2850 5000 2850
Wire Wire Line
	5000 2850 5000 3250
Connection ~ 5400 2850
$Comp
L Device:C C3
U 1 1 5C0BA6FC
P 4700 3400
F 0 "C3" H 4815 3446 50  0000 L CNN
F 1 "10u" H 4750 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4738 3250 50  0001 C CNN
F 3 "~" H 4700 3400 50  0001 C CNN
	1    4700 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5C0BA728
P 4400 3400
F 0 "C2" H 4515 3446 50  0000 L CNN
F 1 "10u" H 4450 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4438 3250 50  0001 C CNN
F 3 "~" H 4400 3400 50  0001 C CNN
	1    4400 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5C0BA754
P 4100 3400
F 0 "C1" H 4215 3446 50  0000 L CNN
F 1 "10u" H 4150 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4138 3250 50  0001 C CNN
F 3 "~" H 4100 3400 50  0001 C CNN
	1    4100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2850 4700 2850
Connection ~ 5000 2850
Wire Wire Line
	4400 3250 4400 2850
Wire Wire Line
	4700 3250 4700 2850
Connection ~ 4700 2850
Wire Wire Line
	4700 2850 4400 2850
Wire Wire Line
	4100 3550 4100 3800
Wire Wire Line
	4100 3800 4400 3800
Wire Wire Line
	5000 3800 5000 3550
Wire Wire Line
	4700 3550 4700 3800
Connection ~ 4700 3800
Wire Wire Line
	4700 3800 5000 3800
Wire Wire Line
	4400 3550 4400 3800
Connection ~ 4400 3800
Wire Wire Line
	4400 3800 4700 3800
NoConn ~ 5550 3650
Text Notes 4450 2800 0    50   ~ 0
Bypass caps within 5mm of the Vin pin
$Comp
L Device:C C5
U 1 1 5C0BC030
P 6450 3800
F 0 "C5" H 6565 3846 50  0000 L CNN
F 1 "10u" H 6500 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 3650 50  0001 C CNN
F 3 "~" H 6450 3800 50  0001 C CNN
	1    6450 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3650 6450 3550
Connection ~ 6450 3550
Wire Wire Line
	6450 3550 6350 3550
Wire Wire Line
	7100 3950 7100 4050
Wire Wire Line
	7100 4050 6750 4050
Wire Wire Line
	6450 4050 6450 3950
Wire Wire Line
	6750 3950 6750 4050
Connection ~ 6750 4050
Wire Wire Line
	6750 4050 6450 4050
Text Label 7100 2850 0    50   ~ 0
VO
$Comp
L power:+5V #PWR01
U 1 1 5C0BD28C
P 3050 2500
F 0 "#PWR01" H 3050 2350 50  0001 C CNN
F 1 "+5V" H 3065 2673 50  0000 C CNN
F 2 "" H 3050 2500 50  0001 C CNN
F 3 "" H 3050 2500 50  0001 C CNN
	1    3050 2500
	1    0    0    -1  
$EndComp
Text Label 8150 2600 0    50   ~ 0
VO
Text Label 2800 2500 0    50   ~ 0
V+
Wire Wire Line
	4400 2850 4100 2850
Wire Wire Line
	4100 2850 4100 3250
Connection ~ 4400 2850
Wire Wire Line
	3250 2500 3050 2500
Connection ~ 3050 2500
Wire Wire Line
	3050 2500 2800 2500
$Comp
L power:GND #PWR0101
U 1 1 5C0BF2B7
P 5350 3450
F 0 "#PWR0101" H 5350 3200 50  0001 C CNN
F 1 "GND" H 5355 3277 50  0000 C CNN
F 2 "" H 5350 3450 50  0001 C CNN
F 3 "" H 5350 3450 50  0001 C CNN
	1    5350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3450 5350 3450
$Comp
L power:GND #PWR0102
U 1 1 5C0BFA3F
P 5950 4050
F 0 "#PWR0102" H 5950 3800 50  0001 C CNN
F 1 "GND" H 5955 3877 50  0000 C CNN
F 2 "" H 5950 4050 50  0001 C CNN
F 3 "" H 5950 4050 50  0001 C CNN
	1    5950 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3950 6050 4000
Wire Wire Line
	6050 4000 5950 4000
Wire Wire Line
	5950 4000 5950 4050
Wire Wire Line
	5950 3950 5950 4000
Connection ~ 5950 4000
$Comp
L power:GND #PWR0103
U 1 1 5C0C0B7F
P 4700 3850
F 0 "#PWR0103" H 4700 3600 50  0001 C CNN
F 1 "GND" H 4705 3677 50  0000 C CNN
F 2 "" H 4700 3850 50  0001 C CNN
F 3 "" H 4700 3850 50  0001 C CNN
	1    4700 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3800 4700 3850
$Comp
L power:GND #PWR0104
U 1 1 5C0C14C3
P 6750 4100
F 0 "#PWR0104" H 6750 3850 50  0001 C CNN
F 1 "GND" H 6755 3927 50  0000 C CNN
F 2 "" H 6750 4100 50  0001 C CNN
F 3 "" H 6750 4100 50  0001 C CNN
	1    6750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4050 6750 4100
$Comp
L power:GND #PWR0105
U 1 1 5C0C1E5B
P 3150 2600
F 0 "#PWR0105" H 3150 2350 50  0001 C CNN
F 1 "GND" H 3155 2427 50  0000 C CNN
F 2 "" H 3150 2600 50  0001 C CNN
F 3 "" H 3150 2600 50  0001 C CNN
	1    3150 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5C0C2889
P 8200 2950
F 0 "#PWR0106" H 8200 2700 50  0001 C CNN
F 1 "GND" H 8205 2777 50  0000 C CNN
F 2 "" H 8200 2950 50  0001 C CNN
F 3 "" H 8200 2950 50  0001 C CNN
	1    8200 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2950 8200 2950
Text Label 4250 2850 0    50   ~ 0
V+
$Comp
L Device:C C9
U 1 1 5C0DCFF5
P 3750 3400
F 0 "C9" H 3865 3446 50  0000 L CNN
F 1 "10u" H 3800 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3788 3250 50  0001 C CNN
F 3 "~" H 3750 3400 50  0001 C CNN
	1    3750 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5C0DD02D
P 3400 3400
F 0 "C8" H 3515 3446 50  0000 L CNN
F 1 "10u" H 3450 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3438 3250 50  0001 C CNN
F 3 "~" H 3400 3400 50  0001 C CNN
	1    3400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2850 3400 3250
Connection ~ 4100 2850
Wire Wire Line
	3750 3250 3750 2850
Wire Wire Line
	3400 2850 3750 2850
Connection ~ 3750 2850
Wire Wire Line
	3750 2850 4100 2850
Wire Wire Line
	4100 3800 3750 3800
Wire Wire Line
	3400 3800 3400 3550
Connection ~ 4100 3800
Wire Wire Line
	3750 3550 3750 3800
Connection ~ 3750 3800
Wire Wire Line
	3750 3800 3400 3800
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5C0E8FD3
P 3450 2500
F 0 "J1" H 3423 2430 50  0000 R CNN
F 1 "Conn_01x01_Male" H 3423 2521 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 3450 2500 50  0001 C CNN
F 3 "~" H 3450 2500 50  0001 C CNN
	1    3450 2500
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5C0EAE68
P 3450 2600
F 0 "J2" H 3423 2530 50  0000 R CNN
F 1 "Conn_01x01_Male" H 3423 2621 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 3450 2600 50  0001 C CNN
F 3 "~" H 3450 2600 50  0001 C CNN
	1    3450 2600
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5C0EAF56
P 8650 2600
F 0 "J4" H 8756 2778 50  0000 C CNN
F 1 "Conn_01x01_Male" H 8756 2687 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 8650 2600 50  0001 C CNN
F 3 "~" H 8650 2600 50  0001 C CNN
	1    8650 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 2600 3250 2600
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5C0EDF7C
P 8450 2950
F 0 "J3" H 8423 2880 50  0000 R CNN
F 1 "Conn_01x01_Male" H 8423 2971 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 8450 2950 50  0001 C CNN
F 3 "~" H 8450 2950 50  0001 C CNN
	1    8450 2950
	-1   0    0    1   
$EndComp
Text HLabel 3400 2850 0    50   Input ~ 0
5V_Bat
Text HLabel 7100 3000 2    50   Input ~ 0
12V_Out
Wire Wire Line
	8400 2600 8450 2600
Wire Wire Line
	8150 2600 8400 2600
Connection ~ 8400 2600
$Comp
L power:+12V #PWR02
U 1 1 5C0BF066
P 8400 2600
F 0 "#PWR02" H 8400 2450 50  0001 C CNN
F 1 "+12V" H 8415 2773 50  0000 C CNN
F 2 "" H 8400 2600 50  0001 C CNN
F 3 "" H 8400 2600 50  0001 C CNN
	1    8400 2600
	1    0    0    -1  
$EndComp
Text HLabel 3400 3800 0    50   Input ~ 0
GND
$EndSCHEMATC

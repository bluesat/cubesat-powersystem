EESchema Schematic File Version 4
LIBS:MainMk1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MainMk1-rescue:MountingHole-Mechanical MH1
U 1 1 5C70847C
P 3550 1100
F 0 "MH1" H 3650 1146 50  0000 L CNN
F 1 "MountingHole" H 3650 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 3550 1100 50  0001 C CNN
F 3 "~" H 3550 1100 50  0001 C CNN
	1    3550 1100
	1    0    0    -1  
$EndComp
$Comp
L MainMk1-rescue:MountingHole-Mechanical MH3
U 1 1 5C70850E
P 5000 1100
F 0 "MH3" H 5100 1146 50  0000 L CNN
F 1 "MountingHole" H 5100 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 5000 1100 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
$Comp
L MainMk1-rescue:MountingHole-Mechanical MH4
U 1 1 5C70857C
P 5700 1100
F 0 "MH4" H 5800 1146 50  0000 L CNN
F 1 "MountingHole" H 5800 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 5700 1100 50  0001 C CNN
F 3 "~" H 5700 1100 50  0001 C CNN
	1    5700 1100
	1    0    0    -1  
$EndComp
$Comp
L MainMk1-rescue:MountingHole-Mechanical MH2
U 1 1 5C7085DC
P 4300 1100
F 0 "MH2" H 4400 1146 50  0000 L CNN
F 1 "MountingHole" H 4400 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 4300 1100 50  0001 C CNN
F 3 "~" H 4300 1100 50  0001 C CNN
	1    4300 1100
	1    0    0    -1  
$EndComp
$Comp
L MainMk1-rescue:PCbus-SOLAR U1
U 1 1 5C713480
P 2100 1650
F 0 "U1" H 3141 1321 50  0000 L CNN
F 1 "PCbus" H 3141 1230 50  0000 L CNN
F 2 "PC BUS:BUS PC104" H 2100 1650 50  0001 C CNN
F 3 "" H 2100 1650 50  0001 C CNN
	1    2100 1650
	1    0    0    -1  
$EndComp
NoConn ~ 3650 2950
NoConn ~ 3650 2850
NoConn ~ 3650 2800
NoConn ~ 3650 2750
NoConn ~ 3650 2000
NoConn ~ 3400 3200
NoConn ~ 3350 3200
NoConn ~ 3000 1450
NoConn ~ 3050 1450
NoConn ~ 3200 1450
NoConn ~ 3300 1450
NoConn ~ 3400 1450
Wire Wire Line
	3650 1800 3850 1800
Wire Wire Line
	2150 1450 2150 1250
Wire Wire Line
	3350 1250 3350 1450
Wire Wire Line
	3150 1450 3150 1250
Connection ~ 3150 1250
Wire Wire Line
	3150 1250 3350 1250
Wire Wire Line
	2950 1450 2950 1250
Connection ~ 2950 1250
Wire Wire Line
	2950 1250 3150 1250
Wire Wire Line
	2750 1450 2750 1250
Connection ~ 2750 1250
Wire Wire Line
	2750 1250 2950 1250
Wire Wire Line
	2550 1450 2550 1250
Connection ~ 2550 1250
Wire Wire Line
	2550 1250 2650 1250
Wire Wire Line
	2350 1450 2350 1250
Wire Wire Line
	2150 1250 2350 1250
Connection ~ 2350 1250
Wire Wire Line
	2350 1250 2550 1250
Wire Wire Line
	2150 1250 1950 1250
Wire Wire Line
	1950 1250 1950 1400
Connection ~ 2150 1250
$Comp
L power:GND #PWR02
U 1 1 5C7BAF9B
P 1950 1400
F 0 "#PWR02" H 1950 1150 50  0001 C CNN
F 1 "GND" H 1955 1227 50  0000 C CNN
F 2 "" H 1950 1400 50  0001 C CNN
F 3 "" H 1950 1400 50  0001 C CNN
	1    1950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2100 3850 2100
Wire Wire Line
	3850 2100 3850 1800
Wire Wire Line
	3650 2900 3850 2900
Connection ~ 3850 2100
Wire Wire Line
	3650 2300 3850 2300
Connection ~ 3850 2300
Wire Wire Line
	3850 2300 3850 2100
Wire Wire Line
	3650 2500 3850 2500
Connection ~ 3850 2500
Wire Wire Line
	3850 2500 3850 2300
Wire Wire Line
	3650 2700 3850 2700
Wire Wire Line
	3850 2500 3850 2700
Connection ~ 3850 2700
Wire Wire Line
	3850 2700 3850 2900
Wire Wire Line
	2900 3200 2900 3400
Wire Wire Line
	2900 3400 3100 3400
Wire Wire Line
	3850 3400 3850 2900
Connection ~ 3850 2900
Wire Wire Line
	3100 3200 3100 3400
Connection ~ 3100 3400
Wire Wire Line
	3100 3400 3300 3400
Wire Wire Line
	3300 3200 3300 3400
Connection ~ 3300 3400
Wire Wire Line
	3300 3400 3850 3400
$Comp
L power:GND #PWR03
U 1 1 5C7BC2F5
P 3850 3400
F 0 "#PWR03" H 3850 3150 50  0001 C CNN
F 1 "GND" H 3855 3227 50  0000 C CNN
F 2 "" H 3850 3400 50  0001 C CNN
F 3 "" H 3850 3400 50  0001 C CNN
	1    3850 3400
	1    0    0    -1  
$EndComp
Connection ~ 3850 3400
Wire Wire Line
	2750 3200 2750 3400
Wire Wire Line
	2750 3400 2800 3400
Connection ~ 2900 3400
Wire Wire Line
	2800 3200 2800 3400
Connection ~ 2800 3400
Wire Wire Line
	2800 3400 2900 3400
Wire Wire Line
	1900 1700 1700 1700
Wire Wire Line
	1700 1700 1700 1750
Wire Wire Line
	1900 2750 1700 2750
Connection ~ 1700 2750
Wire Wire Line
	1700 2750 1700 2950
Wire Wire Line
	1900 2700 1700 2700
Connection ~ 1700 2700
Wire Wire Line
	1700 2700 1700 2750
Wire Wire Line
	1900 2650 1700 2650
Connection ~ 1700 2650
Wire Wire Line
	1700 2650 1700 2700
Wire Wire Line
	1900 2600 1700 2600
Connection ~ 1700 2600
Wire Wire Line
	1700 2600 1700 2650
Wire Wire Line
	1900 1750 1700 1750
Connection ~ 1700 1750
Wire Wire Line
	1700 1750 1700 2600
$Comp
L power:GND #PWR01
U 1 1 5C7BFE55
P 1700 2950
F 0 "#PWR01" H 1700 2700 50  0001 C CNN
F 1 "GND" H 1705 2777 50  0000 C CNN
F 2 "" H 1700 2950 50  0001 C CNN
F 3 "" H 1700 2950 50  0001 C CNN
	1    1700 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3200 2300 3200
Connection ~ 2300 3200
Wire Wire Line
	2300 3200 2350 3200
Connection ~ 2350 3200
Wire Wire Line
	2350 3200 2400 3200
Connection ~ 2400 3200
Wire Wire Line
	2400 3200 2450 3200
Connection ~ 2450 3200
Wire Wire Line
	2450 3200 2500 3200
Connection ~ 2500 3200
Wire Wire Line
	2500 3200 2550 3200
Connection ~ 2550 3200
Wire Wire Line
	2550 3200 2600 3200
Connection ~ 2600 3200
Wire Wire Line
	2600 3200 2650 3200
Connection ~ 2650 3200
Wire Wire Line
	2650 3200 2700 3200
Wire Wire Line
	2500 3200 2500 3400
Text Label 2500 3400 0    30   ~ 0
5V
Wire Wire Line
	1900 2550 1900 2500
Connection ~ 1900 2150
Wire Wire Line
	1900 2150 1900 2100
Connection ~ 1900 2200
Wire Wire Line
	1900 2200 1900 2150
Connection ~ 1900 2250
Wire Wire Line
	1900 2250 1900 2200
Connection ~ 1900 2300
Wire Wire Line
	1900 2300 1900 2250
Connection ~ 1900 2350
Wire Wire Line
	1900 2350 1900 2300
Connection ~ 1900 2400
Wire Wire Line
	1900 2400 1900 2350
Connection ~ 1900 2450
Wire Wire Line
	1900 2450 1900 2400
Connection ~ 1900 2500
Wire Wire Line
	1900 2500 1900 2450
Wire Wire Line
	1900 2250 1850 2250
Text Label 1850 2250 2    50   ~ 0
5V
Wire Wire Line
	1900 2800 1900 2850
Wire Wire Line
	1900 3200 2150 3200
Connection ~ 1900 2850
Wire Wire Line
	1900 2850 1900 2900
Connection ~ 1900 2900
Wire Wire Line
	1900 2900 1900 2950
Connection ~ 1900 2950
Wire Wire Line
	1900 2950 1900 3200
Connection ~ 2150 3200
Wire Wire Line
	2150 3200 2200 3200
Wire Wire Line
	1900 3200 1900 3400
Connection ~ 1900 3200
Wire Wire Line
	1900 1800 1900 1850
Connection ~ 1900 1850
Wire Wire Line
	1900 1850 1900 1900
Connection ~ 1900 1900
Wire Wire Line
	1900 1900 1900 1950
Connection ~ 1900 1950
Wire Wire Line
	1900 1950 1900 2000
Connection ~ 1900 2000
Wire Wire Line
	1900 2000 1900 2050
Wire Wire Line
	1900 1900 1850 1900
Text Label 1850 1900 2    50   ~ 0
3V3
Text Label 1900 3400 2    30   ~ 0
3V3
Text Label 2200 1450 1    30   ~ 0
5V
Text Label 3650 2050 0    30   ~ 0
3V3
Text Label 3650 1900 0    30   ~ 0
5V
Text Label 2850 3200 3    30   ~ 0
3V3
Text Label 3250 1450 1    30   ~ 0
5V
Text Label 3100 1450 1    30   ~ 0
3V3
Text Label 2800 1450 1    30   ~ 0
3V3
Text Label 2250 1450 1    30   ~ 0
OBC1
Text Label 2300 1450 1    30   ~ 0
OBC2
Text Label 2400 1450 1    30   ~ 0
OBC3
Text Label 2450 1450 1    30   ~ 0
OBC4
Text Label 2500 1450 1    30   ~ 0
OBC5
Text Label 3650 2150 0    30   ~ 0
OBC6
Text Label 3650 2200 0    30   ~ 0
OBC7
Text Label 3650 2250 0    30   ~ 0
OBC8
Text Label 3650 2350 0    30   ~ 0
POW1
Text Label 3650 2400 0    30   ~ 0
POW2
Text Label 3650 2450 0    30   ~ 0
POW3
$Sheet
S 5600 3400 1300 1000
U 5C8719A0
F0 "regulator5V" 50
F1 "regulator5V.sch" 50
F2 "EN" I L 5600 3550 50 
F3 "PGOOD" I R 6900 3850 50 
F4 "VIN" I L 5600 3850 50 
F5 "VOUT" O R 6900 3550 50 
F6 "GND" O L 5600 4150 50 
F7 "VREG5" I R 6900 3700 50 
$EndSheet
$Sheet
S 5600 4700 1300 950 
U 5C875CC5
F0 "regulator3V3" 50
F1 "regulator3V3.sch" 50
F2 "EN" I L 5600 4850 50 
F3 "PGOOD" I R 6900 5150 50 
F4 "VIN" I L 5600 5150 50 
F5 "VOUT" O R 6900 4850 50 
F6 "GND" O L 5600 5450 50 
F7 "VREG5" I R 6900 5000 50 
$EndSheet
$Comp
L conn:Conn_01x02 J1
U 1 1 5C88A0BA
P 4800 2100
F 0 "J1" H 4720 1775 50  0000 C CNN
F 1 "Conn_01x02" H 4720 1866 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4800 2100 50  0001 C CNN
F 3 "~" H 4800 2100 50  0001 C CNN
	1    4800 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 2000 5600 2000
$Comp
L power:GND #PWR0123
U 1 1 5C88C187
P 5000 2100
F 0 "#PWR0123" H 5000 1850 50  0001 C CNN
F 1 "GND" H 5005 1927 50  0000 C CNN
F 2 "" H 5000 2100 50  0001 C CNN
F 3 "" H 5000 2100 50  0001 C CNN
	1    5000 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5C88C1B5
P 7100 2250
F 0 "#PWR0124" H 7100 2000 50  0001 C CNN
F 1 "GND" H 7105 2077 50  0000 C CNN
F 2 "" H 7100 2250 50  0001 C CNN
F 3 "" H 7100 2250 50  0001 C CNN
	1    7100 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2250 6900 2250
$Sheet
S 5600 1950 1300 1000
U 5C8611D6
F0 "battery_charger" 50
F1 "ltc4006_battery_charger.sch" 50
F2 "VL" O L 5600 2100 50 
F3 "GNDL" O L 5600 2250 50 
F4 "VBAT" O L 5600 2400 50 
F5 "GNDBAT" O L 5600 2600 50 
F6 "V_LOG" O L 5600 2750 50 
F7 "CHG'" O L 5600 2850 50 
F8 "ACP" O R 6900 2050 50 
F9 "GND" O R 6900 2250 50 
F10 "I_MON" O R 6900 2450 50 
F11 "DCIN" I L 5600 2000 50 
$EndSheet
Wire Wire Line
	6900 3550 7350 3550
Wire Wire Line
	5600 2100 5450 2100
Text Label 5450 2100 2    50   ~ 0
V_UNREG
Wire Wire Line
	5600 5150 5300 5150
Wire Wire Line
	5300 3850 5600 3850
Text Label 5300 3850 2    50   ~ 0
V_UNREG
Text Label 5300 5150 2    50   ~ 0
V_UNREG
Text Label 7350 3550 2    50   ~ 0
5V
Wire Wire Line
	6900 4850 7350 4850
Text Label 7350 4850 2    50   ~ 0
3V3
$Comp
L power:GND #PWR0125
U 1 1 5C8B1AF6
P 5400 4200
F 0 "#PWR0125" H 5400 3950 50  0001 C CNN
F 1 "GND" H 5405 4027 50  0000 C CNN
F 2 "" H 5400 4200 50  0001 C CNN
F 3 "" H 5400 4200 50  0001 C CNN
	1    5400 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 5C8B1B0F
P 5400 5500
F 0 "#PWR0126" H 5400 5250 50  0001 C CNN
F 1 "GND" H 5405 5327 50  0000 C CNN
F 2 "" H 5400 5500 50  0001 C CNN
F 3 "" H 5400 5500 50  0001 C CNN
	1    5400 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5500 5400 5450
Wire Wire Line
	5600 5450 5400 5450
Wire Wire Line
	5400 4200 5400 4150
Wire Wire Line
	5400 4150 5600 4150
$Comp
L power:GND #PWR0127
U 1 1 5C8B8E90
P 5250 2300
F 0 "#PWR0127" H 5250 2050 50  0001 C CNN
F 1 "GND" H 5255 2127 50  0000 C CNN
F 2 "" H 5250 2300 50  0001 C CNN
F 3 "" H 5250 2300 50  0001 C CNN
	1    5250 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2300 5250 2250
Wire Wire Line
	5250 2250 5350 2250
Wire Wire Line
	5600 2600 5350 2600
Wire Wire Line
	5350 2600 5350 2250
Connection ~ 5350 2250
Wire Wire Line
	5350 2250 5600 2250
Text Label 5600 2400 2    50   ~ 0
CHG2MON
$Comp
L power:GND #PWR0128
U 1 1 5C8BE181
P 4600 2900
F 0 "#PWR0128" H 4600 2650 50  0001 C CNN
F 1 "GND" H 4605 2727 50  0000 C CNN
F 2 "" H 4600 2900 50  0001 C CNN
F 3 "" H 4600 2900 50  0001 C CNN
	1    4600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3850 7100 3850
Wire Wire Line
	6900 5150 7100 5150
Text Label 7100 3850 0    50   ~ 0
PGOOD5V
Text Label 7100 5150 0    50   ~ 0
PGOOD3V3
Text Label 2600 1450 1    30   ~ 0
POW4
Text Label 2700 1450 1    30   ~ 0
POW5
Text Label 3650 2550 0    30   ~ 0
POW6
Text Label 3650 2600 0    30   ~ 0
POW7
Text Label 3650 2650 0    30   ~ 0
POW8
Text Label 2850 1450 1    30   ~ 0
POW9
Text Label 2900 1450 1    30   ~ 0
POW10
Text Label 3050 3200 3    30   ~ 0
POW11
Text Label 3000 3200 3    30   ~ 0
POW12
Text Label 2950 3200 3    30   ~ 0
POW13
Text Label 3650 1700 0    30   ~ 0
POW14
Text Label 3650 1750 0    30   ~ 0
POW15
Text Label 3650 1850 0    30   ~ 0
POW16
Text Label 3650 1950 0    30   ~ 0
POW17
Wire Wire Line
	2150 3900 2350 3900
Wire Wire Line
	2150 3950 2350 3950
Wire Wire Line
	2150 4000 2350 4000
Wire Wire Line
	2350 4050 2150 4050
Wire Wire Line
	2150 4100 2350 4100
Wire Wire Line
	2150 4150 2350 4150
Wire Wire Line
	2150 4200 2350 4200
Wire Wire Line
	2350 4250 2150 4250
Wire Wire Line
	2150 4300 2350 4300
Wire Wire Line
	2150 4350 2350 4350
Wire Wire Line
	2150 4400 2350 4400
Wire Wire Line
	2350 4450 2150 4450
Wire Wire Line
	2150 4500 2350 4500
Wire Wire Line
	2150 4550 2350 4550
Wire Wire Line
	2150 4600 2350 4600
Wire Wire Line
	2350 4650 2150 4650
Text Label 2150 3900 2    30   ~ 0
POW1
Text Label 2150 3950 2    30   ~ 0
POW2
Text Label 2150 4000 2    30   ~ 0
POW3
Text Label 2150 4050 2    30   ~ 0
POW4
Text Label 2150 4100 2    30   ~ 0
POW5
Text Label 2150 4150 2    30   ~ 0
POW6
Text Label 2150 4200 2    30   ~ 0
POW7
Text Label 2150 4250 2    30   ~ 0
POW8
Text Label 2150 4300 2    30   ~ 0
POW9
Text Label 2150 4350 2    30   ~ 0
POW10
Text Label 2150 4400 2    30   ~ 0
POW11
Text Label 2150 4450 2    30   ~ 0
POW12
Text Label 2150 4500 2    30   ~ 0
POW13
Text Label 2150 4550 2    30   ~ 0
POW14
Text Label 2150 4600 2    30   ~ 0
POW15
Text Label 2150 4650 2    30   ~ 0
POW16
Wire Wire Line
	2350 4400 2350 4450
Connection ~ 2350 4450
Wire Wire Line
	2350 4450 2350 4500
Wire Wire Line
	2350 4450 2550 4450
Text Label 2550 4450 0    30   ~ 0
DCIN
Text Notes 2750 4450 0    30   ~ 0
Reserved for power from MPPT
Wire Wire Line
	2350 4700 2150 4700
Text Label 2150 4700 2    30   ~ 0
POW17
Text Label 2350 4550 0    30   ~ 0
V_LOG
Text Label 2350 4600 0    30   ~ 0
CHG'
Text Label 2350 4700 0    30   ~ 0
I_MON
Text Label 2350 4300 0    30   ~ 0
SOFT_SDA
Text Label 2350 4350 0    30   ~ 0
SOFT_SCL
Wire Wire Line
	5600 2750 5450 2750
Wire Wire Line
	5600 2850 5450 2850
Wire Wire Line
	6900 2450 7250 2450
Wire Wire Line
	6900 2050 7250 2050
Text Label 2350 4650 0    30   ~ 0
ACP
Text Label 5450 2750 2    30   ~ 0
V_LOG
Text Label 5450 2850 2    30   ~ 0
CHG'
Text Label 7250 2050 0    30   ~ 0
ACP
Text Label 7250 2450 0    30   ~ 0
I_MON
NoConn ~ 3250 3200
NoConn ~ 3200 3200
NoConn ~ 3150 3200
Text Notes 7600 4150 0    50   ~ 0
Need to add switches for 5V and 3V3 out from the conttrol board
$Sheet
S 8450 4750 1400 900 
U 5C8A86D9
F0 "battery_monitor" 50
F1 "battery_monitor.sch" 50
F2 "SCL" I L 8450 5250 50 
F3 "SDA" I L 8450 5400 50 
F4 "VC1" I L 8450 4950 50 
F5 "VC2" I L 8450 4800 50 
F6 "SolarPannel+" I L 8450 5100 50 
F7 "GND" B L 8450 5550 50 
$EndSheet
$Comp
L MainMk1-rescue:Battery-2s_battery BT1
U 1 1 5C8CA1D4
P 4600 2650
F 0 "BT1" H 4707 2696 50  0000 L CNN
F 1 "Battery" H 4707 2605 50  0000 L CNN
F 2 "battery_holder:BatteryHolder_Keystone_1048P" V 4600 2710 50  0001 C CNN
F 3 "~" V 4600 2710 50  0001 C CNN
	1    4600 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2900 4600 2850
Text Label 4600 2450 0    50   ~ 0
VC2
Text Label 4300 2650 0    50   ~ 0
VC1
Wire Wire Line
	8450 4950 8250 4950
Wire Wire Line
	8450 4800 8250 4800
Wire Wire Line
	8450 5250 8250 5250
Wire Wire Line
	8450 5400 8250 5400
Wire Wire Line
	8450 5100 8250 5100
Text Label 8250 4800 0    50   ~ 0
VC2
Text Label 8250 4950 0    50   ~ 0
VC1
Text Label 8250 5100 2    50   ~ 0
CHG2MON
Text Label 8250 5250 2    30   ~ 0
SOFT_SCL
Text Label 8250 5400 2    30   ~ 0
SOFT_SDA
Text Label 5300 2000 0    49   ~ 0
DCIN
Wire Wire Line
	2150 4850 2350 4850
Wire Wire Line
	2150 4900 2350 4900
Wire Wire Line
	2150 4950 2350 4950
Wire Wire Line
	2350 5000 2150 5000
Wire Wire Line
	2150 5050 2350 5050
Wire Wire Line
	2150 5100 2350 5100
Text Label 2150 4850 2    30   ~ 0
OBC1
Text Label 2150 4900 2    30   ~ 0
OBC2
Text Label 2150 4950 2    30   ~ 0
OBC3
Text Label 2150 5000 2    30   ~ 0
OBC4
Text Label 2150 5050 2    30   ~ 0
OBC5
Text Label 2150 5100 2    30   ~ 0
OBC6
NoConn ~ 2350 4850
NoConn ~ 2350 4900
NoConn ~ 2350 4950
NoConn ~ 2350 5000
NoConn ~ 2350 5100
NoConn ~ 2350 5050
NoConn ~ 2350 4050
NoConn ~ 2350 4000
NoConn ~ 2350 3900
Wire Wire Line
	2650 1450 2650 1250
Connection ~ 2650 1250
Wire Wire Line
	2650 1250 2750 1250
Wire Wire Line
	2150 5150 2350 5150
Wire Wire Line
	2150 5200 2350 5200
NoConn ~ 2350 5200
NoConn ~ 2350 5150
Text Label 2150 5150 2    30   ~ 0
OBC7
Text Label 2150 5200 2    30   ~ 0
OBC8
$Comp
L MainMk1-rescue:SolderJumper_2_Open-Jumper JP1
U 1 1 5C96803F
P 3950 5600
F 0 "JP1" H 3950 5805 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3950 5714 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3950 5600 50  0001 C CNN
F 3 "~" H 3950 5600 50  0001 C CNN
	1    3950 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5600 3450 5600
Wire Wire Line
	4100 5600 4450 5600
Text Label 3450 5600 2    30   ~ 0
VC2
Text Label 4450 5600 2    30   ~ 0
V_UNREG
Wire Wire Line
	8450 5550 8250 5550
$Comp
L power:GND #PWR0135
U 1 1 5C8F7E15
P 8250 5550
F 0 "#PWR0135" H 8250 5300 50  0001 C CNN
F 1 "GND" H 8255 5377 50  0000 C CNN
F 2 "" H 8250 5550 50  0001 C CNN
F 3 "" H 8250 5550 50  0001 C CNN
	1    8250 5550
	1    0    0    -1  
$EndComp
NoConn ~ 2350 3950
Text Label 2350 4250 0    30   ~ 0
PGOOD3V3
Text Label 2350 4200 0    30   ~ 0
EN3V3
Text Label 2350 4150 0    30   ~ 0
PGOOD5V
Text Label 2350 4100 0    30   ~ 0
EN5V
Text Label 5600 4850 2    30   ~ 0
EN3V3
Text Label 5600 3550 2    30   ~ 0
EN5V
Wire Wire Line
	4400 2650 4300 2650
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:MainMk1-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4200 4750 1050 700 
U 5BC2F0D8
F0 "battery_charger" 50
F1 "ltc4006_battery_charger.sch" 50
F2 "VL" O R 5250 4850 50 
F3 "GNDL" O R 5250 4950 50 
F4 "VBAT" O R 5250 5150 50 
F5 "GNDBAT" O R 5250 5250 50 
F6 "V_LOG" O L 4200 4850 50 
F7 "CHG'" O L 4200 4950 50 
F8 "ACP" O L 4200 5050 50 
F9 "GND" O L 4200 5150 50 
F10 "I_MON" O L 4200 5250 50 
$EndSheet
$Sheet
S 5700 2700 1050 700 
U 5BC34B3F
F0 "5V regulator" 50
F1 "5Vregulator.sch" 50
F2 "EN" I L 5700 3000 50 
F3 "PGOOD" I L 5700 3100 50 
F4 "VIN" I L 5700 2900 50 
F5 "VOUT" O R 6750 2900 50 
F6 "GND" O R 6750 3000 50 
$EndSheet
$Sheet
S 5700 3650 1050 650 
U 5BC34C61
F0 "3V3 regulator" 50
F1 "3V3regulator.sch" 50
F2 "EN" I L 5700 3950 50 
F3 "PGOOD" I L 5700 4050 50 
F4 "VIN" I L 5700 3850 50 
F5 "VOUT" O R 6750 3850 50 
F6 "GND" O R 6750 3950 50 
$EndSheet
$Sheet
S 7750 2700 1050 700 
U 5C7EFE46
F0 "control" 50
F1 "control.sch" 50
$EndSheet
$Sheet
S 4200 6200 1050 700 
U 5C7EFE4C
F0 "battery_monitor" 50
F1 "battery_monitor.sch" 50
$EndSheet
$Sheet
S 2850 3200 1100 650 
U 5C7EFE56
F0 "mppt" 50
F1 "mppt.sch" 50
$EndSheet
Wire Notes Line
	950  3200 1900 3200
Wire Notes Line
	1900 3200 1900 3850
Wire Notes Line
	1900 3850 950  3850
Wire Notes Line
	950  3850 950  3200
Text Notes 1200 3450 0    50   ~ 0
Solar Panels
$Sheet
S 5700 1650 1050 650 
U 5C82DA32
F0 "pc-104" 50
F1 "pc-104.sch" 50
F2 "5V" B L 5700 1750 50 
F3 "3V3" B L 5700 1850 50 
F4 "GND" B L 5700 1950 50 
F5 "POW1" B L 5700 2050 50 
F6 "POW2" B L 5700 2150 50 
F7 "POW3" B L 5700 2250 50 
$EndSheet
$EndSCHEMATC

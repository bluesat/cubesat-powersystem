EESchema Schematic File Version 4
LIBS:Back_Up_rev0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 600  600  6300 2950
U 5C8C4D2E
F0 "5Vregulator" 50
F1 "5Vregulator.sch" 50
$EndSheet
$Sheet
S 7050 600  4050 5800
U 5C8C91A8
F0 "pc-104" 50
F1 "pc-104.sch" 50
$EndSheet
$Sheet
S 600  3900 6300 3300
U 5C8C52EC
F0 "3V3regulator" 50
F1 "3V3regulator.sch" 50
$EndSheet
$EndSCHEMATC

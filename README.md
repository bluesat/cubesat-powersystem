

### Important Commands
**git status** :: Reads out the current status of git (such as committed changes, current branch)

**git pull** :: Pulls the latest version of the repository from the server. Use the pull command every time you start a work session to ensure that your files are up to date.

**git add [. / * / "File name"]** :: Adds the files whose edits you want to commit. "File name" adds a single file, * adds everything in the folder (reg* will add all files starting with "reg"), . adds everything in the git repository.

**git commit -m "Message which describes what changes you have committed"** :: Commits the changes of the files that you have added to your local respository.

**git push** :: Pushes all of the commits you have made in your local repository on to the server. The bitbucket server will now have the changes you have made.

**git checkout branchName** :: Changes your working branch to branchName.

**git checkout -b branchName** :: Creates a new branch called branchName.

**git merge branchName** :: Don't use this unless you know what you're doing. This will merge the target branch of branchName into your currently active branch. The changes that both branches have been made will be put together.

### Git terms
branch :: The name of a copy of a repository. You can make changes to your copy without it affecting the copies that other people have. Branches can be merged together to bring all of the changes together. A merge conflict occurs when both branches being merged together have made edits to the same thing, so you will need to manually choose which version of the edit to keep from the two branches.

repository :: A single structure of folders and files which is being tracked by git. A repository is used to track the code and files used in a single project.

## Helpful Links

### A guide to learning Github
https://drive.google.com/drive/folders/1IflPeCjdNpS_eWZnppn_d7wLK83OhUo3

### Installing a bash terminal
## Windows
For a simple bash terminal with git:

https://gitforwindows.org/

Alternatively, if you are a hipster or a CSE student, you can also use Ubuntu for this:

https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows#0

## Mac
Mac natively supports a bash terminal with git. This is the Terminal on iOS.


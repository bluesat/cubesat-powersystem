# Switch Documentation

## Overview

The project entails a switch system that will be used to kill power to the CubeSat in the situation of power overloads or any other electrical faults. The purpose of implementing this subsystem into the CubeSat, is to ensure the other subsystems are not damaged. The switch system requires a logical high input to operate and thus is dependent on the control board successfully sending the logical input. Research for this project involved looking into existing CubeSat models to find a potential implementation of the switch system.

## Load Switch

A logical input is provided to the gate of the N-Channel MOSFET and the constant 5 volts is provided to the source of the P-Channel MOSFET.

## Logical High Input

When a high input is provided, the N-Channel MOSFET will have a positive voltage difference between the gate and the source (Vgs), that is higher than the threshold of that MOSFET. The resulting MOSFET will thus be switched on causing the gate of the P-Channel MOSFET to be pulled to ground. Due to the constant 5 volts provided to the source of the P-Channel MOSFET, Vgs will be negative. The MOSFET will subsequently be switched on causing the capacitor to begin charging to the input of 5 volts.

## Logical Low Input

When a low input is provided to the N-Channel MOSFET, Vgs will not surpass the threshold that is required to switch on. The resulting Vgs of the P-Channel MOSFET will not meet the required threshold and thus the MOSFET will be switched off. The output at the capacitor will therefore be 0 volts.

## Research

Many switch designs implemented a simple load switch in the CubeSat modelled however we encountered a problem with the discharging time of the capacitor. The time taken for the capacitor to discharge after a logical low input was provided was too long. The output voltage must reach zero as quick as possible to ensure power to the other subsystems is cut as soon as possible. To ensure this happens we made an alteration to the circuit by incorporating a voltage comparator. The output voltage of the capacitor is fed to the input of the voltage comparator. This reduced the time taken for the output voltage to reach zero after a logical low input was provided.

## Design Requirements and Scope

The design does not have any requirements for the conditions it can successfully operate under.

| #   | Criteria | Requirement                                                              |
| --- | -------- | ------------------------------------------------------------------------ |
| 1   | MUST     | Output voltages must change to 5V and 0V depending on the logical inputs |
| 2   | MUST     | Cut off power quick (within 100ms)                                       |
| 3   | SHOULD   | Be power efficient (>85% for input power/output power)                   |

![Circuit diagram of switch system](circuit_diagram.png "Circuit digram of switch system")

## Design Review and Testing

| #   | Criteria | Requirement                                                              | Testing Method                                                                                           | Expected                                                                        | Observed                                                    | Pass                                                                              |
| --- | -------- | ------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------- | ----------------------------------------------------------- | --------------------------------------------------------------------------------- |
| 1   | MUST     | Output voltages must change to 5V and 0V depending on the logical inputs | The switch system was physically buildt as it required readily available components.                     | 5V at the output when high logic is provided and 0V when low logic is provided. | Approximately 4.2V was measured at the output of the system | 5V was not acieved thus we are unsure                                             |
| 2   | MUST     | Cut off power quick (within 100ms)                                       | The time was physically measured which is somewhat unreliable thus further testing is required for this. | We expect a time of approximately 100ms.                                        | The timing measure was approximately 150ms.                 | Given the unreliability of the test we can say the system passes this requirement |
| 3   | SHOULD   | Be power efficient (>85% for input power/output power)                   | The output voltage was measured using a multimeter.                                                      | We expected at least 4.25V at the output.                                       | We observed approximately at the outpu.                     | The requirement was not passed.                                                   |

The second requirement can be improved if a more reliable testing method is used. A possible testing method could be done using an Arduino. The timing of the switch can be measured by supplying the output voltage as the input to the Arduino. We can then detect the time it takes for the switch to change from 5V to 0v and vice versa.
We are currently looking into a way to improve the efficiency of the output. A possible way could be to use a voltage regulator or a boost converter.

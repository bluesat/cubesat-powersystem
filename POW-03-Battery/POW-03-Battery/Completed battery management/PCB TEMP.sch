EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PCB-TEMP-rescue:Mechanical_MountingHole-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue MH1
U 1 1 5C70847C
P 3550 1100
F 0 "MH1" H 3650 1146 50  0000 L CNN
F 1 "MountingHole" H 3650 1055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm" H 3550 1100 50  0001 C CNN
F 3 "~" H 3550 1100 50  0001 C CNN
	1    3550 1100
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:Mechanical_MountingHole-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue MH3
U 1 1 5C70850E
P 5000 1100
F 0 "MH3" H 5100 1146 50  0000 L CNN
F 1 "MountingHole" H 5100 1055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm" H 5000 1100 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:Mechanical_MountingHole-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue MH4
U 1 1 5C70857C
P 5700 1100
F 0 "MH4" H 5800 1146 50  0000 L CNN
F 1 "MountingHole" H 5800 1055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm" H 5700 1100 50  0001 C CNN
F 3 "~" H 5700 1100 50  0001 C CNN
	1    5700 1100
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:Mechanical_MountingHole-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue MH2
U 1 1 5C7085DC
P 4300 1100
F 0 "MH2" H 4400 1146 50  0000 L CNN
F 1 "MountingHole" H 4400 1055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm" H 4300 1100 50  0001 C CNN
F 3 "~" H 4300 1100 50  0001 C CNN
	1    4300 1100
	1    0    0    -1  
$EndComp
NoConn ~ 3650 2950
NoConn ~ 3650 2850
NoConn ~ 3650 2000
NoConn ~ 3350 3200
NoConn ~ 3250 3200
NoConn ~ 3200 1450
NoConn ~ 3300 1450
NoConn ~ 3400 1450
Wire Wire Line
	3650 1800 3850 1800
Wire Wire Line
	3650 2100 3850 2100
Wire Wire Line
	3850 2900 3650 2900
Connection ~ 3850 2100
Wire Wire Line
	3850 2300 3650 2300
Connection ~ 3850 2300
Wire Wire Line
	3850 2500 3650 2500
Connection ~ 3850 2500
Wire Wire Line
	3850 2700 3650 2700
Connection ~ 3850 2700
Wire Wire Line
	2500 3200 2500 3400
Text Label 2500 3400 0    30   ~ 0
5V
Wire Wire Line
	1900 2250 1850 2250
Text Label 1850 2250 2    50   ~ 0
5V
Wire Wire Line
	1900 1900 1850 1900
Text Label 1850 1900 2    50   ~ 0
3V3
Text Label 2700 1450 1    30   ~ 0
POW5
Text Label 3650 2550 0    30   ~ 0
POW6
Text Label 3650 2600 0    30   ~ 0
POW7
Text Label 3650 2650 0    30   ~ 0
POW8
Text Label 2850 1450 1    30   ~ 0
POW9
Text Label 2900 1450 1    30   ~ 0
POW10
Text Label 3650 1700 0    30   ~ 0
POW14
Text Label 3650 1850 0    30   ~ 0
POW16
Text Label 3650 1950 0    30   ~ 0
POW17
NoConn ~ 3150 3200
NoConn ~ 3400 3200
NoConn ~ 3200 3200
Connection ~ 1900 1900
Connection ~ 2500 3200
Wire Wire Line
	3850 2100 3850 1800
Wire Wire Line
	3850 2300 3850 2100
Wire Wire Line
	3850 2500 3850 2300
Wire Wire Line
	3850 2700 3850 2500
Wire Wire Line
	3850 2900 3850 2700
$Sheet
S 1350 4650 3150 2400
U 5F689933
F0 "Battery_monitoring" 50
F1 "Battery_monitoring 3.sch" 50
F2 "Programmable_port" O R 4500 4850 50 
F3 "SDA" B R 4500 5100 50 
F4 "SCL" I R 4500 5350 50 
F5 "BATT_1" I L 1350 4850 50 
F6 "BATT_2" I L 1350 5050 50 
F7 "SNS" I L 1350 5200 50 
F8 "VSS" I L 1350 5350 50 
$EndSheet
NoConn ~ 3650 2750
NoConn ~ 3650 2800
Wire Wire Line
	1350 4850 1050 4850
Text Label 1050 4850 0    50   ~ 0
VC1
Wire Wire Line
	1350 5050 1050 5050
Text Label 1050 5050 0    50   ~ 0
VC2
Wire Wire Line
	7100 2200 6900 2200
Wire Wire Line
	7100 2400 6900 2400
Text Label 6900 2200 0    50   ~ 0
VC1
Text Label 6900 2400 0    50   ~ 0
VC2
Wire Wire Line
	4500 5100 4700 5100
Wire Wire Line
	4500 5350 4700 5350
Text Label 4700 5100 0    50   ~ 0
POW9
Text Label 4700 5350 0    50   ~ 0
POW10
Wire Wire Line
	4500 4850 4700 4850
Text Label 4700 4850 0    50   ~ 0
POW14
Wire Wire Line
	7100 2650 6900 2650
Wire Wire Line
	7100 2950 6900 2950
Text Label 6900 2650 0    50   ~ 0
SYS
Text Label 6900 2950 0    50   ~ 0
DCIN
Text Label 5450 2500 1    50   ~ 0
VC1
$Comp
L PCB-TEMP-rescue:GND-power-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue #PWR04
U 1 1 5F7EA4D4
P 5500 3600
F 0 "#PWR04" H 5500 3350 50  0001 C CNN
F 1 "GND" H 5505 3427 50  0000 C CNN
F 2 "" H 5500 3600 50  0001 C CNN
F 3 "" H 5500 3600 50  0001 C CNN
	1    5500 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 6000 7600 6000
Wire Wire Line
	7400 6000 7400 6100
Wire Wire Line
	7600 5850 7400 5850
Wire Wire Line
	7600 5700 7400 5700
Wire Wire Line
	7600 5550 7400 5550
Wire Wire Line
	7600 5400 7400 5400
Wire Wire Line
	9450 5300 9650 5300
Text Label 7400 5400 2    50   ~ 0
POW6
Text Label 9650 5300 0    50   ~ 0
POW8
Text Label 7400 5550 2    50   ~ 0
POW7
Text Label 7400 5700 2    50   ~ 0
POW5
Text Label 7400 5850 2    50   ~ 0
SYS
Text Label 5550 2500 1    50   ~ 0
VC2
$Comp
L PCB-TEMP-rescue:GND-power-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue #PWR0131
U 1 1 5F9EC152
P 7400 6100
F 0 "#PWR0131" H 7400 5850 50  0001 C CNN
F 1 "GND" H 7405 5927 50  0000 C CNN
F 2 "" H 7400 6100 50  0001 C CNN
F 3 "" H 7400 6100 50  0001 C CNN
	1    7400 6100
	1    0    0    -1  
$EndComp
$Sheet
S 7100 1900 3050 2050
U 5F6919C1
F0 "Battery_charging" 50
F1 "Battery_charging.sch" 50
F2 "BATT_1" O L 7100 2200 50 
F3 "BATT_2" O L 7100 2400 50 
F4 "System_load" O L 7100 2650 50 
F5 "DCIN" I L 7100 2950 50 
$EndSheet
Wire Wire Line
	3000 3200 3050 3200
Wire Wire Line
	3000 3200 2950 3200
Connection ~ 3000 3200
Wire Wire Line
	3000 3200 3000 3600
Text Label 3000 3600 0    50   ~ 0
DCIN
$Sheet
S 7600 5050 1850 1250
U 5F840520
F0 "3v3_5v5" 50
F1 "voltage_regulator.sch" 50
F2 "VIN_V" I L 7600 5850 50 
F3 "3V3_OUT_V" O R 9450 5300 50 
F4 "5V_OUT_V" I L 7600 5400 50 
F5 "3V3_EN_V" I L 7600 5550 50 
F6 "5V_EN_V" I L 7600 5700 50 
F7 "GND_V" I L 7600 6000 50 
$EndSheet
Connection ~ 1900 2550
Wire Wire Line
	2150 3200 2200 3200
Wire Wire Line
	1900 1900 1900 1950
Connection ~ 1900 1950
Wire Wire Line
	1900 1950 1900 2000
Connection ~ 1900 2000
Wire Wire Line
	1900 2000 1900 2050
Connection ~ 1900 2050
Wire Wire Line
	1900 2100 1900 2050
Connection ~ 1900 2100
Wire Wire Line
	1900 2100 1900 2150
Wire Wire Line
	1900 2150 1900 2200
Connection ~ 1900 2150
Connection ~ 1900 2250
Wire Wire Line
	1900 2250 1900 2300
Connection ~ 1900 2300
Wire Wire Line
	1900 2300 1900 2350
Connection ~ 1900 2350
Wire Wire Line
	1900 2350 1900 2400
Connection ~ 1900 2400
Wire Wire Line
	1900 2400 1900 2450
Connection ~ 1900 2450
Wire Wire Line
	1900 2500 1900 2550
Wire Wire Line
	1900 2450 1900 2500
Connection ~ 1900 2500
Wire Wire Line
	1900 2600 1900 2550
Connection ~ 1900 2600
Wire Wire Line
	1900 2650 1900 2600
Connection ~ 1900 2650
Wire Wire Line
	1900 2700 1900 2650
Connection ~ 1900 2700
Wire Wire Line
	1900 2750 1900 2700
Connection ~ 1900 2750
Wire Wire Line
	1900 2800 1900 2750
Connection ~ 1900 2800
Wire Wire Line
	1900 2800 1900 2850
Connection ~ 1900 2850
Wire Wire Line
	1900 2900 1900 2950
Wire Wire Line
	1900 2850 1900 2900
Connection ~ 1900 2900
Wire Wire Line
	2250 3200 2300 3200
Connection ~ 2300 3200
Wire Wire Line
	2300 3200 2350 3200
Connection ~ 2350 3200
Wire Wire Line
	2350 3200 2400 3200
Connection ~ 2400 3200
Wire Wire Line
	2450 3200 2500 3200
Wire Wire Line
	2400 3200 2450 3200
Connection ~ 2450 3200
Wire Wire Line
	2500 3200 2550 3200
Connection ~ 2550 3200
Wire Wire Line
	2550 3200 2600 3200
Connection ~ 2600 3200
Wire Wire Line
	2650 3200 2700 3200
Wire Wire Line
	2600 3200 2650 3200
Connection ~ 2650 3200
Wire Wire Line
	1900 1750 1900 1700
Connection ~ 1900 1750
Wire Wire Line
	1900 1800 1900 1750
Connection ~ 1900 1800
Wire Wire Line
	1900 1800 1900 1850
Wire Wire Line
	1900 1850 1900 1900
Connection ~ 1900 1850
$Comp
L PCB-TEMP-rescue:SOLAR_PCbus-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue U1
U 1 1 5C713480
P 2100 1650
F 0 "U1" H 3141 1321 50  0000 L CNN
F 1 "PCbus" H 3141 1230 50  0000 L CNN
F 2 "PC BUS:BUS PC104" H 2100 1650 50  0001 C CNN
F 3 "" H 2100 1650 50  0001 C CNN
	1    2100 1650
	1    0    0    -1  
$EndComp
NoConn ~ 3300 3200
NoConn ~ 3100 3200
NoConn ~ 2750 3200
NoConn ~ 2800 3200
NoConn ~ 2850 3200
NoConn ~ 2900 3200
NoConn ~ 3250 1450
Wire Wire Line
	2250 3200 2200 3200
Connection ~ 2250 3200
Connection ~ 2200 3200
NoConn ~ 3650 1750
NoConn ~ 3650 1850
NoConn ~ 3650 1900
NoConn ~ 3650 1950
NoConn ~ 3650 2050
NoConn ~ 3000 1450
NoConn ~ 3050 1450
NoConn ~ 3100 1450
NoConn ~ 2650 1450
NoConn ~ 2600 1450
NoConn ~ 2250 1450
NoConn ~ 2300 1450
NoConn ~ 2400 1450
NoConn ~ 2450 1450
NoConn ~ 2500 1450
NoConn ~ 2200 1450
NoConn ~ 3350 1450
NoConn ~ 2950 1450
NoConn ~ 3150 1450
NoConn ~ 2750 1450
NoConn ~ 2550 1450
NoConn ~ 2350 1450
NoConn ~ 2150 1450
NoConn ~ 2800 1450
NoConn ~ 3650 2150
NoConn ~ 3650 2200
NoConn ~ 3650 2250
NoConn ~ 3650 2350
NoConn ~ 3650 2400
NoConn ~ 3650 2450
$Comp
L PCB-TEMP-rescue:BATT-Battery_monitor-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue U7
U 1 1 6001F1BD
P 5500 2800
F 0 "U7" H 5628 2846 50  0000 L CNN
F 1 "BATT" H 5628 2755 50  0000 L CNN
F 2 "Battery_holding:VC1" H 5500 2800 50  0001 C CNN
F 3 "" H 5500 2800 50  0001 C CNN
	1    5500 2800
	1    0    0    -1  
$EndComp
Text Notes 8300 2950 0    50   ~ 0
Battery charging
Text Notes 2500 5850 0    50   ~ 0
Battery monitoring
$Comp
L Device:R_Small 10m1
U 1 1 60159D28
P 5500 3400
F 0 "10m1" H 5559 3446 50  0000 L CNN
F 1 "R_Small" H 5559 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5500 3400 50  0001 C CNN
F 3 "~" H 5500 3400 50  0001 C CNN
	1    5500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3100 5500 3200
Wire Wire Line
	5500 3200 5300 3200
Connection ~ 5500 3200
Wire Wire Line
	5500 3200 5500 3300
Text Label 5300 3200 0    50   ~ 0
SNS
Wire Wire Line
	5500 3500 5500 3550
Wire Wire Line
	5500 3550 5300 3550
Connection ~ 5500 3550
Wire Wire Line
	5500 3550 5500 3600
Text Label 5300 3550 0    50   ~ 0
VSS
Wire Wire Line
	1350 5200 1150 5200
Text Label 1150 5200 0    50   ~ 0
SNS
Wire Wire Line
	1350 5350 1150 5350
Text Label 1150 5350 0    50   ~ 0
VSS
$EndSCHEMATC

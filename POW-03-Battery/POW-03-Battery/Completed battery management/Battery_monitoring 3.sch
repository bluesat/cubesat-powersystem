EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PCB-TEMP-rescue:D_Zener_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue D8
U 1 1 5E358EFC
P 7600 4350
F 0 "D8" V 7554 4418 50  0000 L CNN
F 1 "D_Zener_Small" V 7645 4418 50  0000 L CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" V 7600 4350 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/Zener_diode" V 7600 4350 50  0001 C CNN
	1    7600 4350
	0    1    1    0   
$EndComp
$Comp
L PCB-TEMP-rescue:D_Zener_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue D7
U 1 1 5E3591E8
P 7300 4350
F 0 "D7" V 7254 4418 50  0000 L CNN
F 1 "D_Zener_Small" V 7345 4418 50  0000 L CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" V 7300 4350 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/Zener_diode" V 7300 4350 50  0001 C CNN
	1    7300 4350
	0    1    1    0   
$EndComp
$Comp
L PCB-TEMP-rescue:R_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 500K1
U 1 1 5E3596F9
P 10150 3050
F 0 "500K1" H 10209 3096 50  0000 L CNN
F 1 "R_Small" H 10209 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 10150 3050 50  0001 C CNN
F 3 "" H 10150 3050 50  0001 C CNN
	1    10150 3050
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:R_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 500K2
U 1 1 5E35A067
P 10150 4150
F 0 "500K2" H 10209 4196 50  0000 L CNN
F 1 "R_Small" H 10209 4105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 10150 4150 50  0001 C CNN
F 3 "" H 10150 4150 50  0001 C CNN
	1    10150 4150
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:R_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 500Ω1111111111
U 1 1 5E35A2A4
P 9900 2500
F 0 "500Ω1111111111" H 9959 2546 50  0000 L CNN
F 1 "R_Small" H 9959 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 9900 2500 50  0001 C CNN
F 3 "" H 9900 2500 50  0001 C CNN
	1    9900 2500
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:GND-power-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue #PWR0124
U 1 1 5E35CFBD
P 8850 2750
F 0 "#PWR0124" H 8850 2500 50  0001 C CNN
F 1 "GND" H 8855 2577 50  0000 C CNN
F 2 "" H 8850 2750 50  0001 C CNN
F 3 "" H 8850 2750 50  0001 C CNN
	1    8850 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2000 7550 2100
$Comp
L PCB-TEMP-rescue:GND-power-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue #PWR0125
U 1 1 5E3612A4
P 7550 2350
F 0 "#PWR0125" H 7550 2100 50  0001 C CNN
F 1 "GND" H 7555 2177 50  0000 C CNN
F 2 "" H 7550 2350 50  0001 C CNN
F 3 "" H 7550 2350 50  0001 C CNN
	1    7550 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2300 7550 2350
$Comp
L PCB-TEMP-rescue:GND-power-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue #PWR0126
U 1 1 5E364812
P 9700 2300
F 0 "#PWR0126" H 9700 2050 50  0001 C CNN
F 1 "GND" H 9705 2127 50  0000 C CNN
F 2 "" H 9700 2300 50  0001 C CNN
F 3 "" H 9700 2300 50  0001 C CNN
	1    9700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 2000 9900 2400
Wire Wire Line
	9900 4050 9900 4600
Connection ~ 9900 4600
Wire Wire Line
	10150 2950 10150 2000
Wire Wire Line
	10600 2000 10150 2000
Wire Wire Line
	7300 4450 7300 4600
Wire Wire Line
	7600 4450 7600 4600
Connection ~ 7600 4600
Wire Wire Line
	7600 4600 7300 4600
$Comp
L PCB-TEMP-rescue:C_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 0.1uF10
U 1 1 5E38294E
P 7550 2200
F 0 "0.1uF10" H 7642 2246 50  0000 L CNN
F 1 "C_Small" H 7642 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 7550 2200 50  0001 C CNN
F 3 "" H 7550 2200 50  0001 C CNN
	1    7550 2200
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:C_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 4.7uF2
U 1 1 5E383108
P 9700 2100
F 0 "4.7uF2" H 9792 2146 50  0000 L CNN
F 1 "C_Small" H 9792 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 9700 2100 50  0001 C CNN
F 3 "" H 9700 2100 50  0001 C CNN
	1    9700 2100
	1    0    0    -1  
$EndComp
$Comp
L PCB-TEMP-rescue:C_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 0.1uF11
U 1 1 5E38331F
P 9900 3950
F 0 "0.1uF11" H 9992 3996 50  0000 L CNN
F 1 "C_Small" H 9992 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 9900 3950 50  0001 C CNN
F 3 "" H 9900 3950 50  0001 C CNN
	1    9900 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2300 9700 2200
NoConn ~ 9450 3950
$Comp
L PCB-TEMP-rescue:DS2782-Battery_monitor-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue U3
U 1 1 5F687689
P 8800 3850
F 0 "U3" H 8725 4715 50  0000 C CNN
F 1 "DS2782" H 8725 4624 50  0000 C CNN
F 2 "battery_charging:ds2782e&plus_" H 8800 3900 50  0001 C CNN
F 3 "" H 8800 3900 50  0001 C CNN
	1    8800 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2000 9900 2000
Wire Wire Line
	9200 3950 9450 3950
Wire Wire Line
	8400 3950 8200 3950
Text HLabel 10600 4050 1    50   Input ~ 0
BATT_1
Text HLabel 10600 2600 3    50   Input ~ 0
BATT_2
Wire Wire Line
	9400 2000 9700 2000
Connection ~ 9700 2000
$Comp
L PCB-TEMP-rescue:GND-power-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue #PWR0132
U 1 1 5FAE2775
P 9050 2750
F 0 "#PWR0132" H 9050 2500 50  0001 C CNN
F 1 "GND" H 9055 2577 50  0000 C CNN
F 2 "" H 9050 2750 50  0001 C CNN
F 3 "" H 9050 2750 50  0001 C CNN
	1    9050 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 2650 8850 2750
Wire Wire Line
	9050 2650 9050 2750
$Comp
L PCB-TEMP-rescue:MAX1616-Battery_monitor-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue U4
U 1 1 5F9D3976
P 8950 2200
F 0 "U4" H 8950 2625 50  0000 C CNN
F 1 "MAX1616" H 8950 2534 50  0000 C CNN
F 2 "battery_charging:MAX1616EUK&plus_" H 8950 2200 50  0001 C CNN
F 3 "" H 8950 2200 50  0001 C CNN
	1    8950 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 2000 10600 2600
Wire Wire Line
	10600 4600 10600 4050
Wire Wire Line
	8300 1700 8300 2000
Connection ~ 8300 2000
Wire Wire Line
	8300 2000 8500 2000
Text HLabel 8450 4600 2    50   Input ~ 0
SNS
Text HLabel 9200 4600 0    50   Input ~ 0
VSS
Wire Wire Line
	8400 4050 8300 4050
Wire Wire Line
	8300 4050 8300 4600
Wire Wire Line
	7600 4600 8300 4600
Connection ~ 8300 4600
Wire Wire Line
	8300 4600 8450 4600
Wire Wire Line
	9200 4050 9300 4050
Wire Wire Line
	9300 4050 9300 4600
Wire Wire Line
	9200 4600 9300 4600
Connection ~ 9300 4600
Wire Wire Line
	9300 4600 9900 4600
Wire Wire Line
	9900 4600 10150 4600
Wire Wire Line
	10150 3800 10150 4050
Wire Wire Line
	9200 3800 10150 3800
Wire Wire Line
	10150 4250 10150 4600
Connection ~ 10150 4600
Wire Wire Line
	10150 4600 10600 4600
Wire Wire Line
	10150 3800 10150 3150
Connection ~ 10150 3800
Wire Wire Line
	7550 2000 8100 2000
Wire Wire Line
	8500 2250 8100 2250
Wire Wire Line
	8100 2250 8100 2000
Connection ~ 8100 2000
Wire Wire Line
	8100 2000 8300 2000
Text HLabel 6950 3650 0    50   BiDi ~ 0
SDA
$Comp
L PCB-TEMP-rescue:R_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 150Ω1111111111
U 1 1 5E359C56
P 7150 3650
F 0 "150Ω1111111111" V 6954 3650 50  0000 C CNN
F 1 "R_Small" V 7045 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7150 3650 50  0001 C CNN
F 3 "" H 7150 3650 50  0001 C CNN
	1    7150 3650
	0    1    1    0   
$EndComp
Text HLabel 6950 3800 0    50   Input ~ 0
SCL
$Comp
L PCB-TEMP-rescue:R_Small-device-PCB-TEMP-rescue-PCB-TEMP-rescue-PCB-TEMP-rescue 150Ω2111111111
U 1 1 5E359A97
P 7150 3800
F 0 "150Ω2111111111" H 7209 3846 50  0000 L CNN
F 1 "R_Small" H 7209 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7150 3800 50  0001 C CNN
F 3 "" H 7150 3800 50  0001 C CNN
	1    7150 3800
	0    1    1    0   
$EndComp
Text HLabel 8200 3950 0    50   Output ~ 0
Programmable_port
Wire Wire Line
	9900 2600 9900 3650
Wire Wire Line
	8400 3650 7600 3650
Wire Wire Line
	7250 3800 7300 3800
Wire Wire Line
	7050 3800 6950 3800
Wire Wire Line
	7050 3650 6950 3650
Wire Wire Line
	7300 4250 7300 3800
Connection ~ 7300 3800
Wire Wire Line
	7300 3800 8400 3800
Wire Wire Line
	7600 4250 7600 3650
Connection ~ 7600 3650
Wire Wire Line
	7600 3650 7250 3650
Wire Wire Line
	10150 1700 10150 2000
Wire Wire Line
	8300 1700 10150 1700
Connection ~ 10150 2000
Wire Wire Line
	9200 3650 9900 3650
Connection ~ 9900 3650
Wire Wire Line
	9900 3650 9900 3850
$EndSCHEMATC

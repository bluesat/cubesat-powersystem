EESchema Schematic File Version 4
LIBS:Master_Regulator-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Master_Regulator-rescue:TPS564208-TPS564208 U1
U 1 1 5D05935D
P 3450 3600
F 0 "U1" H 3500 3987 50  0000 C CNN
F 1 "TPS564208" H 3500 3881 50  0000 C CNN
F 2 "digikey-footprints:SOT-23-6" H 3650 4150 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Ftps27081a" H 3650 4250 60  0001 L CNN
F 4 "296-34970-1-ND" H 3650 4350 60  0001 L CNN "Digi-Key_PN"
F 5 "TPS564208" H 3650 4100 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 3650 4450 60  0001 L CNN "Category"
F 7 "Texas Instruments" H 3650 4550 60  0001 L CNN "Manufacturer"
F 8 "Active" H 3650 4800 60  0001 L CNN "Status"
	1    3450 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5D0D8399
P 3150 3500
F 0 "#PWR0109" H 3150 3250 50  0001 C CNN
F 1 "GND" V 3155 3372 50  0000 R CNN
F 2 "" H 3150 3500 50  0001 C CNN
F 3 "" H 3150 3500 50  0001 C CNN
	1    3150 3500
	0    1    1    0   
$EndComp
$Comp
L Master_Regulator-rescue:L_Small-Device L1
U 1 1 5D0D844B
P 2550 3700
F 0 "L1" V 2500 3700 50  0000 C CNN
F 1 "3.3uH" V 2600 3700 50  0000 C CNN
F 2 "bluesat-inductors:L_Bourns_SRR1210" H 2550 3700 50  0001 C CNN
F 3 "~" H 2550 3700 50  0001 C CNN
	1    2550 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 3700 2800 3700
$Comp
L Master_Regulator-rescue:C_Small-Device C6
U 1 1 5D0D86FE
P 3500 2950
F 0 "C6" V 3400 2950 50  0000 C CNN
F 1 "0.1uF" V 3600 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3500 2950 50  0001 C CNN
F 3 "~" H 3500 2950 50  0001 C CNN
	1    3500 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 3700 2800 2950
Wire Wire Line
	2800 2950 3400 2950
Connection ~ 2800 3700
Wire Wire Line
	2800 3700 3150 3700
Wire Wire Line
	3600 2950 4000 2950
Wire Wire Line
	4000 2950 4000 3500
Wire Wire Line
	4000 3500 3850 3500
$Comp
L Master_Regulator-rescue:R_Small-Device R1
U 1 1 5D0D8887
P 3950 3700
F 0 "R1" V 3850 3700 50  0000 C CNN
F 1 "10k" V 4050 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3950 3700 50  0001 C CNN
F 3 "~" H 3950 3700 50  0001 C CNN
	1    3950 3700
	0    1    1    0   
$EndComp
Text HLabel 4050 3700 2    50   Input ~ 0
EN_H
$Comp
L Master_Regulator-rescue:C_Small-Device C7
U 1 1 5D0D89DD
P 4450 3900
F 0 "C7" V 4350 3900 50  0000 C CNN
F 1 "0.1uF" V 4550 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 3900 50  0001 C CNN
F 3 "~" H 4450 3900 50  0001 C CNN
	1    4450 3900
	0    1    1    0   
$EndComp
$Comp
L Master_Regulator-rescue:R_Small-Device R3
U 1 1 5D0D8A1F
P 4450 4250
F 0 "R3" V 4350 4250 50  0000 C CNN
F 1 "54.9k" V 4550 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 4250 50  0001 C CNN
F 3 "~" H 4450 4250 50  0001 C CNN
	1    4450 4250
	0    1    1    0   
$EndComp
$Comp
L Master_Regulator-rescue:R_Small-Device R2
U 1 1 5D0D8BE2
P 4000 4000
F 0 "R2" H 4059 4046 50  0000 L CNN
F 1 "10k" H 4059 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4000 4000 50  0001 C CNN
F 3 "~" H 4000 4000 50  0001 C CNN
	1    4000 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3900 4000 3900
Connection ~ 4000 3900
Wire Wire Line
	4000 3900 4350 3900
Wire Wire Line
	4350 3900 4350 4250
Connection ~ 4350 3900
Wire Wire Line
	4550 3900 4550 4250
$Comp
L power:GND #PWR0110
U 1 1 5D0D8D6D
P 4000 4100
F 0 "#PWR0110" H 4000 3850 50  0001 C CNN
F 1 "GND" H 4005 3927 50  0000 C CNN
F 2 "" H 4000 4100 50  0001 C CNN
F 3 "" H 4000 4100 50  0001 C CNN
	1    4000 4100
	1    0    0    -1  
$EndComp
Text HLabel 4550 3900 2    50   BiDi ~ 0
5V_out_H
$Comp
L Master_Regulator-rescue:C_Small-Device C2
U 1 1 5D0D8DF9
P 2200 3800
F 0 "C2" H 2292 3846 50  0000 L CNN
F 1 "22uF" H 2292 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2200 3800 50  0001 C CNN
F 3 "~" H 2200 3800 50  0001 C CNN
	1    2200 3800
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C_Small-Device C1
U 1 1 5D0D8E70
P 1850 3800
F 0 "C1" H 1942 3846 50  0000 L CNN
F 1 "22uF" H 1942 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1850 3800 50  0001 C CNN
F 3 "~" H 1850 3800 50  0001 C CNN
	1    1850 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3700 2200 3700
Connection ~ 2200 3700
Wire Wire Line
	2200 3700 1850 3700
Text HLabel 1850 3700 0    50   BiDi ~ 0
5V_out_H
Wire Wire Line
	1850 3900 2200 3900
$Comp
L power:GND #PWR0111
U 1 1 5D0D9496
P 2200 3900
F 0 "#PWR0111" H 2200 3650 50  0001 C CNN
F 1 "GND" H 2205 3727 50  0000 C CNN
F 2 "" H 2200 3900 50  0001 C CNN
F 3 "" H 2200 3900 50  0001 C CNN
	1    2200 3900
	1    0    0    -1  
$EndComp
Connection ~ 2200 3900
$Comp
L Master_Regulator-rescue:C_Small-Device C5
U 1 1 5D0D9702
P 3150 4300
F 0 "C5" H 3242 4346 50  0000 L CNN
F 1 "0.1uF" H 3242 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 4300 50  0001 C CNN
F 3 "~" H 3150 4300 50  0001 C CNN
	1    3150 4300
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C_Small-Device C4
U 1 1 5D0D9709
P 2800 4300
F 0 "C4" H 2892 4346 50  0000 L CNN
F 1 "10uF" H 2892 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2800 4300 50  0001 C CNN
F 3 "~" H 2800 4300 50  0001 C CNN
	1    2800 4300
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C_Small-Device C3
U 1 1 5D0D9862
P 2450 4300
F 0 "C3" H 2542 4346 50  0000 L CNN
F 1 "10uF" H 2542 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2450 4300 50  0001 C CNN
F 3 "~" H 2450 4300 50  0001 C CNN
	1    2450 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3900 3150 4200
Wire Wire Line
	3150 4200 2800 4200
Connection ~ 3150 4200
Connection ~ 2800 4200
Wire Wire Line
	2800 4200 2450 4200
Wire Wire Line
	2450 4400 2800 4400
Connection ~ 2800 4400
Wire Wire Line
	2800 4400 3150 4400
$Comp
L power:GND #PWR0112
U 1 1 5D0D9D99
P 2800 4400
F 0 "#PWR0112" H 2800 4150 50  0001 C CNN
F 1 "GND" H 2805 4227 50  0000 C CNN
F 2 "" H 2800 4400 50  0001 C CNN
F 3 "" H 2800 4400 50  0001 C CNN
	1    2800 4400
	1    0    0    -1  
$EndComp
Text HLabel 2450 4200 0    50   Input ~ 0
V_IN_H
$Comp
L Master_Regulator-rescue:TPS564208-TPS564208 U2
U 1 1 5D0DB2A0
P 8050 3550
F 0 "U2" H 8100 3937 50  0000 C CNN
F 1 "TPS564208" H 8100 3831 50  0000 C CNN
F 2 "digikey-footprints:SOT-23-6" H 8250 4100 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Ftps27081a" H 8250 4200 60  0001 L CNN
F 4 "296-34970-1-ND" H 8250 4300 60  0001 L CNN "Digi-Key_PN"
F 5 "TPS564208" H 8250 4050 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8250 4400 60  0001 L CNN "Category"
F 7 "Texas Instruments" H 8250 4500 60  0001 L CNN "Manufacturer"
F 8 "Active" H 8250 4750 60  0001 L CNN "Status"
	1    8050 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5D0DB2A7
P 7750 3450
F 0 "#PWR0113" H 7750 3200 50  0001 C CNN
F 1 "GND" V 7755 3322 50  0000 R CNN
F 2 "" H 7750 3450 50  0001 C CNN
F 3 "" H 7750 3450 50  0001 C CNN
	1    7750 3450
	0    1    1    0   
$EndComp
$Comp
L Master_Regulator-rescue:L_Small-Device L2
U 1 1 5D0DB2AD
P 7150 3650
F 0 "L2" V 7100 3650 50  0000 C CNN
F 1 "2.2uH" V 7200 3700 50  0000 C CNN
F 2 "bluesat-inductors:L_Bourns_SRR1210" H 7150 3650 50  0001 C CNN
F 3 "~" H 7150 3650 50  0001 C CNN
	1    7150 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	7250 3650 7400 3650
$Comp
L Master_Regulator-rescue:C_Small-Device C13
U 1 1 5D0DB2B5
P 8100 2900
F 0 "C13" V 8000 2900 50  0000 C CNN
F 1 "0.1uF" V 8200 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8100 2900 50  0001 C CNN
F 3 "~" H 8100 2900 50  0001 C CNN
	1    8100 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 3650 7400 2900
Wire Wire Line
	7400 2900 8000 2900
Connection ~ 7400 3650
Wire Wire Line
	7400 3650 7750 3650
Wire Wire Line
	8200 2900 8600 2900
Wire Wire Line
	8600 2900 8600 3450
Wire Wire Line
	8600 3450 8450 3450
$Comp
L Master_Regulator-rescue:R_Small-Device R4
U 1 1 5D0DB2C3
P 8550 3650
F 0 "R4" V 8450 3650 50  0000 C CNN
F 1 "10k" V 8650 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8550 3650 50  0001 C CNN
F 3 "~" H 8550 3650 50  0001 C CNN
	1    8550 3650
	0    1    1    0   
$EndComp
Text HLabel 8650 3650 2    50   Input ~ 0
EN_H
$Comp
L Master_Regulator-rescue:C_Small-Device C14
U 1 1 5D0DB2CB
P 9050 3850
F 0 "C14" V 8950 3850 50  0000 C CNN
F 1 "0.1uF" V 9150 3850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9050 3850 50  0001 C CNN
F 3 "~" H 9050 3850 50  0001 C CNN
	1    9050 3850
	0    1    1    0   
$EndComp
$Comp
L Master_Regulator-rescue:R_Small-Device R6
U 1 1 5D0DB2D2
P 9050 4200
F 0 "R6" V 8950 4200 50  0000 C CNN
F 1 "33.2k" V 9150 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9050 4200 50  0001 C CNN
F 3 "~" H 9050 4200 50  0001 C CNN
	1    9050 4200
	0    1    1    0   
$EndComp
$Comp
L Master_Regulator-rescue:R_Small-Device R5
U 1 1 5D0DB2D9
P 8600 3950
F 0 "R5" H 8659 3996 50  0000 L CNN
F 1 "10k" H 8659 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8600 3950 50  0001 C CNN
F 3 "~" H 8600 3950 50  0001 C CNN
	1    8600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3850 8600 3850
Connection ~ 8600 3850
Wire Wire Line
	8600 3850 8950 3850
Wire Wire Line
	8950 3850 8950 4200
Connection ~ 8950 3850
Wire Wire Line
	9150 3850 9150 4200
$Comp
L power:GND #PWR0114
U 1 1 5D0DB2E6
P 8600 4050
F 0 "#PWR0114" H 8600 3800 50  0001 C CNN
F 1 "GND" H 8605 3877 50  0000 C CNN
F 2 "" H 8600 4050 50  0001 C CNN
F 3 "" H 8600 4050 50  0001 C CNN
	1    8600 4050
	1    0    0    -1  
$EndComp
Text HLabel 9150 3850 2    50   BiDi ~ 0
3V3_out_H
$Comp
L Master_Regulator-rescue:C_Small-Device C9
U 1 1 5D0DB2ED
P 6800 3750
F 0 "C9" H 6892 3796 50  0000 L CNN
F 1 "22uF" H 6892 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6800 3750 50  0001 C CNN
F 3 "~" H 6800 3750 50  0001 C CNN
	1    6800 3750
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C_Small-Device C8
U 1 1 5D0DB2F4
P 6450 3750
F 0 "C8" H 6542 3796 50  0000 L CNN
F 1 "22uF" H 6542 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6450 3750 50  0001 C CNN
F 3 "~" H 6450 3750 50  0001 C CNN
	1    6450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3650 6800 3650
Connection ~ 6800 3650
Wire Wire Line
	6800 3650 6450 3650
Text HLabel 6450 3650 0    50   BiDi ~ 0
3V3_out_H
Wire Wire Line
	6450 3850 6800 3850
$Comp
L power:GND #PWR0115
U 1 1 5D0DB300
P 6800 3850
F 0 "#PWR0115" H 6800 3600 50  0001 C CNN
F 1 "GND" H 6805 3677 50  0000 C CNN
F 2 "" H 6800 3850 50  0001 C CNN
F 3 "" H 6800 3850 50  0001 C CNN
	1    6800 3850
	1    0    0    -1  
$EndComp
Connection ~ 6800 3850
$Comp
L Master_Regulator-rescue:C_Small-Device C12
U 1 1 5D0DB307
P 7750 4250
F 0 "C12" H 7842 4296 50  0000 L CNN
F 1 "0.1uF" H 7842 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7750 4250 50  0001 C CNN
F 3 "~" H 7750 4250 50  0001 C CNN
	1    7750 4250
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C_Small-Device C11
U 1 1 5D0DB30E
P 7400 4250
F 0 "C11" H 7492 4296 50  0000 L CNN
F 1 "10uF" H 7492 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7400 4250 50  0001 C CNN
F 3 "~" H 7400 4250 50  0001 C CNN
	1    7400 4250
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C_Small-Device C10
U 1 1 5D0DB315
P 7050 4250
F 0 "C10" H 7142 4296 50  0000 L CNN
F 1 "10uF" H 7142 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7050 4250 50  0001 C CNN
F 3 "~" H 7050 4250 50  0001 C CNN
	1    7050 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3850 7750 4150
Wire Wire Line
	7750 4150 7400 4150
Connection ~ 7750 4150
Connection ~ 7400 4150
Wire Wire Line
	7400 4150 7050 4150
Wire Wire Line
	7050 4350 7400 4350
Connection ~ 7400 4350
Wire Wire Line
	7400 4350 7750 4350
$Comp
L power:GND #PWR0116
U 1 1 5D0DB324
P 7400 4350
F 0 "#PWR0116" H 7400 4100 50  0001 C CNN
F 1 "GND" H 7405 4177 50  0000 C CNN
F 2 "" H 7400 4350 50  0001 C CNN
F 3 "" H 7400 4350 50  0001 C CNN
	1    7400 4350
	1    0    0    -1  
$EndComp
Text HLabel 7050 4150 0    50   Input ~ 0
V_IN_H
Text Notes 3000 2600 0    89   ~ 18
5V Regulator
Text Notes 7600 2600 0    89   ~ 18
3V3 Regulator\n
$Comp
L power:GND #PWR0117
U 1 1 5D203491
P 5200 5000
F 0 "#PWR0117" H 5200 4750 50  0001 C CNN
F 1 "GND" H 5205 4827 50  0000 C CNN
F 2 "" H 5200 5000 50  0001 C CNN
F 3 "" H 5200 5000 50  0001 C CNN
	1    5200 5000
	1    0    0    -1  
$EndComp
Text HLabel 5200 5000 0    50   BiDi ~ 0
GND_H
$EndSCHEMATC

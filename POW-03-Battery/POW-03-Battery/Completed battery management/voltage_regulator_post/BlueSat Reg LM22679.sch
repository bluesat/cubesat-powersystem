EESchema Schematic File Version 4
LIBS:Master_Regulator-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Master_Regulator-rescue:C-Device C17
U 1 1 5D0DF564
P 4300 2750
F 0 "C17" H 4415 2796 50  0000 L CNN
F 1 "6.8uF" H 4415 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4338 2600 50  0001 C CNN
F 3 "~" H 4300 2750 50  0001 C CNN
	1    4300 2750
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C-Device C19
U 1 1 5D0DF5AC
P 4650 2750
F 0 "C19" H 4765 2796 50  0000 L CNN
F 1 "1uF" H 4765 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4688 2600 50  0001 C CNN
F 3 "~" H 4650 2750 50  0001 C CNN
	1    4650 2750
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:CP1-Device C15
U 1 1 5D0DF5CC
P 3950 2750
F 0 "C15" H 4065 2796 50  0000 L CNN
F 1 "22uF" H 4065 2705 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 3988 2600 50  0001 C CNN
F 3 "~" H 3950 2750 50  0001 C CNN
	1    3950 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2450 5250 2450
Wire Wire Line
	4650 2450 4650 2600
Wire Wire Line
	4650 2450 4300 2450
Wire Wire Line
	4300 2450 4300 2600
Connection ~ 4650 2450
Wire Wire Line
	4300 2450 3950 2450
Wire Wire Line
	3950 2450 3950 2600
Connection ~ 4300 2450
Wire Wire Line
	3950 2900 3950 3100
Wire Wire Line
	3950 3100 4300 3100
Wire Wire Line
	5850 3100 5850 2950
Wire Wire Line
	4300 2900 4300 3100
Connection ~ 4300 3100
Wire Wire Line
	4300 3100 4650 3100
Wire Wire Line
	4650 2900 4650 3100
Connection ~ 4650 3100
NoConn ~ 6050 2950
$Comp
L Master_Regulator-rescue:C-Device C21
U 1 1 5D0DFE6E
P 6550 2450
F 0 "C21" V 6298 2450 50  0000 C CNN
F 1 "10nF" V 6389 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6588 2300 50  0001 C CNN
F 3 "~" H 6550 2450 50  0001 C CNN
	1    6550 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2450 6300 2450
$Comp
L Master_Regulator-rescue:L-Device L3
U 1 1 5D0DFFD7
P 7000 2600
F 0 "L3" V 7190 2600 50  0000 C CNN
F 1 "10uH" V 7099 2600 50  0000 C CNN
F 2 "bluesat-inductors:L_Bourns_SRR1210" H 7000 2600 50  0001 C CNN
F 3 "~" H 7000 2600 50  0001 C CNN
	1    7000 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 2450 6700 2600
Wire Wire Line
	6300 2600 6700 2600
Connection ~ 6700 2600
$Comp
L Master_Regulator-rescue:D_Schottky-Device D1
U 1 1 5D0E03F2
P 6700 2900
F 0 "D1" V 6654 2979 50  0000 L CNN
F 1 "D_Schottky" V 6745 2979 50  0000 L CNN
F 2 "Diode_SMD:D_SMC" H 6700 2900 50  0001 C CNN
F 3 "~" H 6700 2900 50  0001 C CNN
	1    6700 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 3100 6700 3100
Wire Wire Line
	6700 3100 6700 3050
Connection ~ 5850 3100
Wire Wire Line
	6700 2750 6700 2600
$Comp
L Master_Regulator-rescue:C-Device C23
U 1 1 5D0E072F
P 7750 2900
F 0 "C23" H 7865 2946 50  0000 L CNN
F 1 "2.2uF" H 7865 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7788 2750 50  0001 C CNN
F 3 "~" H 7750 2900 50  0001 C CNN
	1    7750 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3050 7300 3100
Wire Wire Line
	7300 3100 6700 3100
Wire Wire Line
	6850 2600 6700 2600
Wire Wire Line
	7150 2600 7300 2600
Wire Wire Line
	7300 2600 7300 2750
$Comp
L Master_Regulator-rescue:R-Device R9
U 1 1 5D0E0D5D
P 7300 2350
F 0 "R9" H 7370 2396 50  0000 L CNN
F 1 "1.54k" H 7370 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7230 2350 50  0001 C CNN
F 3 "~" H 7300 2350 50  0001 C CNN
	1    7300 2350
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:R-Device R10
U 1 1 5D0E0DAD
P 7500 2050
F 0 "R10" V 7293 2050 50  0000 C CNN
F 1 "976R" V 7384 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7430 2050 50  0001 C CNN
F 3 "~" H 7500 2050 50  0001 C CNN
	1    7500 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 2150 5950 2050
Wire Wire Line
	5950 2050 7300 2050
Connection ~ 7300 2050
Wire Wire Line
	7350 2050 7300 2050
$Comp
L power:GNDREF #PWR0118
U 1 1 5D0E1A8E
P 7650 2100
F 0 "#PWR0118" H 7650 1850 50  0001 C CNN
F 1 "GNDREF" H 7655 1927 50  0000 C CNN
F 2 "" H 7650 2100 50  0001 C CNN
F 3 "" H 7650 2100 50  0001 C CNN
	1    7650 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2100 7650 2050
$Comp
L power:GNDREF #PWR0119
U 1 1 5D0E1E19
P 5850 3150
F 0 "#PWR0119" H 5850 2900 50  0001 C CNN
F 1 "GNDREF" H 5855 2977 50  0000 C CNN
F 2 "" H 5850 3150 50  0001 C CNN
F 3 "" H 5850 3150 50  0001 C CNN
	1    5850 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3150 5850 3100
Wire Wire Line
	7300 2050 7300 2200
Wire Wire Line
	7300 2500 7300 2600
Text HLabel 3950 2450 0    50   Input ~ 0
VIN_V
Text HLabel 7300 2600 2    50   Input ~ 0
3V3_OUT_V
$Comp
L Master_Regulator-rescue:C-Device C18
U 1 1 5D0E45C3
P 4350 4800
F 0 "C18" H 4465 4846 50  0000 L CNN
F 1 "6.8uF" H 4465 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4388 4650 50  0001 C CNN
F 3 "~" H 4350 4800 50  0001 C CNN
	1    4350 4800
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C-Device C20
U 1 1 5D0E45C9
P 4700 4800
F 0 "C20" H 4815 4846 50  0000 L CNN
F 1 "1uF" H 4815 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4738 4650 50  0001 C CNN
F 3 "~" H 4700 4800 50  0001 C CNN
	1    4700 4800
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:CP1-Device C16
U 1 1 5D0E45CF
P 4000 4800
F 0 "C16" H 4115 4846 50  0000 L CNN
F 1 "22uF" H 4115 4755 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 4038 4650 50  0001 C CNN
F 3 "~" H 4000 4800 50  0001 C CNN
	1    4000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4500 5300 4500
Wire Wire Line
	4700 4500 4700 4650
Wire Wire Line
	4700 4500 4350 4500
Wire Wire Line
	4350 4500 4350 4650
Connection ~ 4700 4500
Wire Wire Line
	4350 4500 4000 4500
Wire Wire Line
	4000 4500 4000 4650
Connection ~ 4350 4500
Wire Wire Line
	4000 4950 4000 5150
Wire Wire Line
	4000 5150 4350 5150
Wire Wire Line
	5900 5150 5900 5000
Wire Wire Line
	4350 4950 4350 5150
Connection ~ 4350 5150
Wire Wire Line
	4350 5150 4700 5150
Wire Wire Line
	4700 4950 4700 5150
Connection ~ 4700 5150
NoConn ~ 6100 5000
$Comp
L Master_Regulator-rescue:C-Device C22
U 1 1 5D0E45F1
P 6600 4500
F 0 "C22" V 6348 4500 50  0000 C CNN
F 1 "10nF" V 6439 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6638 4350 50  0001 C CNN
F 3 "~" H 6600 4500 50  0001 C CNN
	1    6600 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 4500 6350 4500
$Comp
L Master_Regulator-rescue:L-Device L4
U 1 1 5D0E45F8
P 7050 4650
F 0 "L4" V 7240 4650 50  0000 C CNN
F 1 "10uH" V 7149 4650 50  0000 C CNN
F 2 "bluesat-inductors:L_Bourns_SRR1210" H 7050 4650 50  0001 C CNN
F 3 "~" H 7050 4650 50  0001 C CNN
	1    7050 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 4500 6750 4650
Wire Wire Line
	6350 4650 6750 4650
Connection ~ 6750 4650
$Comp
L Master_Regulator-rescue:D_Schottky-Device D2
U 1 1 5D0E4601
P 6750 4950
F 0 "D2" V 6704 5029 50  0000 L CNN
F 1 "D_Schottky" V 6795 5029 50  0000 L CNN
F 2 "Diode_SMD:D_SMC" H 6750 4950 50  0001 C CNN
F 3 "~" H 6750 4950 50  0001 C CNN
	1    6750 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	5900 5150 6750 5150
Wire Wire Line
	6750 5150 6750 5100
Connection ~ 5900 5150
Wire Wire Line
	6750 4800 6750 4650
Wire Wire Line
	6900 4650 6750 4650
Wire Wire Line
	7200 4650 7350 4650
$Comp
L power:GNDREF #PWR0120
U 1 1 5D0E462E
P 5900 5200
F 0 "#PWR0120" H 5900 4950 50  0001 C CNN
F 1 "GNDREF" H 5905 5027 50  0000 C CNN
F 2 "" H 5900 5200 50  0001 C CNN
F 3 "" H 5900 5200 50  0001 C CNN
	1    5900 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5200 5900 5150
Text HLabel 4000 4500 0    50   Input ~ 0
VIN_V
Text HLabel 7350 4650 2    50   Input ~ 0
5V_OUT_V
Text Notes 4000 4050 0    79   ~ 0
5V Regulator
Text Notes 4000 1900 0    79   ~ 0
3V3 Regulator
Wire Wire Line
	6000 4200 7350 4200
Wire Wire Line
	7350 4200 7350 4650
$Comp
L Master_Regulator-rescue:R-Device R7
U 1 1 5D172178
P 5400 2600
F 0 "R7" V 5500 2500 50  0000 C CNN
F 1 "10k" V 5500 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5330 2600 50  0001 C CNN
F 3 "~" H 5400 2600 50  0001 C CNN
	1    5400 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 2600 5600 2600
Wire Wire Line
	5250 2600 5250 2450
Connection ~ 5250 2450
Wire Wire Line
	5250 2450 4650 2450
Wire Wire Line
	5550 2600 5550 2900
Wire Wire Line
	5550 2900 5450 2900
Connection ~ 5550 2600
$Comp
L Master_Regulator-rescue:R-Device R8
U 1 1 5D176390
P 5450 4650
F 0 "R8" V 5550 4550 50  0000 C CNN
F 1 "10k" V 5550 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5380 4650 50  0001 C CNN
F 3 "~" H 5450 4650 50  0001 C CNN
	1    5450 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 4950 5500 4950
Wire Wire Line
	5300 4650 5300 4500
Connection ~ 5300 4500
Wire Wire Line
	5300 4500 4700 4500
Wire Wire Line
	5600 4950 5600 4650
Wire Wire Line
	5650 4650 5600 4650
Connection ~ 5600 4650
Wire Wire Line
	4650 3100 5850 3100
Wire Wire Line
	4700 5150 5900 5150
Text HLabel 5450 2900 0    50   Input ~ 0
3V3_EN_V
Text HLabel 5500 4950 0    50   Input ~ 0
5V_EN_V
$Comp
L Master_Regulator-rescue:LM22678-LM22678 U3
U 1 1 5D184AF9
P 5950 2550
F 0 "U3" H 5950 3128 50  0000 C CNN
F 1 "LM22678_Adj" H 5950 3037 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-7_TabPin4" H 5950 2550 50  0001 C CNN
F 3 "" H 5950 2550 50  0001 C CNN
	1    5950 2550
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:LM22678-LM22678 U4
U 1 1 5D184B76
P 6000 4600
F 0 "U4" H 6000 5178 50  0000 C CNN
F 1 "LM22678_Fixed" H 6000 5087 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-7_TabPin4" H 6000 4600 50  0001 C CNN
F 3 "" H 6000 4600 50  0001 C CNN
	1    6000 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0121
U 1 1 5D20406D
P 8300 3600
F 0 "#PWR0121" H 8300 3350 50  0001 C CNN
F 1 "GNDREF" H 8305 3427 50  0000 C CNN
F 2 "" H 8300 3600 50  0001 C CNN
F 3 "" H 8300 3600 50  0001 C CNN
	1    8300 3600
	1    0    0    -1  
$EndComp
Text HLabel 8300 3600 0    50   Input ~ 0
GND_V
$Comp
L Master_Regulator-rescue:CP1-Device C24
U 1 1 5D298A92
P 7300 2900
F 0 "C24" H 7415 2946 50  0000 L CNN
F 1 "180uF" H 7415 2855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 7300 2900 50  0001 C CNN
F 3 "~" H 7300 2900 50  0001 C CNN
	1    7300 2900
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C-Device C44
U 1 1 5D298C3B
P 8300 2900
F 0 "C44" H 8415 2946 50  0000 L CNN
F 1 "100nF" H 8415 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8338 2750 50  0001 C CNN
F 3 "~" H 8300 2900 50  0001 C CNN
	1    8300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2750 7750 2750
Connection ~ 7300 2750
Wire Wire Line
	7750 2750 8300 2750
Connection ~ 7750 2750
Wire Wire Line
	8300 3050 8300 3100
Wire Wire Line
	8300 3100 7750 3100
Connection ~ 7300 3100
Wire Wire Line
	7750 3050 7750 3100
Connection ~ 7750 3100
Wire Wire Line
	7750 3100 7300 3100
Connection ~ 7300 2600
Connection ~ 6700 3100
$Comp
L Master_Regulator-rescue:C-Device C43
U 1 1 5D2A0940
P 7800 4950
F 0 "C43" H 7915 4996 50  0000 L CNN
F 1 "2.2uF" H 7915 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7838 4800 50  0001 C CNN
F 3 "~" H 7800 4950 50  0001 C CNN
	1    7800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 5100 7350 5150
Wire Wire Line
	7350 5150 6750 5150
Wire Wire Line
	7350 4650 7350 4800
$Comp
L Master_Regulator-rescue:CP1-Device C42
U 1 1 5D2A0949
P 7350 4950
F 0 "C42" H 7465 4996 50  0000 L CNN
F 1 "180uF" H 7465 4905 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 7350 4950 50  0001 C CNN
F 3 "~" H 7350 4950 50  0001 C CNN
	1    7350 4950
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:C-Device C45
U 1 1 5D2A094F
P 8350 4950
F 0 "C45" H 8465 4996 50  0000 L CNN
F 1 "100nF" H 8465 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8388 4800 50  0001 C CNN
F 3 "~" H 8350 4950 50  0001 C CNN
	1    8350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 4800 7800 4800
Connection ~ 7350 4800
Wire Wire Line
	7800 4800 8350 4800
Connection ~ 7800 4800
Wire Wire Line
	8350 5100 8350 5150
Wire Wire Line
	8350 5150 7800 5150
Connection ~ 7350 5150
Wire Wire Line
	7800 5100 7800 5150
Connection ~ 7800 5150
Wire Wire Line
	7800 5150 7350 5150
Connection ~ 6750 5150
Connection ~ 7350 4650
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:Master_Regulator-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6550 1250 2650 2050
U 5D2043C0
F0 "Harry" 50
F1 "Voltage_Reg_V0.sch" 50
F2 "EN_H" I L 6550 1650 50 
F3 "5V_out_H" B L 6550 2050 50 
F4 "V_IN_H" I L 6550 2800 50 
F5 "3V3_out_H" B L 6550 2400 50 
F6 "GND_H" B L 6550 3050 50 
$EndSheet
$Comp
L Master_Regulator-rescue:Conn_01x02_Male-Connector J5
U 1 1 5D20482C
P 6150 2050
F 0 "J5" H 6123 1930 50  0000 R CNN
F 1 "5V_OUT_H" H 6123 2021 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6150 2050 50  0001 C CNN
F 3 "~" H 6150 2050 50  0001 C CNN
	1    6150 2050
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:Conn_01x02_Male-Connector J8
U 1 1 5D2048DC
P 6600 4150
F 0 "J8" H 6573 4030 50  0000 R CNN
F 1 "V_IN" H 6573 4121 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6600 4150 50  0001 C CNN
F 3 "~" H 6600 4150 50  0001 C CNN
	1    6600 4150
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D2049DA
P 3700 3050
F 0 "#PWR0101" H 3700 2800 50  0001 C CNN
F 1 "GND" H 3705 2877 50  0000 C CNN
F 2 "" H 3700 3050 50  0001 C CNN
F 3 "" H 3700 3050 50  0001 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 3050 3700 3050
$Comp
L power:GND #PWR0102
U 1 1 5D204A30
P 6400 3050
F 0 "#PWR0102" H 6400 2800 50  0001 C CNN
F 1 "GND" H 6405 2877 50  0000 C CNN
F 2 "" H 6400 3050 50  0001 C CNN
F 3 "" H 6400 3050 50  0001 C CNN
	1    6400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3050 6550 3050
$Comp
L Master_Regulator-rescue:Conn_01x03_Male-Connector J9
U 1 1 5D204AA4
P 7600 4150
F 0 "J9" H 7706 4428 50  0000 C CNN
F 1 "GND" H 7706 4337 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 7600 4150 50  0001 C CNN
F 3 "~" H 7600 4150 50  0001 C CNN
	1    7600 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4050 7800 4150
Wire Wire Line
	7800 4150 7800 4250
Connection ~ 7800 4150
$Comp
L power:GND #PWR0103
U 1 1 5D204B0C
P 7800 4300
F 0 "#PWR0103" H 7800 4050 50  0001 C CNN
F 1 "GND" H 7805 4127 50  0000 C CNN
F 2 "" H 7800 4300 50  0001 C CNN
F 3 "" H 7800 4300 50  0001 C CNN
	1    7800 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4300 7800 4250
Connection ~ 7800 4250
$Comp
L Master_Regulator-rescue:Conn_01x01_Male-Connector J7
U 1 1 5D203A9A
P 6350 1650
F 0 "J7" H 6456 1828 50  0000 C CNN
F 1 "EN_H" H 6350 1700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6350 1650 50  0001 C CNN
F 3 "~" H 6350 1650 50  0001 C CNN
	1    6350 1650
	1    0    0    -1  
$EndComp
$Comp
L Master_Regulator-rescue:Conn_01x02_Male-Connector J6
U 1 1 5D2040E0
P 6150 2400
F 0 "J6" H 6123 2280 50  0000 R CNN
F 1 "3V3_OUT_H" H 6123 2371 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6150 2400 50  0001 C CNN
F 3 "~" H 6150 2400 50  0001 C CNN
	1    6150 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2050 6550 2050
Wire Wire Line
	6350 2400 6550 2400
$Comp
L power:GND #PWR0104
U 1 1 5D20413A
P 6350 2150
F 0 "#PWR0104" H 6350 1900 50  0001 C CNN
F 1 "GND" H 6355 1977 50  0000 C CNN
F 2 "" H 6350 2150 50  0001 C CNN
F 3 "" H 6350 2150 50  0001 C CNN
	1    6350 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5D204166
P 6350 2500
F 0 "#PWR0105" H 6350 2250 50  0001 C CNN
F 1 "GND" H 6355 2327 50  0000 C CNN
F 2 "" H 6350 2500 50  0001 C CNN
F 3 "" H 6350 2500 50  0001 C CNN
	1    6350 2500
	1    0    0    -1  
$EndComp
Text GLabel 6500 2800 0    50   Input ~ 0
V_IN
Wire Wire Line
	6500 2800 6550 2800
$Comp
L Master_Regulator-rescue:Conn_01x01_Male-Connector J4
U 1 1 5D203FBD
P 4200 2150
F 0 "J4" H 4172 2080 50  0000 R CNN
F 1 "5V_EN_V" H 4172 2171 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4200 2150 50  0001 C CNN
F 3 "~" H 4200 2150 50  0001 C CNN
	1    4200 2150
	-1   0    0    1   
$EndComp
$Comp
L Master_Regulator-rescue:Conn_01x01_Male-Connector J3
U 1 1 5D204005
P 4200 1950
F 0 "J3" H 4172 1880 50  0000 R CNN
F 1 "3V3_EN_V" H 4172 1971 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4200 1950 50  0001 C CNN
F 3 "~" H 4200 1950 50  0001 C CNN
	1    4200 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 1950 4000 1950
Wire Wire Line
	3600 2150 4000 2150
$Comp
L Master_Regulator-rescue:Conn_01x02_Male-Connector J2
U 1 1 5D204111
P 4200 1850
F 0 "J2" H 4173 1730 50  0000 R CNN
F 1 "5V_OUT_V" H 4173 1821 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4200 1850 50  0001 C CNN
F 3 "~" H 4200 1850 50  0001 C CNN
	1    4200 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 1750 3600 1750
$Comp
L power:GND #PWR0106
U 1 1 5D2041B9
P 3950 1850
F 0 "#PWR0106" H 3950 1600 50  0001 C CNN
F 1 "GND" H 3955 1677 50  0000 C CNN
F 2 "" H 3950 1850 50  0001 C CNN
F 3 "" H 3950 1850 50  0001 C CNN
	1    3950 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1850 4000 1850
$Comp
L Master_Regulator-rescue:Conn_01x02_Male-Connector J1
U 1 1 5D204259
P 4200 1650
F 0 "J1" H 4173 1530 50  0000 R CNN
F 1 "3V3_OUT_V" H 4173 1621 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4200 1650 50  0001 C CNN
F 3 "~" H 4200 1650 50  0001 C CNN
	1    4200 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 1550 3600 1550
$Comp
L power:GND #PWR0107
U 1 1 5D204332
P 3950 1650
F 0 "#PWR0107" H 3950 1400 50  0001 C CNN
F 1 "GND" H 3955 1477 50  0000 C CNN
F 2 "" H 3950 1650 50  0001 C CNN
F 3 "" H 3950 1650 50  0001 C CNN
	1    3950 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1650 4000 1650
$Comp
L power:GND #PWR0108
U 1 1 5D2044DB
P 6350 4150
F 0 "#PWR0108" H 6350 3900 50  0001 C CNN
F 1 "GND" H 6355 3977 50  0000 C CNN
F 2 "" H 6350 4150 50  0001 C CNN
F 3 "" H 6350 4150 50  0001 C CNN
	1    6350 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4150 6400 4150
Text GLabel 6350 4050 0    50   Input ~ 0
V_IN
Wire Wire Line
	6350 4050 6400 4050
Wire Wire Line
	3600 2850 3650 2850
Text GLabel 3650 2850 2    50   Input ~ 0
V_IN
$Sheet
S 1100 1200 2500 2150
U 5D203027
F0 "Vaughan" 50
F1 "BlueSat Reg LM22679.sch" 50
F2 "VIN_V" I R 3600 2850 50 
F3 "3V3_OUT_V" I R 3600 1550 50 
F4 "5V_OUT_V" I R 3600 1750 50 
F5 "3V3_EN_V" I R 3600 1950 50 
F6 "5V_EN_V" I R 3600 2150 50 
F7 "GND_V" I R 3600 3050 50 
$EndSheet
$Sheet
S 1500 4450 3300 2150
U 5D29424E
F0 "Bailey" 50
F1 "Power regulation.sch" 50
$EndSheet
$EndSCHEMATC

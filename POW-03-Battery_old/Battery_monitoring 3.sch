EESchema Schematic File Version 4
LIBS:Battery_monitoring-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L device:D_Zener_Small D2
U 1 1 5E358EFC
P 7000 3400
F 0 "D2" V 6954 3468 50  0000 L CNN
F 1 "D_Zener_Small" V 7045 3468 50  0000 L CNN
F 2 "Diodes_THT:D_5W_P12.70mm_Horizontal" V 7000 3400 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/Zener_diode" V 7000 3400 50  0001 C CNN
	1    7000 3400
	0    1    1    0   
$EndComp
$Comp
L device:D_Zener_Small D1
U 1 1 5E3591E8
P 6700 3400
F 0 "D1" V 6654 3468 50  0000 L CNN
F 1 "D_Zener_Small" V 6745 3468 50  0000 L CNN
F 2 "Diodes_THT:D_5W_P12.70mm_Horizontal" V 6700 3400 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/Zener_diode" V 6700 3400 50  0001 C CNN
	1    6700 3400
	0    1    1    0   
$EndComp
$Comp
L device:R_Small 500K1
U 1 1 5E3596F9
P 9550 2100
F 0 "500K1" H 9609 2146 50  0000 L CNN
F 1 "R_Small" H 9609 2055 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9550 2100 50  0001 C CNN
F 3 "" H 9550 2100 50  0001 C CNN
	1    9550 2100
	1    0    0    -1  
$EndComp
$Comp
L device:R_Small 150Ω2111111111
U 1 1 5E359A97
P 6550 2700
F 0 "150Ω2111111111" H 6609 2746 50  0000 L CNN
F 1 "R_Small" H 6609 2655 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6550 2700 50  0001 C CNN
F 3 "" H 6550 2700 50  0001 C CNN
	1    6550 2700
	0    1    1    0   
$EndComp
$Comp
L device:R_Small 150Ω1111111111
U 1 1 5E359C56
P 6550 2450
F 0 "150Ω1111111111" V 6354 2450 50  0000 C CNN
F 1 "R_Small" V 6445 2450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6550 2450 50  0001 C CNN
F 3 "" H 6550 2450 50  0001 C CNN
	1    6550 2450
	0    1    1    0   
$EndComp
$Comp
L device:R_Small 10m1
U 1 1 5E359E7E
P 8100 3650
F 0 "10m1" V 7904 3650 50  0000 C CNN
F 1 "R_Small" V 7995 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 8100 3650 50  0001 C CNN
F 3 "" H 8100 3650 50  0001 C CNN
	1    8100 3650
	0    1    1    0   
$EndComp
$Comp
L device:R_Small 500K2
U 1 1 5E35A067
P 9550 2800
F 0 "500K2" H 9609 2846 50  0000 L CNN
F 1 "R_Small" H 9609 2755 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9550 2800 50  0001 C CNN
F 3 "" H 9550 2800 50  0001 C CNN
	1    9550 2800
	1    0    0    -1  
$EndComp
$Comp
L device:R_Small 500Ω1111111111
U 1 1 5E35A2A4
P 9300 1550
F 0 "500Ω1111111111" H 9359 1596 50  0000 L CNN
F 1 "R_Small" H 9359 1505 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9300 1550 50  0001 C CNN
F 3 "" H 9300 1550 50  0001 C CNN
	1    9300 1550
	1    0    0    -1  
$EndComp
$Comp
L Battery_monitoring:DS2782 U1
U 1 1 5E35B087
P 8250 2800
F 0 "U1" H 8125 3465 50  0000 C CNN
F 1 "DS2782" H 8125 3374 50  0000 C CNN
F 2 "DS2782:ds2782e&plus_" H 8250 2800 50  0001 C CNN
F 3 "" H 8250 2800 50  0001 C CNN
	1    8250 2800
	1    0    0    -1  
$EndComp
$Comp
L Battery_monitoring:MAX1616 U2
U 1 1 5E35C093
P 8300 1550
F 0 "U2" H 8275 2315 50  0000 C CNN
F 1 "MAX1616" H 8275 2224 50  0000 C CNN
F 2 "MAX1616:MAX1616EUK&plus_" H 8300 1550 50  0001 C CNN
F 3 "" H 8300 1550 50  0001 C CNN
	1    8300 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1350 7500 1350
Wire Wire Line
	7500 1350 7500 1050
Wire Wire Line
	7750 1050 7500 1050
Connection ~ 7500 1050
Wire Wire Line
	7500 1050 7350 1050
Wire Wire Line
	8250 1700 8250 1800
Wire Wire Line
	8450 1700 8450 1800
Wire Wire Line
	8450 1800 8350 1800
Wire Wire Line
	8350 1800 8350 1850
Connection ~ 8350 1800
Wire Wire Line
	8350 1800 8250 1800
$Comp
L power:GND #PWR02
U 1 1 5E35CFBD
P 8350 1850
F 0 "#PWR02" H 8350 1600 50  0001 C CNN
F 1 "GND" H 8355 1677 50  0000 C CNN
F 2 "" H 8350 1850 50  0001 C CNN
F 3 "" H 8350 1850 50  0001 C CNN
	1    8350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1050 7350 900 
Wire Wire Line
	7350 900  6950 900 
Wire Wire Line
	6950 900  6950 1000
Connection ~ 6950 900 
$Comp
L power:GND #PWR01
U 1 1 5E3612A4
P 6950 1250
F 0 "#PWR01" H 6950 1000 50  0001 C CNN
F 1 "GND" H 6955 1077 50  0000 C CNN
F 2 "" H 6950 1250 50  0001 C CNN
F 3 "" H 6950 1250 50  0001 C CNN
	1    6950 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1200 6950 1250
$Comp
L power:GND #PWR03
U 1 1 5E364812
P 9100 1350
F 0 "#PWR03" H 9100 1100 50  0001 C CNN
F 1 "GND" H 9105 1177 50  0000 C CNN
F 2 "" H 9100 1350 50  0001 C CNN
F 3 "" H 9100 1350 50  0001 C CNN
	1    9100 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 1050 9300 1450
Wire Wire Line
	9300 1650 9300 2450
Wire Wire Line
	9300 2450 8800 2450
Wire Wire Line
	9300 2450 9300 2900
Connection ~ 9300 2450
Wire Wire Line
	8800 2700 9550 2700
Wire Wire Line
	9550 2700 9550 2200
Connection ~ 9550 2700
Wire Wire Line
	7450 3200 7300 3200
Wire Wire Line
	7300 3200 7300 3650
Wire Wire Line
	7300 3650 8000 3650
Wire Wire Line
	8200 3650 8900 3650
Wire Wire Line
	8900 3650 8900 3200
Wire Wire Line
	8900 3200 8800 3200
Wire Wire Line
	9300 3100 9300 3650
Wire Wire Line
	9300 3650 8900 3650
Connection ~ 8900 3650
Wire Wire Line
	9300 3650 9550 3650
Wire Wire Line
	9550 3650 9550 2900
Connection ~ 9300 3650
Wire Wire Line
	9550 2000 9550 1050
Wire Wire Line
	9550 1050 9300 1050
Connection ~ 9300 1050
Wire Wire Line
	10000 1050 9550 1050
Connection ~ 9550 1050
Wire Wire Line
	10000 3650 9550 3650
Connection ~ 9550 3650
Wire Wire Line
	7450 2700 6700 2700
Wire Wire Line
	7450 2450 7000 2450
Wire Wire Line
	7000 3300 7000 2450
Wire Wire Line
	6700 3300 6700 2700
Wire Wire Line
	7000 2450 6650 2450
Connection ~ 7000 2450
Wire Wire Line
	6700 2700 6650 2700
Connection ~ 6700 2700
Wire Wire Line
	6450 2700 6200 2700
Wire Wire Line
	7300 3650 7000 3650
Connection ~ 7300 3650
Wire Wire Line
	6700 3500 6700 3650
Connection ~ 6700 3650
Wire Wire Line
	7000 3500 7000 3650
Connection ~ 7000 3650
Wire Wire Line
	7000 3650 6700 3650
$Comp
L device:C_Small 0.1uF1
U 1 1 5E38294E
P 6950 1100
F 0 "0.1uF1" H 7042 1146 50  0000 L CNN
F 1 "C_Small" H 7042 1055 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6950 1100 50  0001 C CNN
F 3 "" H 6950 1100 50  0001 C CNN
	1    6950 1100
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small 4.7uF1
U 1 1 5E383108
P 9100 1150
F 0 "4.7uF1" H 9192 1196 50  0000 L CNN
F 1 "C_Small" H 9192 1105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 9100 1150 50  0001 C CNN
F 3 "" H 9100 1150 50  0001 C CNN
	1    9100 1150
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small 0.1uF2
U 1 1 5E38331F
P 9300 3000
F 0 "0.1uF2" H 9392 3046 50  0000 L CNN
F 1 "C_Small" H 9392 2955 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 9300 3000 50  0001 C CNN
F 3 "" H 9300 3000 50  0001 C CNN
	1    9300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 1050 9100 1050
Connection ~ 9100 1050
Wire Wire Line
	9100 1050 9300 1050
Wire Wire Line
	9100 1350 9100 1250
$Comp
L conn:Conn_01x02 I2C1
U 1 1 5E430531
P 4600 2900
F 0 "I2C1" H 4518 2575 50  0000 C CNN
F 1 "Conn_01x02" H 4518 2666 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 4600 2900 50  0001 C CNN
F 3 "~" H 4600 2900 50  0001 C CNN
	1    4600 2900
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x01 Programmable_port1
U 1 1 5E433C70
P 7250 2950
F 0 "Programmable_port1" H 7168 2725 50  0000 C CNN
F 1 "Conn_01x01" H 7168 2816 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch1.00mm" H 7250 2950 50  0001 C CNN
F 3 "~" H 7250 2950 50  0001 C CNN
	1    7250 2950
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x02 Battery1
U 1 1 5E434732
P 3800 2500
F 0 "Battery1" H 3718 2175 50  0000 C CNN
F 1 "Conn_01x02" H 3718 2266 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.00mm" H 3800 2500 50  0001 C CNN
F 3 "~" H 3800 2500 50  0001 C CNN
	1    3800 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4950 2700 4950 2800
Wire Wire Line
	4950 2800 4800 2800
Wire Wire Line
	4900 2950 4900 2900
Wire Wire Line
	4900 2900 4800 2900
Wire Wire Line
	4150 3500 4150 2500
Wire Wire Line
	4150 2500 4000 2500
Wire Wire Line
	5250 1600 4350 1600
Wire Wire Line
	4350 1600 4350 2400
Wire Wire Line
	4350 2400 4000 2400
Wire Wire Line
	10000 1050 10000 3650
Wire Wire Line
	5250 1600 5250 900 
Wire Wire Line
	5250 900  6950 900 
Wire Wire Line
	5900 3500 5900 3650
Wire Wire Line
	5900 3650 6700 3650
Wire Wire Line
	4150 3500 5900 3500
Wire Wire Line
	6200 2950 6200 2700
Wire Wire Line
	4900 2950 6200 2950
Wire Wire Line
	5850 2700 5850 2450
Wire Wire Line
	5850 2450 6450 2450
Wire Wire Line
	4950 2700 5850 2700
NoConn ~ 8800 2950
$EndSCHEMATC
